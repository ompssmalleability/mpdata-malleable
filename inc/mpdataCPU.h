#ifndef MPDATACPU_H
#define MPDATACPU_H

#include "mpdataGPU.h"

#if GCP<100
bool threadQuery(const int tId, const int val, const int ts, const int tag=0);

//void showMe(const int procId_deb, const int tId);

void initCPU();

void initCpuKrnCall(MatrixCPU<realX> *xC,
            MatrixCPU<realV1> *v1C,
            MatrixCPU<realV2> *v2C,
            MatrixCPU<realV3> *v3C,
            MatrixCPU<realH> *hC,
            MatrixCPU<realX> *xPC,
          /*  MatrixCPU<realV1P> *v1PC,
            MatrixCPU<realV2P> *v2PC,
            MatrixCPU<realV3P> *v3PC,
            MatrixCPU<realCP> *cpC,
            MatrixCPU<realCN> *cnC,
            const int procId,*/
            const int offYCPU, const int offXCPU);

class RunCPU: public RunBase
{
  private:
    MatrixCPU<realX> *xC;
    MatrixCPU<realV1> *v1C;
    MatrixCPU<realV2> *v2C;
    MatrixCPU<realV3> *v3C;
    MatrixCPU<realH> *hC;
    MatrixCPU<realX> *xPC;
    MatrixCPU<realV1P> *v1PC;
    MatrixCPU<realV2P> *v2PC;
    MatrixCPU<realV3P> *v3PC;
    MatrixCPU<realCP> *cpC;
    MatrixCPU<realCN> *cnC;
    const int procId;
    const int threadId;
    const int runFlag;
    const bool firstColumn;

  #if GTC>1
    int waitingList[GTC-1];    
    void notify(const int val, const int ts);
    void wait(const int val, const int ts);
  #endif

  public:
    RunCPU(MatrixCPU<realX> *xC,
           MatrixCPU<realV1> *v1C,
           MatrixCPU<realV2> *v2C,
           MatrixCPU<realV3> *v3C,
           MatrixCPU<realH> *hC,
           MatrixCPU<realX> *xPC,
           MatrixCPU<realV1P> *v1PC,
           MatrixCPU<realV2P> *v2PC,
           MatrixCPU<realV3P> *v3PC,
           MatrixCPU<realCP> *cpC,
           MatrixCPU<realCN> *cnC,
           const int pId, const int tId,
           const int runFlag, const bool firstColumn)
    : xC(xC), v1C(v1C), v2C(v2C), v3C(v3C), hC(hC), xPC(xPC),
      v1PC(v1PC), v2PC(v2PC), v3PC(v3PC), cpC(cpC), cnC(cnC),
      procId(pId), threadId(tId), runFlag(runFlag), firstColumn(firstColumn)
    {
    #if GTC>1
      int wL=0;
      for(int id=0; id<THS; ++id)
      {
        if(mdCPU.tToM[id]==mdCPU.tToM[threadId] && id!=threadId)
        {
          waitingList[wL]=id;
          ++wL;
        }
      }
    #endif
    }

    int run(const int ts);
    void reset() {}
};

class UpdateCPUhor: public RunBase
{
  private:
    const int idTto;
    const int idTfrom;
    realX *fstTo;
    realX *fstFrom;
    const int n;
    const int m;
#if HSPC==1
    const bool LR;
#endif
    int ind;
    const int tmp;

    int update(const int ts);
#if HSPC==1
    int query(const int ts);
#endif
    int done(const int ts);

#if HSPC==1
    int (UpdateCPUhor::*stage[3])(const int ts);
#else
    int (UpdateCPUhor::*stage[2])(const int ts);
#endif

  public:
    UpdateCPUhor(const int tmp, const int idTto, const int idTfrom,
                  realX *fstTo, realX *fstFrom,
                  const int n, const int m
#if HSPC==1
                  , const bool LR
#endif
                  )
    : idTto(idTto), idTfrom(idTfrom),
      fstTo(fstTo), fstFrom(fstFrom), n(n), m(m)
#if HSPC==1
      , LR(LR)
#endif
      , ind(0)
      , tmp(tmp)
    {
      stage[0] = &UpdateCPUhor::update;
    #if HSPC==1
      stage[1] = &UpdateCPUhor::query;
      stage[2] = &UpdateCPUhor::done;
    #else
      stage[1] = &UpdateCPUhor::done;
    #endif
    }

    int run(const int ts);
    void reset() { ind=0; }
};

#if HSPC==0
class QueryCPUhor: public RunBase
{
  private:
    const int idTto;
    const int idTfrom;
    int ind;

    int query(const int ts);
    int done(const int ts);

    int (QueryCPUhor::*stage[2])(const int ts);

  public:
    QueryCPUhor(const int idTto, const int idTfrom)
    : idTto(idTto), idTfrom(idTfrom), ind(0)
    {
      stage[0] = &QueryCPUhor::query;
      stage[1] = &QueryCPUhor::done;
    }

    int run(const int ts);
    void reset() { ind=0; }
};
#endif //HSPC==0

//************************** vertical begin
class UpdateCPUver: public RunBase
{
  private:
    const int idTto;
    const int idTfrom;
  #if CPYC>0
    realX *dstTo;
    realX *srcFrom;
    const int sizeV;
    #if HSPC==0
      realX *dstFrom;
      realX *srcTo;
    #endif
  #else
    realX *fstTo;
    realX *fstFrom;
    const int n;
    const int m;
  #endif
    const bool TB;
    int ind;

    int update(const int ts);
#if HSPC==1
    int query(const int ts);
#endif
    int done(const int ts);

#if HSPC==1
    int (UpdateCPUver::*stage[3])(const int ts);
#else
    int (UpdateCPUver::*stage[2])(const int ts);
#endif

    bool queryRows(const int ts);

  public:
    UpdateCPUver(const int idTto, const int idTfrom,
  #if CPYC>0
    realX *dstTo, realX *srcFrom, const int sizeV,
    #if HSPC==0
      realX *dstFrom, realX *srcTo,
    #endif
  #else
    realX *fstTo, realX *fstFrom, const int n, const int m,
  #endif
    const bool TB)
    : idTto(idTto), idTfrom(idTfrom),
  #if CPYC>0
    dstTo(dstTo), srcFrom(srcFrom), sizeV(sizeV),
    #if HSPC==0
      dstFrom(dstFrom), srcTo(srcTo),
    #endif
  #else
    fstTo(fstTo), fstFrom(fstFrom), n(n), m(m),
  #endif
    TB(TB), ind(0)
    {
      stage[0] = &UpdateCPUver::update;
    #if HSPC==1
      stage[1] = &UpdateCPUver::query;
      stage[2] = &UpdateCPUver::done;
    #else
      stage[1] = &UpdateCPUver::done;
    #endif
    }

    int run(const int ts);
    void reset() { ind=0; }
};

//#if HSPC==0
class QueryCPUver: public RunBase
{
  private:
    const int idTfrom;
    const int tag;
    int ind;

    int query(const int ts);
    int done(const int ts);

    int (QueryCPUver::*stage[2])(const int ts);

  public:
    QueryCPUver(const int idTfrom, const int tag=0)
    : idTfrom(idTfrom), tag(tag), ind(0)
    {
      stage[0] = &QueryCPUver::query;
      stage[1] = &QueryCPUver::done;
    }

    int run(const int ts);
    void reset() { ind=0; }
};
//#endif //HSPC==0
//************************** vertical end

#if NDS>1
class UpdateCPUhorMPI: public RunBase
{
  private:
    const int idTFrom;
    MatrixCPU<realX> *mC;
    const bool LR;
    int ind;

    int procId;

    int updateBuf(const int timeStep);
    int updateMat(const int timeStep);
    int done(const int timeStep);

    int (UpdateCPUhorMPI::*stage[3])(const int timeStep);

  public:
    UpdateCPUhorMPI(const int idTFrom, MatrixCPU<realX> &mC, const bool LR, int procId)
    : idTFrom(idTFrom), mC(&mC), LR(LR), ind(0), procId(procId)
    {
      stage[0] = &UpdateCPUhorMPI::updateBuf;
      stage[1] = &UpdateCPUhorMPI::updateMat;
      stage[2] = &UpdateCPUhorMPI::done;
    }

    int run(const int ts);
    void reset() { ind=0; }
};

class QueryCPUhorMPI: public RunBase
{
  private:
    const int idTQ;
    const int idTS;
    const bool LR;

    int ind;

    int query(const int ts);
    int done(const int ts);

    int (QueryCPUhorMPI::*stage[2])(const int ts);

  public:
    QueryCPUhorMPI(const int idTQ, const int idTS, const bool LR)
    : idTQ(idTQ), idTS(idTS), LR(LR), ind(0)
    {
      stage[0] = &QueryCPUhorMPI::query;
      stage[1] = &QueryCPUhorMPI::done;
    }

    int run(const int ts);
    void reset() { ind=0; }
};
#endif //NDS>1

#if NDS>1
class UpdateCPUhorMPImain: public RunBase
{
  private:
    MatrixCPU<realX> *mC;
    const bool LR;
    const int procId;
    const int tag;
    int ind;

    int updateNode(const int timeStep);
    int queryNode(const int timeStep);
    int done(const int timeStep);

    int (UpdateCPUhorMPImain::*stage[3])(const int timeStep);

  public:
    UpdateCPUhorMPImain(MatrixCPU<realX> &mC, const bool LR, const int procId, const int tag)
    : mC(&mC), LR(LR), procId(procId), tag(tag), ind(0)
    {
      stage[0] = &UpdateCPUhorMPImain::updateNode;
      stage[1] = &UpdateCPUhorMPImain::queryNode;
      stage[2] = &UpdateCPUhorMPImain::done;
    }

    int run(const int ts);
    void reset() { ind=0; }
};

class UpdateCPUverMPImain: public RunBase
{
  private:
    MatrixCPU<realX> *mC;
    const bool TB;
    const int procId;
    const int idTfrom;
    const int tag;
    const int threadIndex;
    int ind;

    int updateNode(const int timeStep);
    int queryNode(const int timeStep);
    int done(const int timeStep);

    int (UpdateCPUverMPImain::*stage[3])(const int timeStep);

    bool queryRows(const int ts);

  public:
    UpdateCPUverMPImain(MatrixCPU<realX> &mC, const bool TB, const int procId, const int idTfrom, const int tag, const int threadIndex)
    : mC(&mC), TB(TB), procId(procId), idTfrom(idTfrom), tag(tag), threadIndex(threadIndex), ind(0)
    {
      stage[0] = &UpdateCPUverMPImain::updateNode;
      stage[1] = &UpdateCPUverMPImain::queryNode;
      stage[2] = &UpdateCPUverMPImain::done;
    }

    int run(const int ts);
    void reset() { ind=0; }
};

class QueryCPUverMPI: public RunBase
{
  private:
    const int threadIndex;

    int ind;

    int query(const int ts);
    int done(const int ts);

    int (QueryCPUverMPI::*stage[2])(const int ts);

  public:
    QueryCPUverMPI(const int threadIndex)
    : threadIndex(threadIndex), ind(0)
    {
      stage[0] = &QueryCPUverMPI::query;
      stage[1] = &QueryCPUverMPI::done;
    }

    int run(const int ts);
    void reset() { ind=0; }
};
#endif //NDS>1

void saveResCPU(MatrixCPU<realX> *xG, const int nGPU,
  const int procId, const int threadId, const int ts, const double time, const double energy);
#endif //GCP<100

#endif
