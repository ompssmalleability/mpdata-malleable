#ifndef MEMCPU_H
#define MEMCPU_H

#include "memGPU.h"

#if GCP<100

#include <cstring>

struct cpuB
{
  int x;
  int y;
};

class MetaDataCPU
{
  private:
  public:
    int nT;  //number of threads that calculate the CPU domain across y per node
    int n;   //size of matrix per CPU/group across y
    int mT;  //number of threads that calculate the CPU domain across x per node
    int mF;  //size of matrix per CPU/group across x
    int mD;  //size of matrix per CPU/group across x
    int mFl; //mF * (LDIM+6)
    int mDl; //mD * (LDIM+6)
    int l;   //LDIM+6
    int sizeF;  //size of matrix per CPU/group in bytes for float
    int sizeD;  //size of matrix per CPU/group in bytes for double

    int yS;  //number of threads per matrix across y
    int xS;  //number of threads per matrix across x

    int yN;  //number of nodes across y
    int xN;  //number of nodes across x

    int matrixCount;

    int xTPG;  //number of CPU matrices across x per node
    int yTPG;  //number of CPU matrices across y per node

    cpuB nBV;  //number of vertical blocks for data copying
    cpuB nBH;  //number of horizontal blocks for data copying

    cpuB cpu0B;
    cpuB cpu1B;
    cpuB cpu2B;
    cpuB cpu3B;

    std::vector<int> tToM;
    std::vector<int> iOffset;
    std::vector<int> jOffset;

    MetaDataCPU();
    ~MetaDataCPU();
};

extern MetaDataCPU mdCPU;

#if NDS>1
struct BufferNodeCPU
{
 // realX *bufH;
  volatile int str;//

  MPI_Request *req;//

  int passed;//
  volatile int passed2;//

  BufferNodeCPU()
  : str(0), req(0), passed(0), passed2(0)
  {}

  void create(const int size)
  {
    if(req==0)
      req = new MPI_Request[2];
    #if GCP>0
      if(bufH==0) cudaHostAlloc(&bufH, size, cudaHostAllocPortable);
    #else
 //     if(bufH==0) bufH = new realX[ size/sizeof(realX) ];
    #endif
  }

  ~BufferNodeCPU()
  {
//    MPI_Barrier(MPI_COMM_WORLD);
    if(req) delete [] req;
    #if GCP>0
      if(bufH) cudaFreeHost(bufH);
    #else
 //     if(bufH) delete [] bufH;
    #endif
  }

  bool pass(bool &first, const int timeStep)
  {
    first=(passed>=1+timeStep*100)? true : false;
    if(passed<1+timeStep*100 && str>=1+timeStep*100)
    {
      passed=1+timeStep*100;
      first=true;
      return true;
    }
    else return false;
  }

  bool pass2(bool &first, const int timeStep)
  {
    first=(passed2<1+100*timeStep)? false : true;
    int flag=1;
    if( passed2<1+100*timeStep ) MPI_Test(&req[0], &flag, MPI_STATUS_IGNORE);
    if(!first && flag)
    {
      passed2=1+100*timeStep;
      first=true;
      return true;
    }
    else return false;
  }

  bool pass3(const int timeStep)
  {
    int flag=0;
    if(passed2<1+100*timeStep)//czy to potrzebne ?
    {
      MPI_Test(&req[0], &flag, MPI_STATUS_IGNORE);
    }
    else
    {
      flag=1;
      return true;
    }
    if(flag==1)
    {
      passed2=1+100*timeStep;
    }
    return flag==1;
  }
/*
  void reset()
  {
//    passed = false;
//    passed2 = false;
  }
*/
  void strSync(const int timeStep)
  {
    while(str<1+100*timeStep);
  }
};

struct UpdateNodeCPU
{
  int sizeLRRL;
  int sizeTBBT;

  BufferNodeCPU srcBufLR;
  BufferNodeCPU dstBufLR;
  BufferNodeCPU srcBufRL;
  BufferNodeCPU dstBufRL;

  BufferNodeCPU srcBufTB;
  BufferNodeCPU dstBufTB;
  BufferNodeCPU srcBufBT;
  BufferNodeCPU dstBufBT;

  void create(int sLRRL, int sTBBT,
              bool l, bool r, bool t, bool b);

  UpdateNodeCPU()
  {}
/*
  void resetLRRL()
  {
    srcBufLR.reset();
    dstBufLR.reset();
    srcBufRL.reset();
    dstBufRL.reset();
  }

  void resetTBBT()
  {
    srcBufTB.reset();
    dstBufTB.reset();
    srcBufBT.reset();
    dstBufBT.reset();
  }
*/
};
#endif

template<typename T> inline int mCPU() { return 0; }
template<> inline int mCPU<float>() { return mdCPU.mF; }
template<> inline int mCPU<double>() { return mdCPU.mD; }

template<typename T> inline int mlCPU() { return 0; }
template<> inline int mlCPU<float>() { return mdCPU.mFl; }
template<> inline int mlCPU<double>() { return mdCPU.mDl; }

template<typename T> inline int sizeCPU() { return 0; }
template<> inline int sizeCPU<float>() { return mdCPU.sizeF; }
template<> inline int sizeCPU<double>() { return mdCPU.sizeD; }

template<typename T>
class MatrixCPU
{
  private:
  public:
    T *ptr;
    T *fst;

    int indexL;
    int indexR;
    int indexT;
    int indexB;

    T *srcTB;
    T *dstTB;
    T *srcBT;
    T *dstBT;

    int n;
    int m;

    int sizeV; //size of vertical buffers: top<->bottom, bottom<->top

    int lTiM;
    int fTiM;

  #if NDS>1
    UpdateNodeCPU *updateNode;
  #endif

    MatrixCPU()
    : ptr(0)
    #if NDS>1
    , updateNode(0)
    #endif
    {}

    void init(int i, bool GPUaboveCPU
    #if(NDS==1 || GCP==0)
      __attribute__((unused))
    #endif
      , int nodes
    #if(NDS==1)
      __attribute__((unused))
    #endif
      , int procId)
    {
      const int sizeN = mdCPU.nT*mdCPU.yS;
      const int sizeM = mdCPU.mT*mdCPU.xS;
    #if GCP>0
      cudaHostAlloc(&ptr, sizeCPU<T>(), cudaHostAllocPortable);
    #else
      ptr = new T[ sizeCPU<T>()/sizeof(T) ];
      
     // std::cout << procId <<") sizeCPU<T>()/sizeof(T)="<<sizeCPU<T>()/sizeof(T)<<"\n";
      
    #endif
      memset(ptr, 0, sizeCPU<T>());//without first touch policy
      fst = ptr+((3*mlCPU<T>()+3+3*mCPU<T>())+
                  (
                    (AGMC-
                      (
                        (
                          (int64_t)(ptr+(3*mlCPU<T>()+3+3*mCPU<T>()))
                        )%AGMC
                      )
                    )/sizeof(T)
                  )
                );

      indexR=(i+mdCPU.yTPG)%(mdCPU.yTPG*mdCPU.xTPG);
      indexL=(i+mdCPU.yTPG*mdCPU.xTPG-mdCPU.yTPG)%(mdCPU.yTPG*mdCPU.xTPG);
      indexB=i/mdCPU.yTPG*mdCPU.yTPG+(i+1)%mdCPU.yTPG;
      indexT=i/mdCPU.yTPG*mdCPU.yTPG+(i+mdCPU.yTPG-1)%mdCPU.yTPG;

      sizeV = 3*mlCPU<T>()*sizeof(T);

      if(GCP<100 && indexB<=i && procId%mdCPU.yN==mdCPU.yN-1)
      {
      #if GCP>0
        n = NDIM - (mdCPU.yN-1)*(mdCPU.yTPG*sizeN+mdGPU.yTPG*mdGPU.nT*mdGPU.yS) -
                   (mdCPU.yTPG-1)*sizeN-mdGPU.yTPG*mdGPU.nT*mdGPU.yS;
      #else
        n = NDIM - (mdCPU.yN-1)*(mdCPU.yTPG*sizeN) -
                                (mdCPU.yTPG-1)*sizeN;
      #endif
      }
      else
      {
        n = sizeN;
      }

      if(indexR<=i && procId>=NDS_V-mdCPU.yN)
      {
      #if GCP>0
        m = MDIM - (mdCPU.xN-1)*(mdGPU.xTPG*mdGPU.mT*mdGPU.xS) -
                                (mdCPU.xTPG-1)*sizeM;
      #else
        m = MDIM - (mdCPU.xN-1)*(mdCPU.xTPG   *sizeM) -
                                (mdCPU.xTPG-1)*sizeM;
      #endif
      }
      else if(indexR<=i)
      {
      #if GCP>0
        m = mdGPU.xTPG*mdGPU.mT*mdGPU.xS-(mdCPU.xTPG-1)*sizeM;
      #else
        m = sizeM;
      #endif
      }
      else
      {
        m = sizeM;
      }

      dstTB = fst-(3*mlCPU<T>()+3+3*mCPU<T>());
      srcTB = fst+((n-3)*mlCPU<T>()-3-3*mCPU<T>());

      dstBT = fst+(n*mlCPU<T>()-3-3*mCPU<T>());
      srcBT = fst-(0*mlCPU<T>()+3+3*mCPU<T>());

      lTiM=0; //the last thread in matrix
      for(int I=0; I<THS; ++I)
      {
        if( mdCPU.tToM[I]==i )
        {
          if(I>lTiM) lTiM=I;
        }
      }

      fTiM=THS; //the first thread in matrix
      for(int I=0; I<THS; ++I)
      {
        if( mdCPU.tToM[I]==i )
        {
          if(I<fTiM) fTiM=I;
        }
      }

    #if NDS>1
      if(nodes==1)
      {
        updateNode = new UpdateNodeCPU();
        bool left=false, right=false, top=false, bottom=false;
        if(procId<NDS_V-NDS_V/TDS && indexR<=i) right=true;
        if(procId>=NDS_V/TDS && indexL>=i) left=true;
      #if GCP>0
        if(procId%(NDS_V/TDS)>0 && !GPUaboveCPU && indexT>=i) top=true;
        if(procId%(NDS_V/TDS)<NDS_V/TDS-1 && GPUaboveCPU && indexB<=i) bottom=true;
      #else
        if(procId%(NDS_V/TDS)>0 && indexT>=i) top=true;
        if(procId%(NDS_V/TDS)<NDS_V/TDS-1 && indexB<=i) bottom=true;
      #endif
        updateNode->create(n*3*(LDIM+6), sizeV/sizeof(T),
                           left, right, top, bottom);
      }
    #endif
    }

    ~MatrixCPU()
    {
        
    #if GCP>0
      if(ptr) cudaFreeHost(ptr);
    #else
      if(ptr) delete [] ptr;
    #endif
    #if NDS>1
      //printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
      if(updateNode) delete updateNode;
    #endif
    }
};
#endif //GCP<100

#endif
