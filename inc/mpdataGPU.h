#ifndef MPDATAGPU_H
#define MPDATAGPU_H

#include "memCPU.h"

#if GCP>0
__device__ __host__
#endif
double initValue(const int i, const int j, const int k);

class RunBase
{
  private:
  public:
    virtual int run(const int ts __attribute__((unused)))
    {
      return 0;
    }

    virtual void reset()
    {}

    virtual ~RunBase()
    {}
};

#if GCP>0

#if(GCP>0 && GCP<100)
struct UpdateGPUCPU
{
  int bodyRow;
  int fullRow;
  int bodySize;
  int fullSize;
  int offset;
  int matrix;
  int idGPU;
#if GCU==0
  realX *GPUtoCPU;
#endif
  realX *CPUtoCPU;
#if GCU==0
  realX *GPUtoGPU;
#endif
  realX *CPUtoGPU;

  int previous;

  dim3 tB;
  dim3 nB;
  dim3 nB2;

  UpdateGPUCPU()
  :
#if GCU==0
  GPUtoCPU(0),
#endif
  CPUtoCPU(0),
#if GCU==0
  GPUtoGPU(0),
#endif
  CPUtoGPU(0)
  {}

  void create();

  ~UpdateGPUCPU();
};

void cpyGPUtoBuf_(UpdateGPUCPU *bufGPU,
                 MatrixGPU<realX> *xG, bool GPUaboveCPU);

void cpyBufToGPU_(UpdateGPUCPU *bufGPU,
                 MatrixGPU<realX> *xG, bool GPUaboveCPU);

void cpyGPUCPU_(UpdateGPUCPU *bufGPU,
               UpdateGPUCPU *bufCPU,
               MatrixGPU<realX> *xG,
               MatrixCPU<realX> *xC, bool GPUaboveCPU);
#endif

void initGPU();

#if NDS>1
void cpyKrnLRtoMat(MatrixGPU<realX> &mG);
void cpyKrnRLtoMat(MatrixGPU<realX> &mG);
void cpyKrnTBtoMat(MatrixGPU<realX> &mG);
void cpyKrnBTtoMat(MatrixGPU<realX> &mG);
#endif

void initKernelGPU(MatrixGPU<realX> *xG,
            MatrixGPU<realV1> *v1G,
            MatrixGPU<realV2> *v2G,
            MatrixGPU<realV3> *v3G,
            MatrixGPU<realH> *hG,
            MatrixGPU<realX> *xPG,
            MatrixGPU<realV1P> *v1PG,
            MatrixGPU<realV2P> *v2PG,
            MatrixGPU<realV3P> *v3PG,
            MatrixGPU<realCP> *cpG,
            MatrixGPU<realCN> *cnG,
            int procId,
            const int offYGPU, const int offXGPU);

//*** Execute kernels on GPUs **************************************************
class RunGPU: public RunBase
{
  private:
    MatrixGPU<realX> *xG;
    MatrixGPU<realV1> *v1G;
    MatrixGPU<realV2> *v2G;
    MatrixGPU<realV3> *v3G;
    MatrixGPU<realH> *hG;
    MatrixGPU<realX> *xPG;
    MatrixGPU<realV1P> *v1PG;
    MatrixGPU<realV2P> *v2PG;
    MatrixGPU<realV3P> *v3PG;
    MatrixGPU<realCP> *cpG;
    MatrixGPU<realCN> *cnG;
    int procId;

  public:
    RunGPU(MatrixGPU<realX> *xG,
           MatrixGPU<realV1> *v1G,
           MatrixGPU<realV2> *v2G,
           MatrixGPU<realV3> *v3G,
           MatrixGPU<realH> *hG,
           MatrixGPU<realX> *xPG,
           MatrixGPU<realV1P> *v1PG,
           MatrixGPU<realV2P> *v2PG,
           MatrixGPU<realV3P> *v3PG,
           MatrixGPU<realCP> *cpG,
           MatrixGPU<realCN> *cnG,
           int pId)
    : xG(xG), v1G(v1G), v2G(v2G), v3G(v3G), hG(hG),
      xPG(xPG), v1PG(v1PG), v2PG(v2PG), v3PG(v3PG),
      cpG(cpG), cnG(cnG), procId(pId)
    {}

    int run(const int ts);
    void reset() {}
};

//*** Update halo between left and right matrices within a single node *********
class UpdateGPUhor: public RunBase
{
  private:
  #if HSPG==0
    const int idGPUto;
    const int idSto;
  #endif

    const int idGPUfrom;
    const int idSfrom;
    realX *fstTo;
    realX *fstFrom;
    const int n;
    const int m;
#if HSPG==1
    const bool LR;
#endif
    int ind;

    const int tmp;

    int update();
    int query();
    int done();

    int (UpdateGPUhor::*stage[3])();

  public:
    UpdateGPUhor(const int tmp,
  #if HSPG==0
    const int idGPUto, const int idSto,
  #endif
    const int idGPUfrom, const int idSfrom,
    realX *fstTo, realX *fstFrom,
    const int n, const int m
  #if HSPG==1
    , const bool LR
  #endif
    )
    :
    #if HSPG==0
      idGPUto(idGPUto), idSto(idSto),
    #endif
      idGPUfrom(idGPUfrom), idSfrom(idSfrom),
      fstTo(fstTo), fstFrom(fstFrom), n(n), m(m)
    #if HSPG==1
      , LR(LR)
    #endif
      , ind(0)
      , tmp(tmp)
    {
      stage[0] = &UpdateGPUhor::update;
      stage[1] = &UpdateGPUhor::query;
      stage[2] = &UpdateGPUhor::done;
    }

    int run(const int ts __attribute__((unused)));
    void reset() { ind=0; }
};

class UpdateGPUver: public RunBase
{
  private:
  #if HSPG==0
    const int idGPUto;
    const int idSto;
  #endif
    const int idGPUfrom;
    const int idSfrom;
  #if CPYG>0
    realX *dstTo;
    realX *srcFrom;
    const int sizeV;
    #if HSPG==0
      realX *dstFrom;
      realX *srcTo;
    #endif
  #else
    realX *fstTo;
    realX *fstFrom;
    const int n;
    const int m;
  #endif
    const bool TB;
    int ind;

    int update();
    int query();
    int done();

    int (UpdateGPUver::*stage[3])();

    bool queryRows();

  public:
    UpdateGPUver(
  #if HSPG==0
    const int idGPUto, const int idSto,
  #endif
    const int idGPUfrom, const int idSfrom
  #if CPYG>0
    , realX *dstTo, realX *srcFrom, const int sizeV
    #if HSPG==0
      , realX *dstFrom, realX *srcTo
    #endif
  #else
    , realX *fstTo, realX *fstFrom, const int n, const int m
  #endif
    , const bool TB
    )
    :
    #if HSPG==0
      idGPUto(idGPUto), idSto(idSto),
    #endif
      idGPUfrom(idGPUfrom), idSfrom(idSfrom)
    #if CPYG>0
      , dstTo(dstTo), srcFrom(srcFrom), sizeV(sizeV)
      #if HSPG==0
        , dstFrom(dstFrom), srcTo(srcTo)
      #endif
    #else
      , fstTo(fstTo), fstFrom(fstFrom), n(n), m(m)
    #endif
      , TB(TB), ind(0)
    {
      stage[0] = &UpdateGPUver::update;
      stage[1] = &UpdateGPUver::query;
      stage[2] = &UpdateGPUver::done;
    }

    int run(const int ts __attribute__((unused)));
    void reset() { ind=0; }
};

#if NDS>1
class UpdateGPUhorMPI: public RunBase
{
  private:
    MatrixGPU<realX> *mG;
    const bool LR;
    const int procId;
    const int tag;
    int ind;

    int updateBuf();
    int done();

    int updateNode()
    {
      cudaSetDevice(mG->idGPU);

      if(LR)
      {
        if(cudaStreamQuery(mG->updateNode->srcBufLR.str)==cudaSuccess)
        {
        #if GDR==0
          MPI_Isend(mG->updateNode->srcBufLR.bufH,
                    mG->updateNode->sizeLRRL,
                    MPI_realX, procId+NDS/TDS, tag, MPI_COMM_WORLD,
                    &mG->updateNode->srcBufLR.req[1]);

          MPI_Irecv(mG->updateNode->dstBufLR.bufH,
                    mG->updateNode->sizeLRRL,
                    MPI_realX, procId+NDS/TDS, tag, MPI_COMM_WORLD,
                    &mG->updateNode->dstBufLR.req[0]);
        #else
          MPI_Isend(mG->updateNode->srcBufLR.buf,
                    mG->updateNode->sizeLRRL,
                    MPI_realX, procId+NDS/TDS, tag, MPI_COMM_WORLD,
                    &mG->updateNode->srcBufLR.req[1]);

          MPI_Irecv(mG->updateNode->dstBufLR.buf,
                    mG->updateNode->sizeLRRL,
                    MPI_realX, procId+NDS/TDS, tag, MPI_COMM_WORLD,
                    &mG->updateNode->dstBufLR.req[0]);
        #endif
          ++ind;
        }
      }
      else
      {
        if(cudaStreamQuery(mG->updateNode->srcBufRL.str)==cudaSuccess)
        {
        #if GDR==0
          MPI_Isend(mG->updateNode->srcBufRL.bufH,
                    mG->updateNode->sizeLRRL,
                    MPI_realX, procId-NDS/TDS, tag, MPI_COMM_WORLD,
                    &mG->updateNode->srcBufRL.req[1]);

          MPI_Irecv(mG->updateNode->dstBufRL.bufH,
                    mG->updateNode->sizeLRRL,
                    MPI_realX, procId-NDS/TDS, tag, MPI_COMM_WORLD,
                    &mG->updateNode->dstBufRL.req[0]);
        #else
          MPI_Isend(mG->updateNode->srcBufRL.buf,
                    mG->updateNode->sizeLRRL,
                    MPI_realX, procId-NDS/TDS, tag, MPI_COMM_WORLD,
                    &mG->updateNode->srcBufRL.req[1]);

          MPI_Irecv(mG->updateNode->dstBufRL.buf,
                    mG->updateNode->sizeLRRL,
                    MPI_realX, procId-NDS/TDS, tag, MPI_COMM_WORLD,
                    &mG->updateNode->dstBufRL.req[0]);
        #endif
          ++ind;
        }
      }
      return 0;
    }

    int updateMat()
    {
      cudaSetDevice(mG->idGPU);

      if(LR)
      {
        if(mG->updateNode->dstBufLR.pass3())
        {
          cpyKrnLRtoMat(*mG);
          ++ind;
        }
      }
      else
      {
        if(mG->updateNode->dstBufRL.pass3())
        {
          cpyKrnRLtoMat(*mG);
          ++ind;
        }
      }
      return 0;
    }

    int queryMat()
    {
      int finished=0;
      if(LR)
      {
        int flag;
        MPI_Test(&mG->updateNode->srcBufLR.req[1], &flag, MPI_STATUS_IGNORE);
        if(cudaStreamQuery(mG->updateNode->dstBufLR.str)==cudaSuccess && flag==1)
        {
          std::cout << procId<<"zakonczylem LR GPU MPI\n";
          ++ind;
          finished=1;
        }
      }
      else
      {
        int flag;
        MPI_Test(&mG->updateNode->srcBufRL.req[1], &flag, MPI_STATUS_IGNORE);
        if(cudaStreamQuery(mG->updateNode->dstBufRL.str)==cudaSuccess && flag==1)
        {
          std::cout << procId<<"zakonczylem RL GPU MPI\n";
          ++ind;
          finished=1;
        }
      }
      return finished;
    }

    int (UpdateGPUhorMPI::*stage[5])();

  public:
    UpdateGPUhorMPI(MatrixGPU<realX> &mG, const bool LR,
      const int procId, const int tag)
    : mG(&mG), LR(LR), procId(procId), tag(tag), ind(0)
    {
      stage[0] = &UpdateGPUhorMPI::updateBuf;
      stage[1] = &UpdateGPUhorMPI::updateNode;
      stage[2] = &UpdateGPUhorMPI::updateMat;
      stage[3] = &UpdateGPUhorMPI::queryMat;
      stage[4] = &UpdateGPUhorMPI::done;
    }

    int run(const int ts __attribute__((unused)));
    void reset() { ind=0; }
};

class UpdateGPUverMPI: public RunBase
{
  private:
    MatrixGPU<realX> *mG;
    const bool TB;
    const int procId;
    const int tag;
    int ind;

  #if GDR==0
    int updateBuf();
  #endif //GDR==0
    int done();

    int updateNode()
    {
    #if GDR==1
      int finished=0;
    #endif
      cudaSetDevice(mG->idGPU);

      if(TB)
      {
      #if GDR==0
        if(cudaStreamQuery(mG->updateNode->srcBufTB.str)==cudaSuccess)
      #else
        bool err=true;
        const int iteration = -GPN*SPG/TGP;
        const int end = -mG->lSiM+GPN*SPG/TGP*(mdGPU.xS-1)+1;

        for(int I=mG->lSiM+iteration; -I<end; I+=iteration)
        {
          err &= (cudaEventQuery( mdGPU.event[ I ] )==cudaSuccess);
        }
        err &= (cudaEventQuery( mdGPU.event[ mG->lSiM ] )==cudaSuccess);

        if(err)
      #endif //GDR==0
        {
        #if GDR==0
          MPI_Isend(mG->updateNode->srcBufTB.bufH,
                    mG->updateNode->sizeTBBT,
                    MPI_realX, procId+1, tag, MPI_COMM_WORLD,
                    &mG->updateNode->srcBufTB.req[1]);

          MPI_Irecv(mG->updateNode->dstBufTB.bufH,
                    mG->updateNode->sizeTBBT,
                    MPI_realX, procId+1, tag, MPI_COMM_WORLD,
                    &mG->updateNode->dstBufTB.req[0]);
        #else
          MPI_Isend(mG->srcTB,
                    mG->updateNode->sizeTBBT,
                    MPI_realX, procId+1, tag, MPI_COMM_WORLD,
                    &mG->updateNode->srcBufTB.req[1]);

          MPI_Irecv(mG->dstBT,
                    mG->updateNode->sizeTBBT,
                    MPI_realX, procId+1, tag, MPI_COMM_WORLD,
                    &mG->updateNode->dstBufTB.req[0]);
          finished=1;
        #endif
          ++ind;
        }
      }
      else
      {
      #if GDR==0
        if(cudaStreamQuery(mG->updateNode->srcBufBT.str)==cudaSuccess)
      #else
        bool err=true;
        const int iteration = GPN*SPG/TGP;
        const int end = mG->fSiM+GPN*SPG/TGP*(mdGPU.xS-1)+1;

        for(int I=mG->fSiM+iteration; I<end; I+=iteration)
        {
          err &= (cudaEventQuery( mdGPU.event[ I ] )==cudaSuccess);
        }
        err &= (cudaEventQuery( mdGPU.event[ mG->fSiM ] )==cudaSuccess);

        if(err)
      #endif //GDR==0
        {
        #if GDR==0
          MPI_Isend(mG->updateNode->srcBufBT.bufH,
                    mG->updateNode->sizeTBBT,
                    MPI_realX, procId-1, tag, MPI_COMM_WORLD,
                    &mG->updateNode->srcBufBT.req[1]);

          MPI_Irecv(mG->updateNode->dstBufBT.bufH,
                    mG->updateNode->sizeTBBT,
                    MPI_realX, procId-1, tag, MPI_COMM_WORLD,
                    &mG->updateNode->dstBufBT.req[0]);
        #else
          MPI_Isend(mG->srcBT,
                    mG->updateNode->sizeTBBT,
                    MPI_realX, procId-1, tag, MPI_COMM_WORLD,
                    &mG->updateNode->srcBufBT.req[1]);

          MPI_Irecv(mG->dstTB,
                    mG->updateNode->sizeTBBT,
                    MPI_realX, procId-1, tag, MPI_COMM_WORLD,
                    &mG->updateNode->dstBufBT.req[0]);
          finished=1;
        #endif
          ++ind;
        }
      }
    #if GDR==0
      return 0;
    #else
      return finished;
    #endif
    }

  #if GDR==0
    int updateMat()
    {
      cudaSetDevice(mG->idGPU);

      if(TB)
      {
        if(mG->updateNode->dstBufTB.pass3())
        {
          cpyKrnTBtoMat(*mG);
          ++ind;
        }
      }
      else
      {
        if(mG->updateNode->dstBufBT.pass3())
        {
          cpyKrnBTtoMat(*mG);
          ++ind;
        }
      }
      return 0;
    }
  #endif //GDR==0

    int queryMat()
    {
      int finished=0;
      if(TB)
      {
        int flag;
        MPI_Test(&mG->updateNode->srcBufTB.req[1], &flag, MPI_STATUS_IGNORE);
        if(
        #if GDR==0
          cudaStreamQuery(mG->updateNode->dstBufTB.str)==cudaSuccess &&
        #else
          mG->updateNode->dstBufTB.pass3() &&
        #endif //GDR==0
          flag==1)
        {
          ++ind;
          finished=1;
        }
      }
      else
      {
        int flag;
        MPI_Test(&mG->updateNode->srcBufBT.req[1], &flag, MPI_STATUS_IGNORE);
        if(
        #if GDR==0
          cudaStreamQuery(mG->updateNode->dstBufBT.str)==cudaSuccess &&
        #else
          mG->updateNode->dstBufBT.pass3() &&
        #endif //GDR==0
          flag==1)
        {
          ++ind;
          finished=1;
        }
      }
      return finished;
    }

  #if GDR==0
    int (UpdateGPUverMPI::*stage[5])();
  #else
    int (UpdateGPUverMPI::*stage[3])();
  #endif

  public:
    UpdateGPUverMPI(MatrixGPU<realX> &mG, const bool TB,
      const int procId, const int tag)
    : mG(&mG), TB(TB), procId(procId), tag(tag), ind(0)
    {
    #if GDR==0
      stage[0] = &UpdateGPUverMPI::updateBuf;
      stage[1] = &UpdateGPUverMPI::updateNode;
      stage[2] = &UpdateGPUverMPI::updateMat;
      stage[3] = &UpdateGPUverMPI::queryMat;
      stage[4] = &UpdateGPUverMPI::done;
    #else
      stage[0] = &UpdateGPUverMPI::updateNode;
      stage[1] = &UpdateGPUverMPI::queryMat;
      stage[2] = &UpdateGPUverMPI::done;
    #endif
    }

    int run(const int ts __attribute__((unused)));
    void reset() { ind=0; }
};
#endif //NDS>1
//******************************************************************************

void saveResGPU(MatrixGPU<realX> *xG, const int nCPU,
  const int procId, const int ts, const double time, const double energy);
#endif //GCP>0

#endif
