#ifndef MEMGPU_H
#define MEMGPU_H

extern int NDS_V;

#include "../ETM/etmLib.h"

#if NDS>1
#include <mpi.h>
#endif

#if GCP<100
#include <omp.h>
#endif

#if(PRC==1)||(PRC==0&&X==1)
#define realX float
#define MPI_realX MPI_FLOAT
#else
#define realX double
#define MPI_realX MPI_DOUBLE
#endif

#if(PRC==1)||(PRC==0&&V1==1)
#define realV1 float
#else
#define realV1 double
#endif

#if(PRC==1)||(PRC==0&&V2==1)
#define realV2 float
#else
#define realV2 double
#endif

#if(PRC==1)||(PRC==0&&V3==1)
#define realV3 float
#else
#define realV3 double
#endif

#if(PRC==1)||(PRC==0&&H==1)
#define realH float
#else
#define realH double
#endif

#if(PRC==1)||(PRC==0&&V1P==1)
#define realV1P float
#else
#define realV1P double
#endif

#if(PRC==1)||(PRC==0&&V2P==1)
#define realV2P float
#else
#define realV2P double
#endif

#if(PRC==1)||(PRC==0&&V3P==1)
#define realV3P float
#else
#define realV3P double
#endif

#if(PRC==1)||(PRC==0&&CP==1)
#define realCP float
#else
#define realCP double
#endif

#if(PRC==1)||(PRC==0&&CN==1)
#define realCN float
#else
#define realCN double
#endif

#if GCP>0
#include <cuda.h>
#include <cuda_profiler_api.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#endif

int divT(const int l, const int r);
int rndT(const int l, const int r);

#if GCP>0
class MetaDataGPU
{
  private:
  public:
    int nT;  //number of threads that calculate the GPU domain across y per node
    int n;   //size of matrix per GPU/stream across y
    int mT;  //number of threads that calculate the GPU domain across x per node
    int mF;  //size of matrix per GPU/stream across x
    int mD;  //size of matrix per GPU/stream across x
    int mFl; //mF * (LDIM+6)
    int mDl; //mD * (LDIM+6)
    int l;   //LDIM+6
    int sizeF;  //size of matrix per GPU/stream in bytes for float
    int sizeD;  //size of matrix per GPU/stream in bytes for double

    int yS;  //number of streams per matrix across y
    int xS;  //number of streams per matrix across x

    int yN;  //number of nodes across y
    int xN;  //number of nodes across x

    int matrixCount;
    int streamCount;
    cudaStream_t *stream;
    cudaEvent_t *event;

    int xTPG;  //number of GPU matrices across x per node
    int yTPG;  //number of GPU matrices across y per node

    dim3 tPB;  //threads per block for data copying
    dim3 nBV;  //number of vertical blocks for data copying
    dim3 nBH;  //number of horizontal blocks for data copying

    dim3 krn0T;
    dim3 krn1T;
    dim3 krn2T;
    dim3 krn3T;

    dim3 krn0B;
    dim3 krn1B;
    dim3 krn2B;
    dim3 krn3B;

    std::vector<int> sToM;
    std::vector<int> iOffset;
    std::vector<int> jOffset;

    MetaDataGPU();
    ~MetaDataGPU();
};

extern MetaDataGPU mdGPU;

#if NDS>1

/*
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
     std::cout << "NOT OK\n";
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
   else
   {
     std::cout << "OK\n";
     fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
   }
}
*/

struct BufferNodeGPU
{
  realX *buf;
  realX *bufH;
  cudaStream_t str;

  MPI_Request *req;
/*
  bool passed;
  bool passed2;
*/
  BufferNodeGPU()
  : buf(0), bufH(0), str(0), req(0)//, passed(false), passed2(false)
  {}

  void create(const int size)
  {
    if(buf==0) cudaMallocManaged(&buf, size);
    cudaDeviceSynchronize();
    if(str==0) cudaStreamCreate(&str);
    if(req==0) req = new MPI_Request[2];
  #if GDR==0
    if(bufH==0) cudaHostAlloc(&bufH, size, cudaHostAllocPortable);
  #endif
  }

  ~BufferNodeGPU()
  {
    if(req) delete [] req;
    if(buf) cudaFree(buf);
    if(str) cudaStreamDestroy(str);
    if(bufH) cudaFreeHost(bufH);
  }
/*
  bool pass(bool &first)
  {
    first=passed;
    if(!passed && cudaStreamQuery(str)==cudaSuccess)
    {
      passed=true;
      first=passed;
      return true;
    }
    else return false;
  }
*/
/*
  bool pass2(bool &first)
  {
    first=passed2;
    int flag=0;
    MPI_Test(&req[0], &flag, MPI_STATUS_IGNORE);
    if(!passed2 && flag)
    {
      passed2=true;
      first=passed2;
      return true;
    }
    else return false;
  }
*/

  bool pass3()
  {
    int flag=0;
    MPI_Test(&req[0], &flag, MPI_STATUS_IGNORE);
    return flag==1;
  }
/*
  void reset()
  {
    passed = false;
    passed2 = false;
  }*/
};

struct UpdateNodeGPU
{
  int idGPU;
  int sizeLRRL;
  int sizeTBBT;

  BufferNodeGPU srcBufLR;
  BufferNodeGPU dstBufLR;
  BufferNodeGPU srcBufRL;
  BufferNodeGPU dstBufRL;

  BufferNodeGPU srcBufTB;
  BufferNodeGPU dstBufTB;
  BufferNodeGPU srcBufBT;
  BufferNodeGPU dstBufBT;

  void create(int idGPU, int sLRRL, int sTBBT,
              bool l, bool r, bool t, bool b, int procId);

  UpdateNodeGPU()
  {}
/*
  void resetLRRL()
  {
    srcBufLR.reset();
    dstBufLR.reset();
    srcBufRL.reset();
    dstBufRL.reset();
  }

  void resetTBBT()
  {
    srcBufTB.reset();
    dstBufTB.reset();
    srcBufBT.reset();
    dstBufBT.reset();
  }*/
};
#endif

template<typename T> inline int mGPU() { return 0; }
template<> inline int mGPU<float>() { return mdGPU.mF; }
template<> inline int mGPU<double>() { return mdGPU.mD; }

template<typename T> inline int mlGPU() { return 0; }
template<> inline int mlGPU<float>() { return mdGPU.mFl; }
template<> inline int mlGPU<double>() { return mdGPU.mDl; }

template<typename T> inline int sizeGPU() { return 0; }
template<> inline int sizeGPU<float>() { return mdGPU.sizeF; }
template<> inline int sizeGPU<double>() { return mdGPU.sizeD; }

template<typename T>
class MatrixGPU
{
  private:
  public:
    int idGPU;

    T *ptr;
    T *fst;

    int indexL;
    int indexR;
    int indexT;
    int indexB;

    T *srcTB;
    T *dstTB;
    T *srcBT;
    T *dstBT;

    int n;
    int m;

    int sizeV; //size of vertical buffers: top<->bottom, bottom<->top

    int lSiM;
    int fSiM;

  #if(GCP>0 && GCP<100)
    cudaStream_t updateGPUCPU;
    cudaEvent_t updateGPUCPUevent;
  #endif

  #if NDS>1
    UpdateNodeGPU *updateNode;
  #endif

    MatrixGPU()
    : ptr(0)
  #if(GCP>0 && GCP<100)
    , updateGPUCPU(0)
    , updateGPUCPUevent(0)
  #endif
  #if NDS>1
    , updateNode(0)
  #endif
    {}

    void init(int i, int id, bool GPUaboveCPU
    #if(NDS==1 || GCP==100)
      __attribute__((unused))
    #endif
      , int nodes
    #if(NDS==1)
      __attribute__((unused))
    #endif
      , int procId)
    {
      const int sizeN = mdGPU.nT*mdGPU.yS;
      const int sizeM = mdGPU.mT*mdGPU.xS;

      idGPU=id;
      cudaSetDevice(idGPU);
      cudaMallocManaged(&ptr, sizeGPU<T>());
      cudaDeviceSynchronize();
      cudaMemset(ptr, 0, sizeGPU<T>());

      fst = ptr+((3*mlGPU<T>()+3+3*mGPU<T>())+
                  (
                    (AGMG-
                      (
                        (
                          (int64_t)(ptr+(3*mlGPU<T>()+3+3*mGPU<T>()))
                        )%AGMG
                      )
                    )/sizeof(T)
                  )
                );

      indexR=(i+mdGPU.yTPG)%(mdGPU.yTPG*mdGPU.xTPG);
      indexL=(i+mdGPU.yTPG*mdGPU.xTPG-mdGPU.yTPG)%(mdGPU.yTPG*mdGPU.xTPG);
      indexB=i/mdGPU.yTPG*mdGPU.yTPG+(i+1)%mdGPU.yTPG;
      indexT=i/mdGPU.yTPG*mdGPU.yTPG+(i+mdGPU.yTPG-1)%mdGPU.yTPG;

      sizeV = 3*mlGPU<T>()*sizeof(T);

      if(GCP==100 && indexB<=i && procId%mdGPU.yN==mdGPU.yN-1)
      {
        n = NDIM - (mdGPU.yN-1)*(mdGPU.yTPG   *sizeN) -
                                (mdGPU.yTPG-1)*sizeN;
      }
      else
      {
        n = sizeN;
      }

      if(indexR<=i && procId>=NDS-mdGPU.yN)
      {
        m = MDIM - (mdGPU.xN-1)*(mdGPU.xTPG   *sizeM) -
                                (mdGPU.xTPG-1)*sizeM;
      }
      else
      {
        m = sizeM;
      }

      dstTB = fst-(3*mlGPU<T>()+3+3*mGPU<T>());
      srcTB = fst+((n-3)*mlGPU<T>()-3-3*mGPU<T>());

      dstBT = fst+(n*mlGPU<T>()-3-3*mGPU<T>());
      srcBT = fst-(0*mlGPU<T>()+3+3*mGPU<T>());

      lSiM=0; //the last stream in matrix
      for(int I=0; I<GPN*SPG; ++I)
      {
        if( mdGPU.sToM[I]==i )
        {
          if(I>lSiM) lSiM=I;
        }
      }

      fSiM=GPN*SPG; //the first stream in matrix
      for(int I=0; I<GPN*SPG; ++I)
      {
        if( mdGPU.sToM[I]==i )
        {
          if(I<fSiM) fSiM=I;
        }
      }
    #if(GCP>0 && GCP<100)
      cudaStreamCreate(&updateGPUCPU);//tylko macierze x i px
      cudaEventCreate(&updateGPUCPUevent);
    #endif

    #if NDS>1
      if(nodes==1)
      {
        updateNode = new UpdateNodeGPU();

        bool left=false, right=false, top=false, bottom=false;
        if(procId<NDS-NDS/TDS && indexR<=i) right=true;
        if(procId>=NDS/TDS && indexL>=i) left=true;
      #if GCP<100
        if(procId%(NDS/TDS)>0 && GPUaboveCPU && indexT>=i) top=true;
        if(procId%(NDS/TDS)<NDS/TDS-1 && !GPUaboveCPU && indexB<=i) bottom=true;
      #else
        if(procId%(NDS/TDS)>0 && indexT>=i) top=true;
        if(procId%(NDS/TDS)<NDS/TDS-1 && indexB<=i) bottom=true;
      #endif
        updateNode->create(idGPU, n*3*(LDIM+6), sizeV/sizeof(T),
                           left, right, top, bottom, procId);
      }
    #endif
    }

    ~MatrixGPU()
    {
      cudaSetDevice(idGPU);
      cudaDeviceSynchronize();
      if(ptr) cudaFree(ptr);//tu jakis warunek?
    #if(GCP>0 && GCP<100)
      if(updateGPUCPU) cudaStreamDestroy(updateGPUCPU);
      if(updateGPUCPUevent) cudaEventDestroy(updateGPUCPUevent);
    #endif
    #if NDS>1
      if(updateNode) delete updateNode;
    #endif
    }
};
#endif //GCP>0

#endif
