#include "mpdataCPU.h"

#if GCP<100

//-----
volatile int sigFromMain[THS]; //wystarczy by rozmiar byl mdCPU.xTPG
volatile int tQ[2][THS]; //nie wiem czy trzeba volatile

bool threadQuery(const int tId, const int val, const int ts, const int tag) {
    const int stopCriterium = ts * 100 + val;
    return tQ[tag][tId] >= stopCriterium;
}

inline void threadSig(const int tId, const int val, const int ts, const int tag = 0) {
    const int stopCriterium = ts * 100 + val;
    //#pragma omp critical
    tQ[tag][tId] = stopCriterium;
}
/*
void showMe(const int procId_deb, const int tId)
{
  std::string tmp="procId_deb="+toStr(procId_deb)+" tId="+toStr(tId)+'\n';
  for(int j=0; j<2; ++j)
  {
    for(int i=0; i<THS; ++i)
    {
      tmp+=toStr(tQ[j][i])+' ';
    }
    tmp+='\n';
  }
  tmp+='\n';
  std::cout << tmp;
}*/
//-----

#define xc(i,j,k) x_[(i)*xOffL + j + (k)*xOff]
#define v1c(i,j,k) v1_[(i)*v1OffL + j + (k)*v1Off]
#define v2c(i,j,k) v2_[(i)*v2OffL + j + (k)*v2Off]
#define v3c(i,j,k) v3_[(i)*v3OffL + j + (k)*v3Off]
#define hc(i,j,k) h_[(i)*hOffL + j + (k)*hOff]
#define xPc(i,j,k) xP_[(i)*xOffL + j + (k)*xOff]
#define v1Pc(i,j,k) v1P_[(i)*v1POffL + j + (k)*v1POff]
#define v2Pc(i,j,k) v2P_[(i)*v2POffL + j + (k)*v2POff]
#define v3Pc(i,j,k) v3P_[(i)*v3POffL + j + (k)*v3POff]
#define cpc(i,j,k) cp_[(i)*cpOffL + j + (k)*cpOff]
#define cnc(i,j,k) cn_[(i)*cnOffL + j + (k)*cnOff]

void initCpuKrn(realX * __restrict__ x_,
        realV1 * __restrict__ v1_ __attribute__ ((unused)),
        realV2 * __restrict__ v2_ __attribute__ ((unused)),
        realV3 * __restrict__ v3_ __attribute__ ((unused)),
        realH * __restrict__ h_ __attribute__ ((unused)),
        realX * __restrict__ xP_ __attribute__ ((unused)),
        const int n, const int m,
        const int blx, const int bly,
        const int thx, const int thy,
        //          const int id, const int procId,
        const int iOffset, const int jOffset,
        const int offY, const int offX) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = -3; k < LDIM + 3; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi + iOffset;
                        const int j = idJ * thx + idj + jOffset;
                        if (i < n && j < m) {
                            xc(i, j, k) = initValue(i + offY, j + offX, k);
                            xPc(i, j, k) = initValue(i + offY, j + offX, k);
                        }

                        if (i < 3 && j < m) {
                            xc(i - 3, j, k) = initValue(i - 3 + offY, j + offX, k);
                            xc(i + n, j, k) = initValue(i + n + offY, j + offX, k);
                            xPc(i - 3, j, k) = initValue(i - 3 + offY, j + offX, k);
                            xPc(i + n, j, k) = initValue(i + n + offY, j + offX, k);
                        }

                        if (j < 3 && i < n) {
                            xc(i, j - 3, k) = initValue(i + offY, j - 3 + offX, k);
                            xc(i, j + m, k) = initValue(i + offY, j + m + offX, k);
                            xPc(i, j - 3, k) = initValue(i + offY, j - 3 + offX, k);
                            xPc(i, j + m, k) = initValue(i + offY, j + m + offX, k);
                        }

                        if (i < 3 && j < 3) {
                            xc(i - 3, j - 3, k) = initValue(i - 3 + offY, j - 3 + offX, k);
                            xc(i + n, j - 3, k) = initValue(i + n + offY, j - 3 + offX, k);
                            xc(i - 3, j + m, k) = initValue(i - 3 + offY, j + m + offX, k);
                            xc(i + n, j + m, k) = initValue(i + n + offY, j + m + offX, k);
                            xPc(i - 3, j - 3, k) = initValue(i - 3 + offY, j - 3 + offX, k);
                            xPc(i + n, j - 3, k) = initValue(i + n + offY, j - 3 + offX, k);
                            xPc(i - 3, j + m, k) = initValue(i - 3 + offY, j + m + offX, k);
                            xPc(i + n, j + m, k) = initValue(i + n + offY, j + m + offX, k);
                        }
                    }
                }
            }
        }
    }
}

void cpu0(realX * __restrict__ x_,
        realV1 * __restrict__ v1_ __attribute__ ((unused)),
        realV2 * __restrict__ v2_ __attribute__ ((unused)),
        realV3 * __restrict__ v3_ __attribute__ ((unused)),
        realH * __restrict__ h_ __attribute__ ((unused)),
        realX * __restrict__ xP_ __attribute__ ((unused)),
        const int n, const int m,
        const int blx, const int bly,
        const int thx, const int thy,
        //          const int id, const int procId,
        const int iOffset, const int jOffset) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = 0; k < LDIM; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi + iOffset;
                        const int j = idJ * thx + idj + jOffset;
                        if (i < n && j < m) {
                            xPc(i, j, k) = xc(i - 1, j - 1, k) +
                                    xc(i - 1, j, k) +
                                    xc(i - 1, j + 1, k) +
                                    xc(i, j - 1, k) +
                                    xc(i, j, k) +
                                    xc(i, j + 1, k) +
                                    xc(i + 1, j - 1, k) +
                                    xc(i + 1, j, k) +
                                    xc(i + 1, j + 1, k) +

                                    xc(i - 1, j - 1, k + 1) +
                                    xc(i - 1, j, k + 1) +
                                    xc(i - 1, j + 1, k + 1) +
                                    xc(i, j - 1, k + 1) +
                                    xc(i, j, k + 1) +
                                    xc(i, j + 1, k + 1) +
                                    xc(i + 1, j - 1, k + 1) +
                                    xc(i + 1, j, k + 1) +
                                    xc(i + 1, j + 1, k + 1) +
                                    xc(i - 1, j - 1, k - 1) +
                                    xc(i - 1, j, k - 1) +
                                    xc(i - 1, j + 1, k - 1) +
                                    xc(i, j - 1, k - 1) +
                                    xc(i, j, k - 1) +
                                    xc(i, j + 1, k - 1) +
                                    xc(i + 1, j - 1, k - 1) +
                                    xc(i + 1, j, k - 1) +
                                    xc(i + 1, j + 1, k - 1) +
                                    xc(i + 3, j + 3, k + 3) +
                                    xc(i - 3, j - 3, k - 3) +

                                    xc(i + 3, j + 2, k + 1);
                        }
                        /*
                                    if(i<3 && j<m)
                                    {
                                      xPc(i-3, j, k) = 1.0;
                                      xPc(i+n, j, k) = 2.0;
                                    }

                                    if(j<3 && i<n)
                                    {
                                      xPc(i, j-3, k) = 3.0;
                                      xPc(i, j+m, k) = 4.0;
                                    }*/
                    }
                }
            }
        }
    }
}

void cpu1(realV1 * __restrict__ v1_ __attribute__ ((unused)),
        realV2 * __restrict__ v2_ __attribute__ ((unused)),
        realV3 * __restrict__ v3_ __attribute__ ((unused)),
        realH * __restrict__ h_ __attribute__ ((unused)),
        realX * __restrict__ xP_,
        realV1P * __restrict__ v1P_ __attribute__ ((unused)),
        realV2P * __restrict__ v2P_ __attribute__ ((unused)),
        realV3P * __restrict__ v3P_ __attribute__ ((unused)),
        const int n, const int m,
        const int blx, const int bly,
        const int thx, const int thy,
        const int id,
        const int iOffset, const int jOffset) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = 0; k < LDIM; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi + iOffset;
                        const int j = idJ * thx + idj + jOffset;
                        if (i < n && j < m) {
                            xPc(i, j, k) = id + 20;
                        }
                        /*
                                    if(i<3 && j<m)
                                    {
                                      xPc(i-3, j, k) = 1.0;
                                      xPc(i+n, j, k) = 2.0;
                                    }

                                    if(j<3 && i<n)
                                    {
                                      xPc(i, j-3, k) = 3.0;
                                      xPc(i, j+m, k) = 4.0;
                                    }*/
                    }
                }
            }
        }
    }
}

void cpu2(realX * __restrict__ x_ __attribute__ ((unused)),
        realH * __restrict__ h_ __attribute__ ((unused)),
        realX * __restrict__ xP_,
        realV1P * __restrict__ v1P_ __attribute__ ((unused)),
        realV2P * __restrict__ v2P_ __attribute__ ((unused)),
        realV3P * __restrict__ v3P_ __attribute__ ((unused)),
        realCP * __restrict__ cp_ __attribute__ ((unused)),
        realCN * __restrict__ cn_ __attribute__ ((unused)),
        const int n, const int m,
        const int blx, const int bly,
        const int thx, const int thy,
        const int id,
        const int iOffset, const int jOffset) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = 0; k < LDIM; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi + iOffset;
                        const int j = idJ * thx + idj + jOffset;
                        if (i < n && j < m) {
                            xPc(i, j, k) = id + 20;
                        }
                        /*
                                    if(i<3 && j<m)
                                    {
                                      xPc(i-3, j, k) = 1.0;
                                      xPc(i+n, j, k) = 2.0;
                                    }

                                    if(j<3 && i<n)
                                    {
                                      xPc(i, j-3, k) = 3.0;
                                      xPc(i, j+m, k) = 4.0;
                                    }*/
                    }
                }
            }
        }
    }
}

void cpu3(realX * __restrict__ x_ __attribute__ ((unused)),
        realH * __restrict__ h_ __attribute__ ((unused)),
        realX * __restrict__ xP_,
        realV1P * __restrict__ v1P_ __attribute__ ((unused)),
        realV2P * __restrict__ v2P_ __attribute__ ((unused)),
        realV3P * __restrict__ v3P_ __attribute__ ((unused)),
        realCP * __restrict__ cp_ __attribute__ ((unused)),
        realCN * __restrict__ cn_ __attribute__ ((unused)),
        const int n, const int m,
        const int blx, const int bly,
        const int thx, const int thy,
        const int id,
        const int iOffset, const int jOffset) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = 0; k < LDIM; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi + iOffset;
                        const int j = idJ * thx + idj + jOffset;
                        if (i < n && j < m) {
                            xPc(i, j, k) = id + 20;
                        }
                        /*
                                    if(i<3 && j<m)
                                    {
                                      xPc(i-3, j, k) = 1.0;
                                      xPc(i+n, j, k) = 2.0;
                                    }

                                    if(j<3 && i<n)
                                    {
                                      xPc(i, j-3, k) = 3.0;
                                      xPc(i, j+m, k) = 4.0;
                                    }*/
                    }
                }
            }
        }
    }
}

#define dst(i,j,k) dst_[(i)*xOffL + j + (k)*xOff]
#define src(i,j,k) src_[(i)*xOffL + j + (k)*xOff]

void cpyCpuLR(realX *dst_, realX *src_, const int n, const int m,
        const int blx, const int bly, const int thx, const int thy) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = -3; k < LDIM + 3; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi;
                        const int j = idJ * thx + idj;

                        if (j < 3 && i < n)
                            dst(i, j - 3, k) = src(i, j + m - 3, k);
                    }
                }
            }
        }
    }
}

void cpyCpuRL(realX *dst_, realX *src_, const int n, const int m,
        const int blx, const int bly, const int thx, const int thy) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = -3; k < LDIM + 3; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi;
                        const int j = idJ * thx + idj;

                        if (j < 3 && i < n)
                            dst(i, j + m, k) = src(i, j, k);
                    }
                }
            }
        }
    }
}

void cpyCpuLRRL(realX *dst_, realX *src_, const int n, const int m,
        const int blx, const int bly, const int thx, const int thy) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = -3; k < LDIM + 3; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        int i = idI * thy + idi;
                        int j = idJ * thx + idj;

                        if (j < 3 && i < n) {
                            dst(i, j - 3, k) = src(i, j + m - 3, k);
                            src(i, j + m, k) = dst(i, j, k);
                        }
                    }
                }
            }
        }
    }
}

void cpyCpuTB(realX *dst_, realX *src_, const int n, const int m,
        const int blx, const int bly, const int thx, const int thy) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = -3; k < LDIM + 3; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi;
                        const int j = idJ * thx + idj;

                        if (i < 3 && j < m + 6)
                            dst(i - 3, j - 3, k) = src(i + n - 3, j - 3, k);
                    }
                }
            }
        }
    }
}

void cpyCpuBT(realX *dst_, realX *src_, const int n, const int m,
        const int blx, const int bly, const int thx, const int thy) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = -3; k < LDIM + 3; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi;
                        const int j = idJ * thx + idj;

                        if (i < 3 && j < m + 6)
                            dst(i + n, j - 3, k) = src(i, j - 3, k);
                    }
                }
            }
        }
    }
}

void cpyCpuTBBT(realX *dst_, realX *src_, const int n, const int m,
        const int blx, const int bly, const int thx, const int thy) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = -3; k < LDIM + 3; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi;
                        const int j = idJ * thx + idj;

                        if (i < 3 && j < m + 6) {
                            dst(i - 3, j - 3, k) = src(i + n - 3, j - 3, k);
                            src(i + n, j - 3, k) = dst(i, j - 3, k);
                        }
                    }
                }
            }
        }
    }
}

#if NDS>1

void cpyCpuLRtoBuf(realX *dst_, realX *src_, const int n, const int m,
        const int blx, const int bly, const int thx, const int thy) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = -3; k < LDIM + 3; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi;
                        const int j = idJ * thx + idj;

                        if (j < 3 && i < n) {
                            dst_[i * 3 * (LDIM + 6) + j + (k + 3)*3] = src(i, j + m - 3, k);
                        }
                    }
                }
            }
        }
    }
}

void cpyCpuRLtoMat(realX *dst_, realX *src_, const int n,
        const int blx, const int bly, const int thx, const int thy) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = -3; k < LDIM + 3; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi;
                        const int j = idJ * thx + idj;

                        if (j < 3 && i < n) {
                            dst(i, j - 3, k) = src_[i * 3 * (LDIM + 6) + j + (k + 3)*3];
                        }
                    }
                }
            }
        }
    }
}

void cpyCpuRLtoBuf(realX *dst_, realX *src_, const int n,
        const int blx, const int bly, const int thx, const int thy) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = -3; k < LDIM + 3; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi;
                        const int j = idJ * thx + idj;

                        if (j < 3 && i < n) {
                            dst_[i * 3 * (LDIM + 6) + j + (k + 3)*3] = src(i, j, k);
                        }
                    }
                }
            }
        }
    }
}

void cpyCpuLRtoMat(realX *dst_, realX *src_, const int n, const int m,
        const int blx, const int bly, const int thx, const int thy) {
    const int xOff = mCPU<realX>();
    const int xOffL = xOff * (LDIM + 6);

    for (int k = -3; k < LDIM + 3; ++k) {
        for (int idI = 0; idI < bly; ++idI) {
            for (int idJ = 0; idJ < blx; ++idJ) {
                for (int idi = 0; idi < thy; ++idi) {
                    for (int idj = 0; idj < thx; ++idj) {
                        const int i = idI * thy + idi;
                        const int j = idJ * thx + idj;

                        if (j < 3 && i < n) {
                            dst(i, j + m, k) = src_[i * 3 * (LDIM + 6) + j + (k + 3)*3];
                        }
                    }
                }
            }
        }
    }
}
#endif //NDS>0

void initCPU() {
    for (int id = 0; id < THS; ++id) {
        //    flag[id] = -1;
    }
}

void initCpuKrnCall(MatrixCPU<realX> *xC,
        MatrixCPU<realV1> *v1C,
        MatrixCPU<realV2> *v2C,
        MatrixCPU<realV3> *v3C,
        MatrixCPU<realH> *hC,
        MatrixCPU<realX> *xPC,
        /*            MatrixCPU<realV1P> *v1PC,
                    MatrixCPU<realV2P> *v2PC,
                    MatrixCPU<realV3P> *v3PC,
                    MatrixCPU<realCP> *cpC,
                    MatrixCPU<realCN> *cnC,*/
        //            const int procId,
        const int offYCPU, const int offXCPU) {
    for (int i = 0; i < THS; ++i) {
        const int offY = offYCPU - mdCPU.iOffset[i]+(i % (mdCPU.yTPG * mdCPU.yS)) * mdCPU.nT;
        const int offX = offXCPU - mdCPU.jOffset[i]+(i / (mdCPU.yTPG * mdCPU.yS)) * mdCPU.mT;
        initCpuKrn(xC[ mdCPU.tToM[i] ].fst,
                v1C[ mdCPU.tToM[i] ].fst,
                v2C[ mdCPU.tToM[i] ].fst,
                v3C[ mdCPU.tToM[i] ].fst,
                hC[ mdCPU.tToM[i] ].fst,
                xPC[ mdCPU.tToM[i] ].fst,
                xC[ mdCPU.tToM[i] ].n, xC[ mdCPU.tToM[i] ].m,
                mdCPU.cpu0B.x, mdCPU.cpu0B.y,
                CPUAX, CPUAY,
                //         i, procId,
                mdCPU.iOffset[i], mdCPU.jOffset[i], offY, offX);
    }
}

/*
realX valCPU(int i, int j, int k, int offY, int offX)
{
  return i-3+offY + (j-3+offX)*NDIM + (k-3)*NDIM*MDIM;
}
 */
void saveResCPU(MatrixCPU<realX> *xG, const int nGPU,
        const int procId, const int threadId, const int ts, const double time, const double energy) {
    for (int i = 0; i < mdCPU.matrixCount; ++i) {
        if (mdCPU.tToM[threadId] == i) {
            //      arr[i]=ts*100+1;
            if (threadId == xG[i].fTiM) {
                /*        const int stopCriterium=ts*100+1;
                        bool passed=false;
                        while(!passed)
                        {
                          bool flag=true;
                          for(int j=0; j<THS; ++j)
                          if(mdCPU.tToM[j]==i)
                          {
                            if(arr[j]>=stopCriterium)
                            {
                              flag&=true;
                            }
                            else
                            {
                              flag&=false;
                            }
                          }
                          passed = flag;
                        }*/
                const int metaDataEl = 12 * sizeof (int) / sizeof (realX);
                int metaData[12];
                metaData[0] = mdCPU.n;
                metaData[1] = mCPU<realX>();
                metaData[2] = mdCPU.l;
                metaData[3] = xG[i].n;
                metaData[4] = xG[i].m;
                metaData[5] = mdCPU.mT * mdCPU.xS;
#if GCP>0
                metaData[6] = mdGPU.nT * mdGPU.yS;
#else
                metaData[6] = 0;
#endif
                metaData[7] = nGPU;

                memcpy(&metaData[8], &time, 8);
                memcpy(&metaData[10], &energy, 8);
                /*
                        std::cout << "mdCPU.n="<<mdCPU.n << " mCPU<realX>()="<<mCPU<realX>() << " mdCPU.l="<<mdCPU.l << " xG[i].n="<<xG[i].n << " xG[i].m="<<xG[i].m << " mdCPU.mT*mdCPU.xS="<<mdCPU.mT*mdCPU.xS <<'\n';
                 */
                realX *tmp = new realX[ xG[i].n * mlCPU<realX>() + metaDataEl ];

                memcpy(tmp, metaData, 12 * sizeof (int));

                memcpy(tmp + metaDataEl, xG[i].fst - 3, xG[i].n * mlCPU<realX>() * sizeof (realX));

                std::string filename = toStr("./sim/") + TSK + toStr("-CPU-") + toStr(ts) + "-" + toStr(procId) + "-" + toStr(i) + ".txt";
                std::ofstream fout;
                fout.open(filename.c_str(), std::ofstream::binary);
                fout.write((char*) tmp, 12 * sizeof (int)+xG[i].n * mlCPU<realX>() * sizeof (realX));
                fout.close();

                delete [] tmp;
            }
        }
    }
}

#if GTC>1
volatile int signals[THS];

void RunCPU::notify(const int val, const int ts) {
    signals[threadId] = ts * 100 + val;
}

void RunCPU::wait(const int val, const int ts) {
    bool done;
    do {
        done = true;
        for (int i = 0; i < GTC - 1; ++i) {
            if (signals[ waitingList[i] ] < ts * 100 + val) done = false;
        }
    } while (!done);
}
#endif

int RunCPU::run(const int ts) {
    cpu0(xC[ mdCPU.tToM[threadId] ].fst,
            v1C[ mdCPU.tToM[threadId] ].fst,
            v2C[ mdCPU.tToM[threadId] ].fst,
            v3C[ mdCPU.tToM[threadId] ].fst,
            hC[ mdCPU.tToM[threadId] ].fst,
            xPC[ mdCPU.tToM[threadId] ].fst,
            xC[ mdCPU.tToM[threadId] ].n, xC[ mdCPU.tToM[threadId] ].m,
            mdCPU.cpu0B.x, mdCPU.cpu0B.y,
            CPUAX, CPUAY,
            //       threadId, procId,
            mdCPU.iOffset[threadId], mdCPU.jOffset[threadId]);

#if GTC>1
    if (ts < TS - 1) {
        notify(1, ts);
        wait(1, ts);
    }
#endif
    /*
    cpu1(v1C[ mdCPU.tToM[threadId] ].fst,
         v2C[ mdCPU.tToM[threadId] ].fst,
         v3C[ mdCPU.tToM[threadId] ].fst,
         hC[ mdCPU.tToM[threadId] ].fst,
         xPC[ mdCPU.tToM[threadId] ].fst,
         v1PC[ mdCPU.tToM[threadId] ].fst,
         v2PC[ mdCPU.tToM[threadId] ].fst,
         v3PC[ mdCPU.tToM[threadId] ].fst,
         xC[ mdCPU.tToM[threadId] ].n, xC[ mdCPU.tToM[threadId] ].m,
         mdCPU.cpu1B.x, mdCPU.cpu1B.y,
         CPUBX, CPUBY,
         threadId, mdCPU.iOffset[threadId], mdCPU.jOffset[threadId]);
    #if GTC>1
      //synchronize kernels that share a single matrix
    #endif
    cpu2(xC[ mdCPU.tToM[threadId] ].fst,
         hC[ mdCPU.tToM[threadId] ].fst,
         xPC[ mdCPU.tToM[threadId] ].fst,
         v1PC[ mdCPU.tToM[threadId] ].fst,
         v2PC[ mdCPU.tToM[threadId] ].fst,
         v3PC[ mdCPU.tToM[threadId] ].fst,
         cpC[ mdCPU.tToM[threadId] ].fst,
         cnC[ mdCPU.tToM[threadId] ].fst,
         xC[ mdCPU.tToM[threadId] ].n, xC[ mdCPU.tToM[threadId] ].m,
         mdCPU.cpu2B.x, mdCPU.cpu2B.y,
         CPUCX, CPUCY,
         threadId, mdCPU.iOffset[threadId], mdCPU.jOffset[threadId]);
    #if GTC>1
      //synchronize kernels that share a single matrix
    #endif
    cpu3(xC[ mdCPU.tToM[threadId] ].fst,
         hC[ mdCPU.tToM[threadId] ].fst,
         xPC[ mdCPU.tToM[threadId] ].fst,
         v1PC[ mdCPU.tToM[threadId] ].fst,
         v2PC[ mdCPU.tToM[threadId] ].fst,
         v3PC[ mdCPU.tToM[threadId] ].fst,
         cpC[ mdCPU.tToM[threadId] ].fst,
         cnC[ mdCPU.tToM[threadId] ].fst,
         xC[ mdCPU.tToM[threadId] ].n, xC[ mdCPU.tToM[threadId] ].m,
         mdCPU.cpu3B.x, mdCPU.cpu3B.y,
         CPUDX, CPUDY,
         threadId, mdCPU.iOffset[threadId], mdCPU.jOffset[threadId]);
     */
    threadSig(threadId, runFlag, ts);
    if (firstColumn) threadSig(threadId, 3, ts, 1);
    return 1;
}

int UpdateCPUhor::update(const int ts) {
    //a gdzie synchronizacja z calą kolumną????????? - tu nie trzeba - tylko w MPI - bo tu nie moze byc w kolumnie dwóch
#if HSPC==1
    if (LR) {
        cpyCpuLR(fstTo, fstFrom, n, m, mdCPU.nBV.x, mdCPU.nBV.y, CPUDX, CPUDY);
        threadSig(idTfrom, 2, ts);
    } else {
        cpyCpuRL(fstTo, fstFrom, n, m, mdCPU.nBV.x, mdCPU.nBV.y, CPUDX, CPUDY);
        threadSig(idTfrom, 2, ts, 1);
    }
    ++ind;
    return 0;
#else
    if (threadQuery(idTto, 1, ts)) {
        cpyCpuLRRL(fstTo, fstFrom, n, m, mdCPU.nBV.x, mdCPU.nBV.y, CPUDX, CPUDY);
        threadSig(idTfrom, 3, ts);
        std::cout << tmp << "zakonczylem kopiowac CPU LRRL\n";
        ++ind;
    }
    return 0;
#endif
}

#if HSPC==1

int UpdateCPUhor::query(const int ts) {
    int finished = 0;
    if (LR) {
        if (threadQuery(idTto, 2, ts, 1)) {
            threadSig(idTfrom, 3, ts);
            ++ind;
            finished = 1;
        }
    } else {
        if (threadQuery(idTto, 2, ts)) {
            threadSig(idTfrom, 3, ts, 1);
            ++ind;
            finished = 1;
        }
    }
    return finished;
}
#endif

int UpdateCPUhor::done(const int ts __attribute__ ((unused))) {
    return 1;
}

int UpdateCPUhor::run(const int ts) {
    return (this->*stage[ind])(ts);
}

#if HSPC==0

int QueryCPUhor::query(const int ts) {
    int finished = 0;
    if (threadQuery(idTfrom, 2, ts)) {
        threadSig(idTto, 3, ts, 1);
        ++ind;
        finished = 1;
    }
    return finished;
}

int QueryCPUhor::done(const int ts __attribute__ ((unused))) {
    return 1;
}

int QueryCPUhor::run(const int ts) {
    return (this->*stage[ind])(ts);
}
#endif //HSPC==0

//********************************************
/*
// Requires #include <xmmintrin.h>
// Requires #include <pmmintrin.h>
_MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
_MM_SET_DENORMALS_ZERO_MODE(_MM_DENORMALS_ZERO_ON);
 */

//============================begin ver

bool UpdateCPUver::queryRows(const int ts) {
    if (TB) {
        const int iteration = -THS / TTH;
        const int end = -idTfrom + THS / TTH * (mdCPU.xS - 1) + 1;

        bool err = true;
        for (int I = idTfrom + iteration; -I < end; I += iteration) {
            if (-(I + iteration) >= end) {
                if (TB) err &= threadQuery(I, 3, ts, 1);
                else err &= threadQuery(I, 3, ts);
            } else err &= threadQuery(I, 1, ts);
#if HSPC==0
            if (-(I + iteration) >= end) {
                if (TB) err &= threadQuery(I + 1, 3, ts, 1);
                else err &= threadQuery(I + 1, 3, ts);
            } else err &= threadQuery(I + 1, 1, ts);
#endif
        }
#if HSPC==0
        err &= threadQuery(idTfrom + 1, 3, ts);
        if (-idTfrom - iteration >= end) {
            err &= threadQuery(idTfrom + 1, 3, ts, 1);
        }
#endif
        return err;
    } else {
        const int iteration = THS / TTH;
        const int end = idTfrom + THS / TTH * (mdCPU.xS - 1) + 1;

        bool err = true;
        for (int I = idTfrom + iteration; I < end; I += iteration) {
            if (I + iteration >= end) {
                err &= threadQuery(I, 3, ts);
            } else err &= threadQuery(I, 1, ts);
#if HSPC==0
            if (I + iteration >= end) {
                err &= threadQuery(I + 1, 3, ts);
            } else err &= threadQuery(I + 1, 1, ts);
#endif
        }
#if HSPC==0
        err &= threadQuery(idTfrom + 1, 3, ts, 1);
        if (idTfrom + iteration >= end) {
            err &= threadQuery(idTfrom + 1, 3, ts);
        }
#endif
        return err;
    }
}

int UpdateCPUver::update(const int ts) {
#if HSPC==1
    if (TB) {
        if (queryRows(ts)) {
#if CPYC>0
            memcpy(dstTo, srcFrom, sizeV);
#else
            cpyCpuTB(fstTo, fstFrom, n, m, mdCPU.nBH.x, mdCPU.nBH.y, CPUDX, CPUDY);
#endif
            threadSig(idTfrom, 4, ts);
            ++ind;
        }
    } else {
        if (queryRows(ts)) {
#if CPYC>0
            memcpy(dstTo, srcFrom, sizeV);
#else
            cpyCpuBT(fstTo, fstFrom, n, m, mdCPU.nBH.x, mdCPU.nBH.y, CPUDX, CPUDY);
#endif
            threadSig(idTfrom, 4, ts, 1);
            ++ind;
        }
    }
#else
#if CPYC>0
    if (queryRows(ts)) {
        memcpy(dstTo, srcFrom, sizeV);
        memcpy(dstFrom, srcTo, sizeV);
        threadSig(idTfrom, 4, ts);
        ++ind;
    }
#else
    if (queryRows(ts)) {
        cpyCpuTBBT(fstTo, fstFrom, n, m, mdCPU.nBH.x, mdCPU.nBH.y, CPUDX, CPUDY);
        threadSig(idTfrom, 4, ts);
        ++ind;
    }
#endif
#endif
    return 0;
}

#if HSPC==1

int UpdateCPUver::query(const int ts) {
    int finished = 0;
    if (TB) {
        if (threadQuery(idTto, 4, ts, 1)) {
            ++ind;
            finished = 1;
        }
    } else {
        if (threadQuery(idTto, 4, ts)) {
            ++ind;
            finished = 1;
        }
    }
    return finished;
}
#endif

int UpdateCPUver::done(const int ts __attribute__ ((unused))) {
    return 1;
}

int UpdateCPUver::run(const int ts) {
    return (this->*stage[ind])(ts);
}

int QueryCPUver::query(const int ts) {
    int finished = 0;
    if (threadQuery(idTfrom, 4, ts, tag)) {
        ++ind;
        finished = 1;
    }
    return finished;
}

int QueryCPUver::done(const int ts __attribute__ ((unused))) {
    return 1;
}

int QueryCPUver::run(const int ts) {
    return (this->*stage[ind])(ts);
}
//============================end ver

#if NDS>1

int UpdateCPUhorMPI::updateBuf(const int timeStep) {
    bool err = true;
    if (LR) {
        for (int i = mC->lTiM - 1; i > mC->lTiM - mdCPU.yS; --i) {
            err &= threadQuery(i, 1, timeStep);
        }
    } else {
        for (int i = mC->fTiM + 1; i < mC->fTiM + mdCPU.yS; ++i) {
            err &= threadQuery(i, 1, timeStep);
        }
    }

    if (err) {
        if (LR) {
          /*  cpyCpuLRtoBuf
                    (mC->updateNode->srcBufLR.bufH, mC->fst, mC->n, mdCPU.mT * mdCPU.xS,
                    mdCPU.nBV.x, mdCPU.nBV.y, CPUDX, CPUDY);*/
            mC->updateNode->srcBufLR.str = 1 + timeStep * 100;
        } else {
         /*   cpyCpuRLtoBuf
                    (mC->updateNode->srcBufRL.bufH, mC->fst, mC->n,
                    mdCPU.nBV.x, mdCPU.nBV.y, CPUDX, CPUDY);*/
            mC->updateNode->srcBufRL.str = 1 + timeStep * 100;
        }
        ++ind;
    }
    return 0;
}

int UpdateCPUhorMPI::updateMat(const int timeStep) {
    int finished = 0;
    if (LR) {
        if (mC->updateNode->dstBufLR.passed2 >= 1 + timeStep * 100) {
        /*    cpyCpuLRtoMat
                    (mC->fst, mC->updateNode->dstBufLR.bufH, mC->n, mdCPU.mT * mdCPU.xS,
                    mdCPU.nBV.x, mdCPU.nBV.y, CPUDX, CPUDY);*/
            threadSig(idTFrom, 3, timeStep);
            ++ind;
            finished = 1;
        }
    } else {
        if (mC->updateNode->dstBufRL.passed2 >= 1 + timeStep * 100) {
       /*     cpyCpuRLtoMat
                    (mC->fst, mC->updateNode->dstBufRL.bufH, mC->n,
                    mdCPU.nBV.x, mdCPU.nBV.y, CPUDX, CPUDY);*/
            threadSig(idTFrom, 3, timeStep, 1);
            ++ind;
            finished = 1;
        }
    }
    return finished;
}

int UpdateCPUhorMPI::done(const int timeStep __attribute__ ((unused))) {
    return 1;
}

int UpdateCPUhorMPI::run(const int ts) {
    return (this->*stage[ind])(ts);
}

//-------------

int QueryCPUhorMPI::query(const int ts) {
    if (LR) {
        int finished = 0;
        if (threadQuery(idTQ, 3, ts)) //if( threadQuery(idTQ, 3, ts, LR) ) - bez ifa
        {
            threadSig(idTS, 3, ts);
            ++ind;
            finished = 1;
        }
        return finished;
    } else {
        int finished = 0;
        if (threadQuery(idTQ, 3, ts, 1)) {
            threadSig(idTS, 3, ts, 1);
            ++ind;
            finished = 1;
        }
        return finished;
    }
}

int QueryCPUhorMPI::done(const int ts __attribute__ ((unused))) {
    return 1;
}

int QueryCPUhorMPI::run(const int ts) {
    return (this->*stage[ind])(ts);
}
//-------------
#endif //NDS>1

#if NDS>1

int UpdateCPUhorMPImain::updateNode(const int timeStep) {
    if (LR) {
        if (mC->updateNode->srcBufLR.str >= 1 + timeStep * 100) {
      /*      MPI_Isend(mC->updateNode->srcBufLR.bufH,
                    mC->updateNode->sizeLRRL,
                    MPI_realX, procId + NDS_V / TDS, tag, MPI_COMM_WORLD,
                    &mC->updateNode->srcBufLR.req[1]);

            MPI_Irecv(mC->updateNode->dstBufLR.bufH,
                    mC->updateNode->sizeLRRL,
                    MPI_realX, procId + NDS_V / TDS, tag, MPI_COMM_WORLD,
                    &mC->updateNode->dstBufLR.req[0]);*/
            ++ind;
        }
    } else {
        if (mC->updateNode->srcBufRL.str >= 1 + timeStep * 100) {
      /*      MPI_Isend(mC->updateNode->srcBufRL.bufH,
                    mC->updateNode->sizeLRRL,
                    MPI_realX, procId - NDS_V / TDS, tag, MPI_COMM_WORLD,
                    &mC->updateNode->srcBufRL.req[1]);

            MPI_Irecv(mC->updateNode->dstBufRL.bufH,
                    mC->updateNode->sizeLRRL,
                    MPI_realX, procId - NDS_V / TDS, tag, MPI_COMM_WORLD,
                    &mC->updateNode->dstBufRL.req[0]);*/
            ++ind;
        }
    }
    return 0;
}

int UpdateCPUhorMPImain::queryNode(const int timeStep) {
    int finished = 0;
    if (LR) {
        if (mC->updateNode->dstBufLR.pass3(timeStep)) {
            int flag = 0;
            MPI_Test(&mC->updateNode->srcBufLR.req[1], &flag, MPI_STATUS_IGNORE);
            if (flag == 1) {
                std::cout << procId << "zakonczylem LR CPU MPI\n";
                ++ind;
                finished = 1;
            }
        }
    } else {
        if (mC->updateNode->dstBufRL.pass3(timeStep)) {
            int flag = 0;
            MPI_Test(&mC->updateNode->srcBufRL.req[1], &flag, MPI_STATUS_IGNORE);
            if (flag == 1) {
                std::cout << procId << "zakonczylem RL CPU MPI\n";
                ++ind;
                finished = 1;
            }
        }
    }
    return finished;
}

int UpdateCPUhorMPImain::done(const int timeStep __attribute__ ((unused))) {
    return 1;
}

int UpdateCPUhorMPImain::run(const int ts) {
    return (this->*stage[ind])(ts);
}

//---------------------------------

bool UpdateCPUverMPImain::queryRows(const int ts) {
    if (TB) {
        const int iteration = -THS / TTH;
        const int end = -idTfrom + THS / TTH * (mdCPU.xS - 1) + 1;

        bool err = true;
        for (int I = idTfrom + iteration; -I < end; I += iteration) {
            if (-(I + iteration) >= end) {
                err &= threadQuery(I, 3, ts, 1);
            } else err &= threadQuery(I, 1, ts);
        }
        err &= threadQuery(idTfrom, 3, ts);
        if (-idTfrom - iteration >= end) {
            err &= threadQuery(idTfrom, 3, ts, 1);
        }
        return err;
    } else {
        const int iteration = THS / TTH;
        const int end = idTfrom + THS / TTH * (mdCPU.xS - 1) + 1;

        bool err = true;
        for (int I = idTfrom + iteration; I < end; I += iteration) {
            if (I + iteration >= end) {
                err &= threadQuery(I, 3, ts);
            } else err &= threadQuery(I, 1, ts);
        }
        err &= threadQuery(idTfrom, 3, ts, 1);
        if (idTfrom + iteration >= end) {
            err &= threadQuery(idTfrom, 3, ts);
        }
        return err;
    }
}

int UpdateCPUverMPImain::updateNode(const int timeStep) {
    if (TB) {
        if (queryRows(timeStep)) {
            MPI_Isend(mC->srcTB,
                    mC->updateNode->sizeTBBT,
                    MPI_realX, procId + 1, tag, MPI_COMM_WORLD,
                    &mC->updateNode->srcBufTB.req[1]);

            MPI_Irecv(mC->dstBT,
                    mC->updateNode->sizeTBBT,
                    MPI_realX, procId + 1, tag, MPI_COMM_WORLD,
                    &mC->updateNode->dstBufTB.req[0]);
            ++ind;
        }
    } else {
        if (queryRows(timeStep)) {
            MPI_Isend(mC->srcBT,
                    mC->updateNode->sizeTBBT,
                    MPI_realX, procId - 1, tag, MPI_COMM_WORLD,
                    &mC->updateNode->srcBufBT.req[1]);

            MPI_Irecv(mC->dstTB,
                    mC->updateNode->sizeTBBT,
                    MPI_realX, procId - 1, tag, MPI_COMM_WORLD,
                    &mC->updateNode->dstBufBT.req[0]);
            ++ind;
        }
    }
    return 0;
}

int UpdateCPUverMPImain::queryNode(const int timeStep) {
    int finished = 0;
    if (TB) {
        if (mC->updateNode->dstBufTB.pass3(timeStep)) {
            int flag = 0;
            MPI_Test(&mC->updateNode->srcBufTB.req[1], &flag, MPI_STATUS_IGNORE);
            if (flag == 1) {
                sigFromMain[threadIndex] = timeStep * 100 + 1;
                ++ind;
                finished = 1;
            }
        }
    } else {
        if (mC->updateNode->dstBufBT.pass3(timeStep)) {
            int flag = 0;
            MPI_Test(&mC->updateNode->srcBufBT.req[1], &flag, MPI_STATUS_IGNORE);
            if (flag == 1) {
                sigFromMain[threadIndex] = timeStep * 100 + 1;
                ++ind;
                finished = 1;
            }
        }
    }
    return finished;
}

int UpdateCPUverMPImain::done(const int timeStep __attribute__ ((unused))) {
    return 1;
}

int UpdateCPUverMPImain::run(const int ts) {
    return (this->*stage[ind])(ts);
}

//-------------------------------------------------------

int QueryCPUverMPI::query(const int ts) {
    int finished = 0;
    if (sigFromMain[threadIndex] >= ts * 100 + 1) {
        ++ind;
        finished = 1;
    }
    return finished;
}

int QueryCPUverMPI::done(const int ts __attribute__ ((unused))) {
    return 1;
}

int QueryCPUverMPI::run(const int ts) {
    return (this->*stage[ind])(ts);
}
#endif //NDS>1

int threadQueryTMP2(int idTfrom, int n, int ts) {
    return threadQuery(idTfrom, n, ts);
}

#endif //GCP<100
