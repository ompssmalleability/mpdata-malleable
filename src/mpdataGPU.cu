#include "mpdataGPU.h"

/*
  int n, m, l;
  n = metaData[0];
  m = metaData[1];
  l = metaData[2];

  type="Ball";

    const double dx=2.0;
    const double dy=2.0;
    const double dz=2.0;

    const double omega=0.1;
    const double omx=omega/sqrt(3.);//omega/2.0;
    const double omy=omega/sqrt(3.);//omega/2.0;
    const double omz=omega/sqrt(3.);//omega*( 1.0/sqrt(2.0) );

    const double x0=20.0*dx;
    const double y0=20.0*dx;
    const double z0=20.0*dx;

    const double xos = 20.0*dx-7.0*dx*( 1.0/sqrt(6.0) );
    const double yos = 20.0*dx-7.0*dx*( 1.0/sqrt(6.0) );
    const double zos = 20.0*dx+14.0*dx*( 1.0/sqrt(6.0) );
    const double radius=15.0;//7.0*dx;
//    const double radius=30;//7.0*dx;

    const double xr=50.0;
    const double yr=50.0;
    const double zr=50.0;
  //  const double xr=0.0;
  //  const double yr=0.0;
  //  const double zr=0.0;

  int index;
  for(int i=0; i<n; ++i)
    for(int k=0; k<l; ++k)
      for(int j=0; j<m; ++j)
      {
        index = i*m*l + k*m + j;
        const double ux1=i*dx-xr;
        const double uy1=j*dy-yr;
        const double uz1=k*dz-zr;

        const double sxl=i*dx-xos;
        const double syl=j*dy-yos;
        const double szl=k*dz-zos;

        const double dist=sqrt(sxl*sxl+syl*syl+szl*szl);

        u1[index] = omy*uz1-omz*uy1;//-omz*(uy1-y0) + omy*(uz1-z0);
        u2[index] = omz*ux1-omx*uz1;// omz*(ux1-x0) - omx*(uz1-z0);
        u3[index] = omx*uy1-omy*ux1;//-omy*(ux1-x0) + omx*(uy1-y0);

        if(dist<=radius)
        {
          x[index]=4.0 - dist/radius*4.0;
//          x[index]=4.0;
        }
        else x[index] = 0.0;
        h[index] = 10.0;
      }

  for(int i=0; i<2; ++i)
    for(int k=0; k<l; ++k)
      for(int j=0; j<m; ++j)
      {
        index = i*m*l + k*m + j;
        ubc[index] = 0.0;
        bcx[index] = 0.0;
      }

  for(int i=0; i<n; ++i)
    for(int k=0; k<l; ++k)
      for(int j=0; j<2; ++j)
      {
        index = i*2*l + k*2 + j;
        vbc[index] = 0.0;
        bcy[index] = 0.0;
      }

  for(int i=0; i<n; ++i)
    for(int k=0; k<2; ++k)
      for(int j=0; j<m; ++j)
      {
        index = i*m*2 + k*m + j;
        wbc[index] = 0.0;
      }
*/

#if GCP>0
__device__ __host__
#endif
double initValue(const int i, const int j, const int k)
{
  return i + j*NDIM + k*NDIM*MDIM;
}

/*
#define DX (2.0*41.0/NDIM)
#define OMEGA 0.1
#define XOS (NDIM/41.0)*20.0*dx-(NDIM/41.0)*7.0*dx*(1.0/sqrt(6.0))
#define YOS (NDIM/41.0)*20.0*dx-(NDIM/41.0)*7.0*dx*(1.0/sqrt(6.0))
#define ZOS (NDIM/41.0)*20.0*dx+(NDIM/41.0)*7.0*dx*(1.0/sqrt(6.0))
#define OMX omega/sqrt(3.)
#define OMY omega/sqrt(3.)
#define OMZ omega/sqrt(3.)
#define XR 50.0
#define YR 50.0
#define ZR 50.0
#define RADIUS 14.0
#define BALLBODY 4.0-dist/(radius-0.1)*3.9
#define DENSITYH 10.0
*/

#define DX (2.0*41.0/NDIM)

//#define OMEGA 0.000000000000000000000000000000000000001
//#define DENSITYH 0.0000000000000000000000000000000000001

#define OMEGA 0.0000000000000000000000000000000000001
#define DENSITYH 0.00000000000000000000000000000000001

#define XOS (NDIM/41.0)*19.99999930682059999999999999999999999999999973910573786*dx-(NDIM/41.0)*6.999999306820573910573786*dx*(0.999999306820573910573786/sqrt(6.0))
#define YOS (NDIM/41.0)*19.99999930682057391057378699999999999999999999999999999*dx-(NDIM/41.0)*6.999999306820573910573786*dx*(0.99999930682059999999999999999999999999999973910573786/sqrt(6.0))
#define ZOS (NDIM/41.0)*19.99999930682057391057378699999999999999999999999999999*dx+(NDIM/41.0)*6.99999930682057391057378699999999999999999999999999999*dx*(0.99999930682057391057378699999999999999999999999999999/sqrt(6.0))
#define OMX omega/sqrt(2.99999930682057391057378699999999999999999999999999999)
#define OMY omega/sqrt(2.99999930682057391057378699999999999999999999999999999)
#define OMZ omega/sqrt(2.99999930682057391057378699999999999999999999999999999)
#define XR 49.99999930682057391057378699999999999999999999999999999
#define YR 49.99999930682057391057378699999999999999999999999999999
#define ZR 49.99999930682057391057378699999999999999999999999999999
#define RADIUS 13.99999930682057391057378699999999999999999999999999999
#define BALLBODY 3.99999930682057391057378699999999999999999999999999999-dist/(radius-0.099999930682057391057378699999999999999999999999999999)*3.899999930682057391057378699999999999999999999999999999


#if GCP>0
__device__ __host__
#endif
double initValueX(const int i, const int j, const int k)
{
  const double dx=DX;
  const double dy=DX;
  const double dz=DX;

  const double xos = XOS;
  const double yos = YOS;
  const double zos = ZOS;
  const double radius=RADIUS;

  const double sxl=i*dx-xos;
  const double syl=j*dy-yos;
  const double szl=k*dz-zos;

  const double dist=sqrt(sxl*sxl+syl*syl+szl*szl);

  if(dist<=radius)
  {
    return BALLBODY;
  }
  else return 0.000000000000132213221323;
}
#if GCP>0
__device__ __host__
#endif
double initValueV1(const int i, const int j, const int k)
{
  const double dx=DX;
  const double dy=DX;
  const double dz=DX;

  const double omega=OMEGA;
  const double omy=OMY;
  const double omz=OMZ;

  const double xos = XOS;
  const double yos = YOS;
  const double zos = ZOS;

  const double yr=YR;
  const double zr=ZR;

  const double uy1=j*dy-yr;
  const double uz1=k*dz-zr;

  const double sxl=i*dx-xos;
  const double syl=j*dy-yos;
  const double szl=k*dz-zos;

  const double dist=sqrt(sxl*sxl+syl*syl+szl*szl);

  return omy*uz1-omz*uy1;
}
#if GCP>0
__device__ __host__
#endif
double initValueV2(const int i, const int j, const int k)
{
  const double dx=DX;
  const double dy=DX;
  const double dz=DX;

  const double omega=OMEGA;
  const double omx=OMX;
  const double omz=OMZ;

  const double xos = XOS;
  const double yos = YOS;
  const double zos = ZOS;

  const double xr=XR;
  const double zr=ZR;

  const double ux1=i*dx-xr;
  const double uz1=k*dz-zr;

  const double sxl=i*dx-xos;
  const double syl=j*dy-yos;
  const double szl=k*dz-zos;

  const double dist=sqrt(sxl*sxl+syl*syl+szl*szl);

  return omz*ux1-omx*uz1;
}
#if GCP>0
__device__ __host__
#endif
double initValueV3(const int i, const int j, const int k)
{
  const double dx=DX;
  const double dy=DX;
  const double dz=DX;

  const double omega=OMEGA;
  const double omx=OMX;
  const double omy=OMY;

  const double xos = XOS;
  const double yos = YOS;
  const double zos = ZOS;

  const double xr=XR;
  const double yr=YR;

  const double ux1=i*dx-xr;
  const double uy1=j*dy-yr;

  const double sxl=i*dx-xos;
  const double syl=j*dy-yos;
  const double szl=k*dz-zos;

  const double dist=sqrt(sxl*sxl+syl*syl+szl*szl);

  return omx*uy1-omy*ux1;
}
#if GCP>0
__device__ __host__
#endif
double initValueH(const int i, const int j, const int k)
{
  return DENSITYH;
}

#if GCP>0


__device__ __constant__ int mF;
__device__ __constant__ int mD;

template<typename T> __device__ inline int mG() { return 0; }
template<> __device__ inline int mG<float>() { return mF; }
template<> __device__ inline int mG<double>() { return mD; }

#define xg(i,j,k) x_[(i)*xOffL + j + (k)*xOff]
#define v1g(i,j,k) v1_[(i)*v1OffL + j + (k)*v1Off]
#define v2g(i,j,k) v2_[(i)*v2OffL + j + (k)*v2Off]
#define v3g(i,j,k) v3_[(i)*v3OffL + j + (k)*v3Off]
#define hg(i,j,k) h_[(i)*hOffL + j + (k)*hOff]
#define xPg(i,j,k) xP_[(i)*xOffL + j + (k)*xOff]
#define v1Pg(i,j,k) v1P_[(i)*v1POffL + j + (k)*v1POff]
#define v2Pg(i,j,k) v2P_[(i)*v2POffL + j + (k)*v2POff]
#define v3Pg(i,j,k) v3P_[(i)*v3POffL + j + (k)*v3POff]
#define cpg(i,j,k) cp_[(i)*cpOffL + j + (k)*cpOff]
#define cng(i,j,k) cn_[(i)*cnOffL + j + (k)*cnOff]

#define ep 1.e-10

/*
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
     std::cout << "NOT OK\n";
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
   else
   {
     std::cout << "OK\n";
     fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
   }
}
*/

#if(GCP>0 && GCP<100)
void UpdateGPUCPU::create()
{
#if GCU==0
  bodySize = bodyRow*3*(LDIM+6)*sizeof(realX);
  fullSize = fullRow*3*(LDIM+6)*sizeof(realX);

  cudaSetDevice(idGPU);
  cudaMalloc(&GPUtoCPU, bodySize);
  cudaMalloc(&GPUtoGPU, fullSize);
  cudaMemset(GPUtoCPU, 0, bodySize);
  cudaMemset(GPUtoGPU, 0, fullSize);
#endif

#if GCU==0
  cudaHostAlloc(&CPUtoCPU, bodySize, cudaHostAllocPortable);
  memset(CPUtoCPU, 0, bodySize);//without first touch policy
  cudaHostAlloc(&CPUtoGPU, fullSize, cudaHostAllocPortable);
  memset(CPUtoGPU, 0, fullSize);//without first touch policy
#else
  const int sizeV=3*mlGPU<realX>()*sizeof(realX);
  cudaHostAlloc(&CPUtoCPU, sizeV, cudaHostAllocPortable);
  memset(CPUtoCPU, 0, sizeV);//without first touch policy
  cudaHostAlloc(&CPUtoGPU, sizeV, cudaHostAllocPortable);
  memset(CPUtoGPU, 0, sizeV);//without first touch policy
#endif
}

UpdateGPUCPU::~UpdateGPUCPU()
{
#if GCU==0
  if(GPUtoCPU)
  {
    cudaSetDevice(idGPU);
    cudaFree(GPUtoCPU);
  }
  if(GPUtoGPU)
  {
    cudaSetDevice(idGPU);
    cudaFree(GPUtoGPU);
  }
#endif
  if(CPUtoCPU)
  {
    cudaFreeHost(CPUtoCPU);
  }
  if(CPUtoGPU)
  {
    cudaFreeHost(CPUtoGPU);
  }
}
#endif

__global__ void initKrn(realX * __restrict__ x_,
                     realV1 * __restrict__ v1_,
                     realV2 * __restrict__ v2_,
                     realV3 * __restrict__ v3_,
                     realH * __restrict__ h_,
                     realX * __restrict__ xP_,
                     const int n, const int m, const int id, const int procId,
                     const int iOffset, const int jOffset,
                     const int offY, const int offX)
{
  const int i = blockIdx.y * blockDim.y + threadIdx.y + iOffset;
  const int j = blockIdx.x * blockDim.x + threadIdx.x + jOffset;
  const int xOff=mG<realX>();
  const int xOffL=xOff*(LDIM+6);

  const int v1Off=mG<realV1>();
  const int v1OffL=v1Off*(LDIM+6);
  const int v2Off=mG<realV2>();
  const int v2OffL=v2Off*(LDIM+6);
  const int v3Off=mG<realV3>();
  const int v3OffL=v3Off*(LDIM+6);

  const int hOff=mG<realH>();
  const int hOffL=hOff*(LDIM+6);

//zamiast wszelkich ifów zrobic tablice wskaznikow do funkcji

  if(i<n && j<m)
  for(int k=-3; k<LDIM+3; ++k)
  {
    xg(i, j, k) = initValueX(i+offY, j+offX, k);

    v1g(i, j, k) = initValueV1(i+offY, j+offX, k);
    v2g(i, j, k) = initValueV2(i+offY, j+offX, k);
    v3g(i, j, k) = initValueV3(i+offY, j+offX, k);

    hg(i, j, k) = initValueH(i+offY, j+offX, k);

    xPg(i, j, k) = initValueX(i+offY, j+offX, k);
  }

  if(i<3 && j<m)
  {
    for(int k=-3; k<LDIM+3; ++k)
    {
      xg(i-3, j, k) = initValueX(i-3+offY, j+offX, k);
      xg(i+n, j, k) = initValueX(i+n+offY, j+offX, k);

      v1g(i-3, j, k) = initValueV1(i-3+offY, j+offX, k);
      v1g(i+n, j, k) = initValueV1(i+n+offY, j+offX, k);
      v2g(i-3, j, k) = initValueV2(i-3+offY, j+offX, k);
      v2g(i+n, j, k) = initValueV2(i+n+offY, j+offX, k);
      v3g(i-3, j, k) = initValueV3(i-3+offY, j+offX, k);
      v3g(i+n, j, k) = initValueV3(i+n+offY, j+offX, k);

      hg(i-3, j, k) = initValueH(i-3+offY, j+offX, k);
      hg(i+n, j, k) = initValueH(i+n+offY, j+offX, k);

      xPg(i-3, j, k) = initValueX(i-3+offY, j+offX, k);
      xPg(i+n, j, k) = initValueX(i+n+offY, j+offX, k);
    }
  }

  if(j<3 && i<n)
  {
    for(int k=-3; k<LDIM+3; ++k)
    {
      xg(i, j-3, k) = initValueX(i+offY, j-3+offX, k);
      xg(i, j+m, k) = initValueX(i+offY, j+m+offX, k);

      v1g(i, j-3, k) = initValueV1(i+offY, j-3+offX, k);
      v1g(i, j+m, k) = initValueV1(i+offY, j+m+offX, k);
      v2g(i, j-3, k) = initValueV2(i+offY, j-3+offX, k);
      v2g(i, j+m, k) = initValueV2(i+offY, j+m+offX, k);
      v3g(i, j-3, k) = initValueV3(i+offY, j-3+offX, k);
      v3g(i, j+m, k) = initValueV3(i+offY, j+m+offX, k);

      hg(i, j-3, k) = initValueH(i+offY, j-3+offX, k);
      hg(i, j+m, k) = initValueH(i+offY, j+m+offX, k);

      xPg(i, j-3, k) = initValueX(i+offY, j-3+offX, k);
      xPg(i, j+m, k) = initValueX(i+offY, j+m+offX, k);
    }
  }

  if(i<3 && j<3)
  for(int k=-3; k<LDIM+3; ++k)
  {
    xg(i-3, j-3, k) = initValueX(i-3+offY, j-3+offX, k);
    xg(i+n, j-3, k) = initValueX(i+n+offY, j-3+offX, k);
    xg(i-3, j+m, k) = initValueX(i-3+offY, j+m+offX, k);
    xg(i+n, j+m, k) = initValueX(i+n+offY, j+m+offX, k);

    v1g(i-3, j-3, k) = initValueV1(i-3+offY, j-3+offX, k);
    v1g(i+n, j-3, k) = initValueV1(i+n+offY, j-3+offX, k);
    v1g(i-3, j+m, k) = initValueV1(i-3+offY, j+m+offX, k);
    v1g(i+n, j+m, k) = initValueV1(i+n+offY, j+m+offX, k);
    v2g(i-3, j-3, k) = initValueV2(i-3+offY, j-3+offX, k);
    v2g(i+n, j-3, k) = initValueV2(i+n+offY, j-3+offX, k);
    v2g(i-3, j+m, k) = initValueV2(i-3+offY, j+m+offX, k);
    v2g(i+n, j+m, k) = initValueV2(i+n+offY, j+m+offX, k);
    v3g(i-3, j-3, k) = initValueV3(i-3+offY, j-3+offX, k);
    v3g(i+n, j-3, k) = initValueV3(i+n+offY, j-3+offX, k);
    v3g(i-3, j+m, k) = initValueV3(i-3+offY, j+m+offX, k);
    v3g(i+n, j+m, k) = initValueV3(i+n+offY, j+m+offX, k);

    hg(i-3, j-3, k) = initValueH(i-3+offY, j-3+offX, k);
    hg(i+n, j-3, k) = initValueH(i+n+offY, j-3+offX, k);
    hg(i-3, j+m, k) = initValueH(i-3+offY, j+m+offX, k);
    hg(i+n, j+m, k) = initValueH(i+n+offY, j+m+offX, k);

    xPg(i-3, j-3, k) = initValueX(i-3+offY, j-3+offX, k);
    xPg(i+n, j-3, k) = initValueX(i+n+offY, j-3+offX, k);
    xPg(i-3, j+m, k) = initValueX(i-3+offY, j+m+offX, k);
    xPg(i+n, j+m, k) = initValueX(i+n+offY, j+m+offX, k);
  }
}

__global__ void krn0(realX * __restrict__ x_,
                     realV1 * __restrict__ v1_,
                     realV2 * __restrict__ v2_,
                     realV3 * __restrict__ v3_,
                     realH * __restrict__ h_,
                     realX * __restrict__ xP_,
                     const int n, const int m, const int id, const int procId,
                     const int iOffset, const int jOffset)
{
  const int i = blockIdx.y * blockDim.y + threadIdx.y + iOffset;
  const int j = blockIdx.x * blockDim.x + threadIdx.x + jOffset;
  const int xOff=mG<realX>();
  const int xOffL=xOff*(LDIM+6);

  const int v1Off=mG<realV1>();
  const int v1OffL=v1Off*(LDIM+6);
  const int v2Off=mG<realV2>();
  const int v2OffL=v2Off*(LDIM+6);
  const int v3Off=mG<realV3>();
  const int v3OffL=v3Off*(LDIM+6);

  const int hOff=mG<realH>();
  const int hOffL=hOff*(LDIM+6);

//zamiast wszelkich ifów zrobic tablice wskaznikow do funkcji

//---------------------------------------
  if(i>=n+1 || j>=m+1) return;//niby na edge n+2 i m+2 - tu caly czas n+2 i m+2 - bo jeden node

//  const int Ml = M*l;
//  int ijk = i*Ml + j + off;

  __shared__ realV1 iQ[KRNAY+1][KRNAX];
  __shared__ realV2 jQ[KRNAY][KRNAX+1];

  realV3 q0, q1;

  q0 =  fmax((0.0f),v3g(i,j,1)  )*xg(i,j,0)
       +fmin((0.0f),v3g(i,j,1)  )*xg(i,j,1);

  xPg(i, j, 0)=xg(i, j, 0)-( fmax((0.0f),v1g(i+1,j,0))*xg(i,j,0)
                     +fmin((0.0f),v1g(i+1,j,0))*xg(i+1,j,0)
                     -fmax((0.0f),v1g(i,j,0)  )*xg(i-1,j,0)
                     -fmin((0.0f),v1g(i,j,0)  )*xg(i,j,0)
                     +fmax((0.0f),v2g(i,j+1,0))*xg(i,j,0)
                     +fmin((0.0f),v2g(i,j+1,0))*xg(i,j+1,0)
                     -fmax((0.0f),v2g(i,j,0)  )*xg(i,j-1,0)
                     -fmin((0.0f),v2g(i,j,0)  )*xg(i,j,0)
                     +(2.0f)*q0
                    )/hg(i, j, 0);

  for(int k=1; k<LDIM-1; ++k)
  {
    __syncthreads();

    if(threadIdx.y==KRNAY-1)
    {
      iQ[KRNAY][threadIdx.x] =
         fmax((0.0f),v1g(i+1,j,k))*xg(i,j,k)
        +fmin((0.0f),v1g(i+1,j,k))*xg(i+1,j,k);

      if(threadIdx.x<KRNAY)
      {
        const int I=i-threadIdx.y+threadIdx.x;
        const int J=j-threadIdx.x+KRNAX-1;
        jQ[threadIdx.x][KRNAX] =
           fmax((0.0f),v2g(I,J+1,k))*xg(I,J,k)
          +fmin((0.0f),v2g(I,J+1,k))*xg(I,J+1,k);
      }
    }

    iQ[threadIdx.y][threadIdx.x] =
         fmax((0.0f),v1g(i,j,k)  )*xg(i-1,j,k)
        +fmin((0.0f),v1g(i,j,k)  )*xg(i,j,k);

    jQ[threadIdx.y][threadIdx.x] =
         fmax((0.0f),v2g(i,j,k)  )*xg(i,j-1,k)
        +fmin((0.0f),v2g(i,j,k)  )*xg(i,j,k);

    __syncthreads();

    q1 =  fmax((0.0f),v3g(i,j,k+1))*xg(i,j,k)
         +fmin((0.0f),v3g(i,j,k+1))*xg(i,j,k+1);

    xPg(i, j, k)=xg(i, j, k)-( iQ[threadIdx.y+1][threadIdx.x]
                       -iQ[threadIdx.y][threadIdx.x]
                       +jQ[threadIdx.y][threadIdx.x+1]
                       -jQ[threadIdx.y][threadIdx.x]
                       +q1
                       -q0
                      )/hg(i, j, k);
    q0=q1;
  }
  const int k=LDIM-1;

  xPg(i, j, k)=xg(i, j, k)-( fmax((0.0f),v1g(i+1,j,k))*xg(i,j,k)
                     +fmin((0.0f),v1g(i+1,j,k))*xg(i+1,j,k)
                     -fmax((0.0f),v1g(i,j,k)  )*xg(i-1,j,k)
                     -fmin((0.0f),v1g(i,j,k)  )*xg(i,j,k)
                     +fmax((0.0f),v2g(i,j+1,k))*xg(i,j,k)
                     +fmin((0.0f),v2g(i,j+1,k))*xg(i,j+1,k)
                     -fmax((0.0f),v2g(i,j,k)  )*xg(i,j-1,k)
                     -fmin((0.0f),v2g(i,j,k)  )*xg(i,j,k)
                     -(2.0f)*q1
                    )/hg(i, j, k);
}

__global__ void krn1(realV1 * __restrict__ v1_,
                     realV2 * __restrict__ v2_,
                     realV3 * __restrict__ v3_,
                     realH * __restrict__ h_,
                     realX * __restrict__ xP_,
                     realV1P * __restrict__ v1P_,
                     realV2P * __restrict__ v2P_,
                     realV3P * __restrict__ v3P_,
                     const int n, const int m, const int id,
                     const int iOffset, const int jOffset)
{
  const int i = blockIdx.y * blockDim.y + threadIdx.y + iOffset;
  const int j = blockIdx.x * blockDim.x + threadIdx.x + jOffset;
  const int xOff=mG<realX>();
  const int xOffL=xOff*(LDIM+6);

  const int v1Off=mG<realV1>();
  const int v1OffL=v1Off*(LDIM+6);
  const int v2Off=mG<realV2>();
  const int v2OffL=v2Off*(LDIM+6);
  const int v3Off=mG<realV3>();
  const int v3OffL=v3Off*(LDIM+6);

  const int v1POff=mG<realV1P>();
  const int v1POffL=v1POff*(LDIM+6);
  const int v2POff=mG<realV2P>();
  const int v2POffL=v2POff*(LDIM+6);
  const int v3POff=mG<realV3P>();
  const int v3POffL=v3POff*(LDIM+6);

  const int hOff=mG<realH>();
  const int hOffL=hOff*(LDIM+6);


//  const int cmpT = !(edge&TOP);
//  const int cmpB = 2*!(edge&BTM);

  if(i>=n+1 || j>=m+1) return;//jak cos to +1 a nie +2 - bo od -1

//  const int Ml = M*l;
//  int ijk = i*Ml + j + off;

  const bool eR = j==MDIM;//m-1;
  const bool eTL = i==0 && j==0;
  const bool eB = i==NDIM;//n-1;//true;//i==nMod && edge&BTM;

  v1Pg(i, j, 0)=(0.0f);
  if(i<n+1) v2Pg(i, j, 0)=(0.0f);
  if(eB) v1Pg(i+1, j, 0)=(0.0f);
  if(eR) v2Pg(i, j+1, 0)=(0.0f);

  if(!eTL && !eB)
  {
    const realV2 v2H = v2g(i,j,0)/(hg(i,j-1,0)+hg(i,j,0));
    v2Pg(i, j, 0)=((0.5f)*fabs(v2g(i,j,0))-v2g(i,j,0)*v2H
             )*(xPg(i,j,0)-xPg(i,j-1,0))
             -(0.0625f)*v2H*
               (v1g(i,j-1,0)+v1g(i,j,0)+v1g(i+1,j,0)+v1g(i+1,j-1,0 ))*
               (xPg(i+1,j,0)+xPg(i+1,j-1,0)-xPg(i-1,j,0)-xPg(i-1,j-1,0));
  }
//  ijk+=M;

  if(i<n+1)//gdy jest wiecej niz jeden strumien/wezel
  {
    if(eTL)
      for(int k=1; k<LDIM-1; ++k)
      {
        v1Pg(i, j, k)=(0.0f);
        v2Pg(i, j, k)=(0.0f);
        v3Pg(i, j, k)=(0.0f);

        if(eR) { v1Pg(i, j, k)=(0.0f); v2Pg(i, j+1, k)=(0.0f); v3Pg(i, j, k)=(0.0f); }
        if(eB) { v1Pg(i+1, j, k)=(0.0f); v2Pg(i, j, k)=(0.0f); v3Pg(i, j, k)=(0.0f); }
  //      ijk+=M;
      }
    else
      for(int k=1; k<LDIM-1; ++k)
      {
        const realV1 v1H = v1g(i,j,k)/(hg(i-1,j,k)+hg(i,j,k));
        v1Pg(i, j, k)=((0.5f)*fabs(v1g(i,j,k))-v1g(i,j,k)*v1H
                 )*(xPg(i,j,k)-xPg(i-1,j,k))
                 -(0.0625f)*v1H*
                 (
                   (v2g(i-1,j,k)+v2g(i-1,j+1,k)+v2g(i,j+1,k)+v2g(i,j,k))*
                   (xPg(i,j+1,k)+xPg(i-1,j+1,k)-xPg(i,j-1,k)-xPg(i-1,j-1,k))+
                   (v3g(i-1,j,k)+v3g(i-1,j,k+1)+v3g(i,j,k+1)+v3g(i,j,k))*
                   (xPg(i,j,k+1)+xPg(i-1,j,k+1)-xPg(i,j,k-1)-xPg(i-1,j,k-1))
                 );

        const realV2 v2H = v2g(i,j,k)/(hg(i,j-1,k)+hg(i,j,k));
        v2Pg(i, j, k)=((0.5f)*fabs(v2g(i,j,k))-v2g(i,j,k)*v2H
                 )*(xPg(i,j,k)-xPg(i,j-1,k))
                 -(0.0625f)*v2H*
                 (
                   (v1g(i,j-1,k)+v1g(i,j,k)+v1g(i+1,j,k)+v1g(i+1,j-1,k ))*
                   (xPg(i+1,j,k)+xPg(i+1,j-1,k)-xPg(i-1,j,k)-xPg(i-1,j-1,k))+
                   (v3g(i,j-1,k)+v3g(i,j,k)+v3g(i,j,k+1)+v3g(i,j-1,k+1))*
                   (xPg(i,j,k+1)+xPg(i,j-1,k+1)-xPg(i,j,k-1)-xPg(i,j-1,k-1))
                 );

        const realV3 v3H = v3g(i,j,k)/(hg(i,j,k-1)+hg(i,j,k));
        v3Pg(i, j, k)=((0.5f)*fabs(v3g(i,j,k))-v3g(i,j,k)*v3H
                 )*(xPg(i,j,k)-xPg(i,j,k-1))
                 -(0.0625f)*v3H*
                 (
                   (v1g(i,j,k-1)+v1g(i,j,k)+v1g(i+1,j,k)+v1g(i+1,j,k-1))*
                   (xPg(i+1,j,k)+xPg(i+1,j,k-1)-xPg(i-1,j,k)-xPg(i-1,j,k-1))+
                   (v2g(i,j,k-1)+v2g(i,j+1,k-1)+v2g(i,j+1,k)+v2g(i,j,k))*
                   (xPg(i,j+1,k)+xPg(i,j+1,k-1)-xPg(i,j-1,k)-xPg(i,j-1,k-1))
                 );

        if(eR) { v1Pg(i, j, k)=(0.0f); v2Pg(i, j+1, k)=(0.0f); v3Pg(i, j, k)=(0.0f); }
        if(eB) { v1Pg(i+1, j, k)=(0.0f); v2Pg(i, j, k)=(0.0f); v3Pg(i, j, k)=(0.0f); }
//        ijk+=M;
      }
  }
  else
  {
    for(int k=1; k<LDIM-1; ++k)
    {
      if(eTL || eR )
      {
        v1Pg(i, j, k)=(0.0f);
      }
      else
      {
        const realV1 v1H = v1g(i,j,k)/(hg(i-1,j,k)+hg(i,j,k));
        v1Pg(i, j, k)=((0.5f)*fabs(v1g(i,j,k))-v1g(i,j,k)*v1H
                 )*(xPg(i,j,k)-xPg(i-1,j,k))
                 -(0.0625f)*v1H*
                 (
                   (v2g(i-1,j,k)+v2g(i-1,j+1,k)+v2g(i,j+1,k)+v2g(i,j,k))*
                   (xPg(i,j+1,k)+xPg(i-1,j+1,k)-xPg(i,j-1,k)-xPg(i-1,j-1,k))+
                   (v3g(i-1,j,k)+v3g(i-1,j,k+1)+v3g(i,j,k+1)+v3g(i,j,k))*
                   (xPg(i,j,k+1)+xPg(i-1,j,k+1)-xPg(i,j,k-1)-xPg(i-1,j,k-1))
                 );
      }
      if(eB) v1Pg(i+1, j, k)=(0.0f);
//      ijk+=M;
    }
  }
  const int k=LDIM-1;

  if(i<n+1) v3Pg(i, j, 0)=-v3Pg(i, j, 1);
  v1Pg(i, j, k)=(0.0f);
  if(eB) v1Pg(i+1, j, k)=(0.0f);

  if(i<n+1)//gdy jest wiecej niz jeden strumien/wezel
  {
    v2Pg(i, j, k)=(0.0f);
    if(eR) v2Pg(i, j+1, k)=(0.0f);

    if(!eTL && !eB)
    {
      const realV2 v2H = v2g(i,j,k)/(hg(i,j-1,k)+hg(i,j,k));
      v2Pg(i, j, k)=((0.5f)*fabs(v2g(i,j,k))-v2g(i,j,k)*v2H
               )*(xPg(i,j,k)-xPg(i,j-1,k))
               -(0.0625f)*v2H*
                 (v1g(i,j-1,k)+v1g(i,j,k)+v1g(i+1,j,k)+v1g(i+1,j-1,k ))*
                 (xPg(i+1,j,k)+xPg(i+1,j-1,k)-xPg(i-1,j,k)-xPg(i-1,j-1,k));
    }

    if(eTL || eB || eR)
    {
      v3Pg(i, j, k)=(0.0f);
    }
    else
    {
      const realV3 v3H = v3g(i,j,k)/(hg(i,j,k-1)+hg(i,j,k));
      v3Pg(i, j, k)=((0.5f)*fabs(v3g(i,j,k))-v3g(i,j,k)*v3H
               )*(xPg(i,j,k)-xPg(i,j,k-1))
               -(0.0625f)*v3H*
               (
                 (v1g(i,j,k-1)+v1g(i,j,k)+v1g(i+1,j,k)+v1g(i+1,j,k-1))*
                 (xPg(i+1,j,k)+xPg(i+1,j,k-1)-xPg(i-1,j,k)-xPg(i-1,j,k-1))+
                 (v2g(i,j,k-1)+v2g(i,j+1,k-1)+v2g(i,j+1,k)+v2g(i,j,k))*
                 (xPg(i,j+1,k)+xPg(i,j+1,k-1)-xPg(i,j-1,k)-xPg(i,j-1,k-1))
               );
    }
  }
}

__global__ void krn2(realX * __restrict__ x_,
                     realH * __restrict__ h_,
                     realX * __restrict__ xP_,
                     realV1P * __restrict__ v1P_,
                     realV2P * __restrict__ v2P_,
                     realV3P * __restrict__ v3P_,
                     realCP * __restrict__ cp_,
                     realCN * __restrict__ cn_,
                     const int n, const int m, const int id,
                     const int iOffset, const int jOffset)
{
  const int i = blockIdx.y * blockDim.y + threadIdx.y + iOffset;
  const int j = blockIdx.x * blockDim.x + threadIdx.x + jOffset;
  const int xOff=mG<realX>();
  const int xOffL=xOff*(LDIM+6);

  const int cpOff=mG<realCP>();
  const int cpOffL=cpOff*(LDIM+6);
  const int cnOff=mG<realCN>();
  const int cnOffL=cnOff*(LDIM+6);

  const int v1POff=mG<realV1P>();
  const int v1POffL=v1POff*(LDIM+6);
  const int v2POff=mG<realV2P>();
  const int v2POffL=v2POff*(LDIM+6);
  const int v3POff=mG<realV3P>();
  const int v3POffL=v3POff*(LDIM+6);

  const int hOff=mG<realH>();
  const int hOffL=hOff*(LDIM+6);

//  const int cmpT = !(edge&TOP);
//  const int cmpB = !(edge&BTM);


  if(i>=n || j>=m) return;

//  const int Ml = M*l;
//  int ijk = i*Ml + j + off;

  const bool eL = j==0;//    && edge&LFT;
  const bool eR = j==MDIM;//m-1;
  const bool eT = i==0;//    && edge&TOP;
  const bool eB = i==NDIM;//n-1;

  realX xim, xip, xjm, xjp, xkm, xkp, xc;
  realX xPim, xPip, xPjm, xPjp, xPkm, xPkp, xPc;

  realV3 v3c = v3Pg(i,j,0);
  realV3 v3Pg = v3Pg(i,j,1);

  xkm = xg(i,j,0);
  xPkm = xPg(i,j,0);

  xc = xkm;
  xPc = xPkm;

  xkp = xg(i,j,1);
  xPkp = xPg(i,j,1);

  if(eT) { xim = xc; xPim = xPc; }
  else { xim = xg(i-1,j,0); xPim = xPg(i-1,j,0); }

  if(eB) { xip = xc; xPip = xPc; }
  else { xip = xg(i+1,j,0); xPip = xPg(i+1,j,0); }

  if(eL) { xjm = xc; xPjm = xPc; }
  else { xjm = xg(i,j-1,0); xPjm = xPg(i,j-1,0); }

  if(eR) { xjp = xc; xPjp = xPc; }
  else { xjp = xg(i,j+1,0); xPjp = xPg(i,j+1,0); }

  cpg(i, j, 0)=(fmax(fmax(fmax(fmax(
           fmax(fmax(fmax(fmax(
           fmax(fmax(fmax(fmax(
           fmax(xc,xim),xip),
                xjm),xjp),xkm),xkp),
                xPim),xPip),xPjm),xPjp),
                xPkm),xPkp),xPc)-xPc
          )*hg(i,j,0)/( -fmin((0.0f),v1Pg(i+1,j,0))+fmax((0.0f),v1Pg(i,j,0))
                        -fmin((0.0f),v2Pg(i,j+1,0))+fmax((0.0f),v2Pg(i,j,0))
                        -fmin((0.0f),v3Pg)+fmax((0.0f),v3c)+ep);

  cng(i, j, 0)=(xPc-fmin(fmin(fmin(fmin(
           fmin(fmin(fmin(fmin(
           fmin(fmin(fmin(fmin(
           fmin(xc,xim),xip),
                xjm),xjp),xkm),xkp),
                xPim),xPip),xPjm),xPjp),
                xPkm),xPkp),xPc)
          )*hg(i,j,0)/(  fmax((0.0f),v1Pg(i+1,j,0))-fmin((0.0f),v1Pg(i,j,0))
                        +fmax((0.0f),v2Pg(i,j+1,0))-fmin((0.0f),v2Pg(i,j,0))
                        +fmax((0.0f),v3Pg)-fmin((0.0f),v3c)+ep);
//  ijk+=M;

  for(int k=1; k<LDIM-1; ++k)
  {
    xkm = xc;
    xPkm = xPc;

    xc = xkp;
    xPc = xPkp;

    xkp = xg(i,j,k+1);
    xPkp = xPg(i,j,k+1);

    if(eT) { xim = xc; xPim = xPc; }
    else { xim = xg(i-1,j,k); xPim = xPg(i-1,j,k); }

    if(eB) { xip = xc; xPip = xPc; }
    else { xip = xg(i+1,j,k); xPip = xPg(i+1,j,k); }

    if(eL) { xjm = xc; xPjm = xPc; }
    else { xjm = xg(i,j-1,k); xPjm = xPg(i,j-1,k); }

    if(eR) { xjp = xc; xPjp = xPc; }
    else { xjp = xg(i,j+1,k); xPjp = xPg(i,j+1,k); }

    v3c = v3Pg;
    v3Pg = v3Pg(i,j,k+1);

    const realV1 v1 = v1Pg(i,j,k);
    const realV2 v2 = v2Pg(i,j,k);
    const realV1P v1Pg = v1Pg(i+1,j,k);
    const realV2P v2Pg = v2Pg(i,j+1,k);

    cpg(i, j, k)=(fmax(fmax(fmax(fmax(xc,xim),
                            fmax(xip,xjm)),
                       fmax(fmax(xjp,xkm),
                            fmax(xkp,xPim))),
                  fmax(fmax(fmax(xPip,xPjm),
                            fmax(xPjp,xPkm)),
                            fmax(xPkp,xPc)))-xPc
            )*hg(i,j,k)/( -fmin((0.0f),v1Pg)+fmax((0.0f),v1)
                          -fmin((0.0f),v2Pg)+fmax((0.0f),v2)
                          -fmin((0.0f),v3Pg)+fmax((0.0f),v3c)+ep);

    cng(i, j, k)=(xPc-fmin(fmin(fmin(fmin(xc,xim),
                           fmin(xip,xjm)),
                      fmin(fmin(xjp,xkm),
                           fmin(xkp,xPim))),
                 fmin(fmin(fmin(xPip,xPjm),
                           fmin(xPjp,xPkm)),
                           fmin(xPkp,xPc)))
            )*hg(i,j,k)/(  fmax((0.0f),v1Pg)-fmin((0.0f),v1)
                          +fmax((0.0f),v2Pg)-fmin((0.0f),v2)
                          +fmax((0.0f),v3Pg)-fmin((0.0f),v3c)+ep);

//    ijk+=M;
  }
  const int k=LDIM-1;

  xkm = xc;
  xPkm = xPc;

  xc = xkp;
  xPc = xPkp;

  xkp = xc;
  xPkp = xPc;

  if(eT) { xim = xc; xPim = xPc; }
  else { xim = xg(i-1,j,k); xPim = xPg(i-1,j,k); }

  if(eB) { xip = xc; xPip = xPc; }
  else { xip = xg(i+1,j,k); xPip = xPg(i+1,j,k); }

  if(eL) { xjm = xc; xPjm = xPc; }
  else { xjm = xg(i,j-1,k); xPjm = xPg(i,j-1,k); }

  if(eR) { xjp = xc; xPjp = xPc; }
  else { xjp = xg(i,j+1,k); xPjp = xPg(i,j+1,k); }

  v3c = v3Pg;

  cpg(i, j, k)=(fmax(fmax(fmax(fmax(
           fmax(fmax(fmax(fmax(
           fmax(fmax(fmax(fmax(
           fmax(xc,xim),xip),
                xjm),xjp),xkm),xkp),
                xPim),xPip),xPjm),xPjp),
                xPkm),xPkp),xPc)-xPc
          )*hg(i,j,k)/( -fmin((0.0f),v1Pg(i+1,j,k))+fmax((0.0f),v1Pg(i,j,k))
                        -fmin((0.0f),v2Pg(i,j+1,k))+fmax((0.0f),v2Pg(i,j,k))
                        -fmin((0.0f),-v3c)+fmax((0.0f),v3c)+ep);

  cng(i, j, k)=(xPc-fmin(fmin(fmin(fmin(
           fmin(fmin(fmin(fmin(
           fmin(fmin(fmin(fmin(
           fmin(xc,xim),xip),
                xjm),xjp),xkm),xkp),
                xPim),xPip),xPjm),xPjp),
                xPkm),xPkp),xPc)
          )*hg(i,j,k)/(  fmax((0.0f),v1Pg(i+1,j,k))-fmin((0.0f),v1Pg(i,j,k))
                        +fmax((0.0f),v2Pg(i,j+1,k))-fmin((0.0f),v2Pg(i,j,k))
                        +fmax((0.0f),-v3c)-fmin((0.0f),v3c)+ep);
}

__global__ void krn3(realX * __restrict__ x_,
                     realH * __restrict__ h_,
                     realX * __restrict__ xP_,
                     realV1P * __restrict__ v1P_,
                     realV2P * __restrict__ v2P_,
                     realV3P * __restrict__ v3P_,
                     realCP * __restrict__ cp_,
                     realCN * __restrict__ cn_,
                     const int n, const int m, const int id,
                     const int iOffset, const int jOffset)
{
  const int i = blockIdx.y * blockDim.y + threadIdx.y + iOffset;
  const int j = blockIdx.x * blockDim.x + threadIdx.x + jOffset;
  const int xOff=mG<realX>();
  const int xOffL=xOff*(LDIM+6);

  const int cpOff=mG<realCP>();
  const int cpOffL=cpOff*(LDIM+6);
  const int cnOff=mG<realCN>();
  const int cnOffL=cnOff*(LDIM+6);

  const int v1POff=mG<realV1P>();
  const int v1POffL=v1POff*(LDIM+6);
  const int v2POff=mG<realV2P>();
  const int v2POffL=v2POff*(LDIM+6);
  const int v3POff=mG<realV3P>();
  const int v3POffL=v3POff*(LDIM+6);

  const int hOff=mG<realH>();
  const int hOffL=hOff*(LDIM+6);

  if(i>=n || j>=m) return;

  __shared__ realV1P iQ[KRNDY+1][KRNDX];
  __shared__ realV2P jQ[KRNDY][KRNDX+1];

  realV3 q0, q1;

  q1 = fmax((0.0f),v3Pg(i,j,1))*fmin(fmin((1.0f),cpg(i,j,1)),cng(i,j,0))
      +fmin((0.0f),v3Pg(i,j,1))*fmin(fmin((1.0f),cpg(i,j,0)),  cng(i,j,1));

  xg(i, j, 0)=xPg(i,j,0)-(fmax((0.0f),v1Pg(i+1,j,0))*fmin(fmin((1.0f),cpg(i+1,j,0)),cng(i,j,0))
                    +fmin((0.0f),v1Pg(i+1,j,0))*fmin(fmin((1.0f),cpg(i,j,0)  ),cng(i+1,j,0))
                    -fmax((0.0f),v1Pg(i,j,0))  *fmin(fmin((1.0f),cpg(i,j,0)  ),cng(i-1,j,0))
                    -fmin((0.0f),v1Pg(i,j,0))  *fmin(fmin((1.0f),cpg(i-1,j,0)),cng(i,j,0))
                    +fmax((0.0f),v2Pg(i,j+1,0))*fmin(fmin((1.0f),cpg(i,j+1,0)),cng(i,j,0))
                    +fmin((0.0f),v2Pg(i,j+1,0))*fmin(fmin((1.0f),cpg(i,j,0)),  cng(i,j+1,0))
                    -fmax((0.0f),v2Pg(i,j,0))  *fmin(fmin((1.0f),cpg(i,j,0)),  cng(i,j-1,0))
                    -fmin((0.0f),v2Pg(i,j,0))  *fmin(fmin((1.0f),cpg(i,j-1,0)),cng(i,j,0))
                    +q1
                    +fmax((0.0f),v3Pg(i,j,1))  *fmin(fmin((1.0f),cpg(i,j,1)),  cng(i,j,0))
                    +fmin((0.0f),v3Pg(i,j,1))  *fmin(fmin((1.0f),cpg(i,j,0)),  cng(i,j,1))
                    )/hg(i,j,0);
//  ijk+=M;

  for(int k=1; k<LDIM-1; ++k)
  {
    __syncthreads();

    if(threadIdx.y==KRNDY-1)
    {
      iQ[KRNDY][threadIdx.x] =
         fmax((0.0f),v1Pg(i+1,j,k))*fmin(fmin((1.0f),cpg(i+1,j,k)),cng(i,j,k))
        +fmin((0.0f),v1Pg(i+1,j,k))*fmin(fmin((1.0f),cpg(i,j,k)  ),cng(i+1,j,k));

      if(threadIdx.x<KRNDY)
      {
        const int I=i-threadIdx.y+threadIdx.x;
        const int J=j-threadIdx.x+KRNDX-1;
        jQ[threadIdx.x][KRNDX] =
           fmax((0.0f),v2Pg(I,J+1,k))*fmin(fmin((1.0f),cpg(I,J+1,k)),cng(I,J,k))
          +fmin((0.0f),v2Pg(I,J+1,k))*fmin(fmin((1.0f),cpg(I,J,k)),  cng(I,J+1,k));
      }
    }

    iQ[threadIdx.y][threadIdx.x] =
         fmax((0.0f),v1Pg(i,j,k))  *fmin(fmin((1.0f),cpg(i,j,k)  ),cng(i-1,j,k))
        +fmin((0.0f),v1Pg(i,j,k))  *fmin(fmin((1.0f),cpg(i-1,j,k)),cng(i,j,k));

    jQ[threadIdx.y][threadIdx.x] =
         fmax((0.0f),v2Pg(i,j,k))  *fmin(fmin((1.0f),cpg(i,j,k)),  cng(i,j-1,k))
        +fmin((0.0f),v2Pg(i,j,k))  *fmin(fmin((1.0f),cpg(i,j-1,k)),cng(i,j,k));

    __syncthreads();

    q0 = fmax((0.0f),v3Pg(i,j,k+1))*fmin(fmin((1.0f),cpg(i,j,k+1)),cng(i,j,k))
        +fmin((0.0f),v3Pg(i,j,k+1))*fmin(fmin((1.0f),cpg(i,j,k)),  cng(i,j,k+1));

    xg(i, j, k)=xPg(i,j,k)-(
                       iQ[threadIdx.y+1][threadIdx.x]
                      -iQ[threadIdx.y][threadIdx.x]
                      +jQ[threadIdx.y][threadIdx.x+1]
                      -jQ[threadIdx.y][threadIdx.x]
                      +q0
                      -q1
                      )/hg(i,j,k);
    q1=q0;
 //   ijk+=M;
  }
  const int k=LDIM-1;
  xg(i, j, k)=xPg(i,j,k)-(fmax((0.0f),v1Pg(i+1,j,k))*fmin(fmin((1.0f),cpg(i+1,j,k)),cng(i,j,k))
                    +fmin((0.0f),v1Pg(i+1,j,k))*fmin(fmin((1.0f),cpg(i,j,k)  ),cng(i+1,j,k))
                    -fmax((0.0f),v1Pg(i,j,k))  *fmin(fmin((1.0f),cpg(i,j,k)  ),cng(i-1,j,k))
                    -fmin((0.0f),v1Pg(i,j,k))  *fmin(fmin((1.0f),cpg(i-1,j,k)),cng(i,j,k))
                    +fmax((0.0f),v2Pg(i,j+1,k))*fmin(fmin((1.0f),cpg(i,j+1,k)),cng(i,j,k))
                    +fmin((0.0f),v2Pg(i,j+1,k))*fmin(fmin((1.0f),cpg(i,j,k)),  cng(i,j+1,k))
                    -fmax((0.0f),v2Pg(i,j,k))  *fmin(fmin((1.0f),cpg(i,j,k)),  cng(i,j-1,k))
                    -fmin((0.0f),v2Pg(i,j,k))  *fmin(fmin((1.0f),cpg(i,j-1,k)),cng(i,j,k))
                    -(2.0f)*fmax((0.0f),v3Pg(i,j,k))*fmin(fmin((1.0f),cpg(i,j,k)),cng(i,j,k-1))
                    -(2.0f)*fmin((0.0f),v3Pg(i,j,k))*fmin(fmin((1.0f),cpg(i,j,k-1)),cng(i,j,k))
                    )/hg(i,j,k);
}

#define dst(i,j,k) dst_[(i)*xOffL + j + (k)*xOff]
#define src(i,j,k) src_[(i)*xOffL + j + (k)*xOff]

__global__ void cpyKrnLR(realX *dst_, realX *src_, const int n, const int m)
{
    const int i = blockIdx.y * blockDim.y + threadIdx.y;
    const int j = blockIdx.x * blockDim.x + threadIdx.x;
    const int xOff=mG<realX>();
    const int xOffL=xOff*(LDIM+6);

    if(j<3 && i<n)
    for(int k=-3; k<LDIM+3; ++k)
    {
      dst(i, j-3, k) = src(i, j+m-3, k);
    }
}

__global__ void cpyKrnRL(realX *dst_, realX *src_, const int n, const int m)
{
    const int i = blockIdx.y * blockDim.y + threadIdx.y;
    const int j = blockIdx.x * blockDim.x + threadIdx.x;
    const int xOff=mG<realX>();
    const int xOffL=xOff*(LDIM+6);

    if(j<3 && i<n)
    for(int k=-3; k<LDIM+3; ++k)
    {
      dst(i, j+m, k) = src(i, j, k);
    }
}

__global__ void cpyKrnLRRL(realX *dst_, realX *src_, const int n, const int m)
{
    const int i = blockIdx.y * blockDim.y + threadIdx.y;
    const int j = blockIdx.x * blockDim.x + threadIdx.x;
    const int xOff=mG<realX>();
    const int xOffL=xOff*(LDIM+6);

    if(j<3 && i<n)
    for(int k=-3; k<LDIM+3; ++k)
    {
      dst(i, j-3, k) = src(i, j+m-3, k);
      src(i, j+m, k) = dst(i, j, k);
    }
}

__global__ void cpyKrnTB(realX *dst_, realX *src_, const int n, const int m)
{
    const int i = blockIdx.y * blockDim.y + threadIdx.y;
    const int j = blockIdx.x * blockDim.x + threadIdx.x;
    const int xOff=mG<realX>();
    const int xOffL=xOff*(LDIM+6);

    if(i<3 && j<m+6)
    for(int k=-3; k<LDIM+3; ++k)
    {
      dst(i-3, j-3, k) = src(i+n-3, j-3, k);
    }
}

__global__ void cpyKrnBT(realX *dst_, realX *src_, const int n, const int m)
{
    const int i = blockIdx.y * blockDim.y + threadIdx.y;
    const int j = blockIdx.x * blockDim.x + threadIdx.x;
    const int xOff=mG<realX>();
    const int xOffL=xOff*(LDIM+6);

    if(i<3 && j<m+6)
    for(int k=-3; k<LDIM+3; ++k)
    {
      dst(i+n, j-3, k) = src(i, j-3, k);
    }
}

__global__ void cpyKrnTBBT(realX *dst_, realX *src_, const int n, const int m)
{
    const int i = blockIdx.y * blockDim.y + threadIdx.y;
    const int j = blockIdx.x * blockDim.x + threadIdx.x;
    const int xOff=mG<realX>();
    const int xOffL=xOff*(LDIM+6);

    if(i<3 && j<m+6)
    for(int k=-3; k<LDIM+3; ++k)
    {
      dst(i-3, j-3, k) = src(i+n-3, j-3, k);
      src(i+n, j-3, k) = dst(i, j-3, k);
    }
}

#if(GCP>0 && GCP<100) //start GPUCPU update

#if GCU==0
#define dstBuf(i,j,k) dst_[(i)*offL + j + (k)*m]

__global__ void cpyGPUtoBuf(realX *dst_, realX *src_,
                            const int n, const int m,
                            const int offset, bool GPUaboveCPU)
{
  const int i = blockIdx.y * blockDim.y + threadIdx.y;
  const int j = blockIdx.x * blockDim.x + threadIdx.x;
  const int xOff=mG<realX>();
  const int xOffL=xOff*(LDIM+6);
  const int offL=m*(LDIM+6);

  if(i<3 && j<m)
  for(int k=-3; k<LDIM+3; ++k)
  {
    if(GPUaboveCPU)
    {
      dstBuf(i, j, k+3) = src(i+n-3, j+offset, k);
    }
    else
    {
      dstBuf(i, j, k+3) = src(i, j+offset, k);
    }
  }
}

__global__ void cpyBufToGPU(realX *src_, realX *dst_,
                            const int n, const int m,
                            const int offset, bool GPUaboveCPU)
{
  const int i = blockIdx.y * blockDim.y + threadIdx.y;
  const int j = blockIdx.x * blockDim.x + threadIdx.x;
  const int xOff=mG<realX>();
  const int xOffL=xOff*(LDIM+6);
  const int offL=m*(LDIM+6);

  if(i<3 && j<m)
  for(int k=-3; k<LDIM+3; ++k)
  {
    if(GPUaboveCPU)
    {
      src(i+n, j+offset, k) = dstBuf(i, j, k+3);
    }
    else
    {
      src(i-3, j+offset, k) = dstBuf(i, j, k+3);
    }
  }
}
#endif

void cpyGPUtoBuf_(UpdateGPUCPU *bufGPU,
                 MatrixGPU<realX> *xG, bool GPUaboveCPU)
{
#if GCU==0
  if(GPUaboveCPU)
  {
    cudaSetDevice(bufGPU->idGPU);
    cpyGPUtoBuf<<<bufGPU->nB, bufGPU->tB, 0,
      mdGPU.stream[xG[bufGPU->matrix].lSiM]>>>
      (bufGPU->GPUtoCPU, xG[bufGPU->matrix].fst,
      xG[bufGPU->matrix].n, bufGPU->bodyRow, bufGPU->offset, GPUaboveCPU);

    cudaMemcpyAsync(bufGPU->CPUtoCPU, bufGPU->GPUtoCPU, bufGPU->bodySize,
      cudaMemcpyDeviceToHost,
      mdGPU.stream[xG[bufGPU->matrix].lSiM]);
    cudaEventRecord(xG[bufGPU->matrix].updateGPUCPUevent, mdGPU.stream[xG[bufGPU->matrix].lSiM]);
  }
  else
  {
    cudaSetDevice(bufGPU->idGPU);
    cpyGPUtoBuf<<<bufGPU->nB, bufGPU->tB, 0,
      mdGPU.stream[xG[bufGPU->matrix].fSiM]>>>
      (bufGPU->GPUtoCPU, xG[bufGPU->matrix].fst,
      xG[bufGPU->matrix].n, bufGPU->bodyRow, bufGPU->offset, GPUaboveCPU);

    cudaMemcpyAsync(bufGPU->CPUtoCPU, bufGPU->GPUtoCPU, bufGPU->bodySize,
      cudaMemcpyDeviceToHost,
      mdGPU.stream[xG[bufGPU->matrix].fSiM]);
    cudaEventRecord(xG[bufGPU->matrix].updateGPUCPUevent, mdGPU.stream[xG[bufGPU->matrix].fSiM]);
  }
#else
  cudaSetDevice(bufGPU->idGPU);
  if(GPUaboveCPU)
  {
    cudaMemcpyAsync(bufGPU->CPUtoCPU, xG[bufGPU->matrix].srcTB,
      xG[bufGPU->matrix].sizeV, cudaMemcpyDeviceToHost,
      mdGPU.stream[xG[bufGPU->matrix].lSiM]);
    cudaEventRecord(xG[bufGPU->matrix].updateGPUCPUevent, mdGPU.stream[xG[bufGPU->matrix].lSiM]);
  }
  else
  {
    cudaMemcpyAsync(bufGPU->CPUtoCPU, xG[bufGPU->matrix].srcBT,
      xG[bufGPU->matrix].sizeV, cudaMemcpyDeviceToHost,
      mdGPU.stream[xG[bufGPU->matrix].fSiM]);
    cudaEventRecord(xG[bufGPU->matrix].updateGPUCPUevent, mdGPU.stream[xG[bufGPU->matrix].fSiM]);
  }
#endif
}

void cpyBufToGPU_(UpdateGPUCPU *bufGPU,
                 MatrixGPU<realX> *xG, bool GPUaboveCPU)
{
#if GCU==0
  cudaSetDevice(bufGPU->idGPU);
  cudaMemcpyAsync(bufGPU->GPUtoGPU, bufGPU->CPUtoGPU,
    bufGPU->fullSize,
    cudaMemcpyHostToDevice,
    xG[bufGPU->matrix].updateGPUCPU);
  cpyBufToGPU<<<bufGPU->nB2, bufGPU->tB, 0,
    xG[bufGPU->matrix].updateGPUCPU>>>
    (xG[bufGPU->matrix].fst, bufGPU->GPUtoGPU,
    xG[bufGPU->matrix].n, bufGPU->fullRow, -3, GPUaboveCPU);
#else
  cudaSetDevice(bufGPU->idGPU);
  if(GPUaboveCPU)
  {
    cudaMemcpyAsync(xG[bufGPU->matrix].dstBT, bufGPU->CPUtoGPU,
      xG[bufGPU->matrix].sizeV,
      cudaMemcpyHostToDevice,
      xG[bufGPU->matrix].updateGPUCPU);
  }
  else
  {
    cudaMemcpyAsync(xG[bufGPU->matrix].dstTB, bufGPU->CPUtoGPU,
      xG[bufGPU->matrix].sizeV,
      cudaMemcpyHostToDevice,
      xG[bufGPU->matrix].updateGPUCPU);
  }
#endif
}

void cpyGPUCPU_(UpdateGPUCPU *bufGPU,
               UpdateGPUCPU *bufCPU,
               MatrixGPU<realX> *xG,
               MatrixCPU<realX> *xC, bool GPUaboveCPU)
{
  cudaSetDevice(bufGPU->idGPU);
  if(GPUaboveCPU)
  {
    cudaMemcpyAsync(xC[bufCPU->matrix].dstTB,
                xG[bufGPU->matrix].srcTB,
                xC[bufCPU->matrix].sizeV,
                cudaMemcpyDeviceToHost,
                mdGPU.stream[xG[bufGPU->matrix].lSiM]);//to jest zle
    cudaMemcpyAsync(xG[bufGPU->matrix].dstBT,
                    xC[bufCPU->matrix].srcBT,
                    xG[bufGPU->matrix].sizeV,
                    cudaMemcpyHostToDevice,
                    xG[bufGPU->matrix].updateGPUCPU);
  }
  else
  {
    cudaMemcpyAsync(xC[bufCPU->matrix].dstBT,
                xG[bufGPU->matrix].srcBT,
                xC[bufCPU->matrix].sizeV,
                cudaMemcpyDeviceToHost,
                mdGPU.stream[xG[bufGPU->matrix].fSiM]);//to jest zle
    cudaMemcpyAsync(xG[bufGPU->matrix].dstTB,
                    xC[bufCPU->matrix].srcTB,
                    xG[bufGPU->matrix].sizeV,
                    cudaMemcpyHostToDevice,
                    xG[bufGPU->matrix].updateGPUCPU);
  }
}
#endif // finish GPUCPU update

#if NDS>1
__global__ void cpyKrnLRtoBuf(realX *dst_, realX *src_, const int n, const int m)
{
  const int i = blockIdx.y * blockDim.y + threadIdx.y;
  const int j = blockIdx.x * blockDim.x + threadIdx.x;
  const int xOff=mG<realX>();
  const int xOffL=xOff*(LDIM+6);

  if(j<3 && i<n)
  for(int k=-3; k<LDIM+3; ++k)
  {
    dst_[i*3*(LDIM+6) + j + (k+3)*3] = src(i, j+m-3, k);
  }
}

__global__ void cpyKrnRLtoMat(realX *dst_, realX *src_, const int n, const int m)
{
  const int i = blockIdx.y * blockDim.y + threadIdx.y;
  const int j = blockIdx.x * blockDim.x + threadIdx.x;
  const int xOff=mG<realX>();
  const int xOffL=xOff*(LDIM+6);

  if(j<3 && i<n)
  for(int k=-3; k<LDIM+3; ++k)
  {
    dst(i, j-3, k) = src_[i*3*(LDIM+6) + j + (k+3)*3];
  }
}

__global__ void cpyKrnRLtoBuf(realX *dst_, realX *src_, const int n, const int m)
{
  const int i = blockIdx.y * blockDim.y + threadIdx.y;
  const int j = blockIdx.x * blockDim.x + threadIdx.x;
  const int xOff=mG<realX>();
  const int xOffL=xOff*(LDIM+6);

  if(j<3 && i<n)
  for(int k=-3; k<LDIM+3; ++k)
  {
    dst_[i*3*(LDIM+6) + j + (k+3)*3] = src(i, j, k);
  }
}

__global__ void cpyKrnLRtoMat(realX *dst_, realX *src_, const int n, const int m)
{
  const int i = blockIdx.y * blockDim.y + threadIdx.y;
  const int j = blockIdx.x * blockDim.x + threadIdx.x;
  const int xOff=mG<realX>();
  const int xOffL=xOff*(LDIM+6);

  if(j<3 && i<n)
  for(int k=-3; k<LDIM+3; ++k)
  {
    dst(i, j+m, k) = src_[i*3*(LDIM+6) + j + (k+3)*3];
  }
}

void cpyKrnLRtoMat(MatrixGPU<realX> &mG)
{
//  cudaSetDevice(mG.idGPU);
#if GDR==0
 cudaMemcpyAsync(mG.updateNode->dstBufLR.buf, mG.updateNode->dstBufLR.bufH,
                 mG.updateNode->sizeLRRL*sizeof(realX), cudaMemcpyHostToDevice,
                 mG.updateNode->dstBufLR.str);
#endif
 cpyKrnLRtoMat<<<mdGPU.nBV, mdGPU.tPB, 0, mG.updateNode->dstBufLR.str>>>
     (mG.fst, mG.updateNode->dstBufLR.buf, mG.n, mdGPU.mT*mdGPU.xS);
}

void cpyKrnRLtoMat(MatrixGPU<realX> &mG)
{
//  cudaSetDevice(mG.idGPU);
#if GDR==0
  cudaMemcpyAsync(mG.updateNode->dstBufRL.buf, mG.updateNode->dstBufRL.bufH,
                  mG.updateNode->sizeLRRL*sizeof(realX), cudaMemcpyHostToDevice,
                  mG.updateNode->dstBufRL.str);
#endif
  cpyKrnRLtoMat<<<mdGPU.nBV, mdGPU.tPB, 0, mG.updateNode->dstBufRL.str>>>
      (mG.fst, mG.updateNode->dstBufRL.buf, mG.n, mdGPU.mT*mdGPU.xS);
}

void cpyKrnTBtoMat(MatrixGPU<realX> &mG)
{
#if GDR==0
  cudaSetDevice(mG.idGPU);
  cudaMemcpyAsync(mG.dstBT, mG.updateNode->dstBufTB.bufH,
                  mG.updateNode->sizeTBBT*sizeof(realX), cudaMemcpyHostToDevice,
                  mG.updateNode->dstBufTB.str);
#endif
}

void cpyKrnBTtoMat(MatrixGPU<realX> &mG)
{
#if GDR==0
  cudaSetDevice(mG.idGPU);
  cudaMemcpyAsync(mG.dstTB, mG.updateNode->dstBufBT.bufH,
                  mG.updateNode->sizeTBBT*sizeof(realX), cudaMemcpyHostToDevice,
                  mG.updateNode->dstBufBT.str);
#endif
}
#endif //NDS>1

void initGPU()
{
  int mF_ = mGPU<float>();
  int mD_ = mGPU<double>();

  for(int i=0; i<GPN; ++i)
  {
    cudaSetDevice(i);
    cudaMemcpyToSymbol(mF, &mF_, sizeof(int), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(mD, &mD_, sizeof(int), 0, cudaMemcpyHostToDevice);
  }
}

//*** Execute kernels on GPUs **************************************************
int RunGPU::run(const int ts)
{
  for(int i=0; i<mdGPU.streamCount; ++i)
  {
    cudaSetDevice(xG[i].idGPU);
    krn0<<<mdGPU.krn0B, mdGPU.krn0T, 0, mdGPU.stream[i]>>>(
                  xG[ mdGPU.sToM[i] ].fst,
                  v1G[ mdGPU.sToM[i] ].fst,
                  v2G[ mdGPU.sToM[i] ].fst,
                  v3G[ mdGPU.sToM[i] ].fst,
                  hG[ mdGPU.sToM[i] ].fst,
                  xPG[ mdGPU.sToM[i] ].fst,
                  xG[ mdGPU.sToM[i] ].n, xG[ mdGPU.sToM[i] ].m,
                  i, procId, mdGPU.iOffset[i], mdGPU.jOffset[i]);
  }

#if SDP==1
/*  for(int i=0; i<mdGPU.streamCount; ++i)
  {
    cudaEventRecord(event[i], mdGPU.stream[i]);
  }*/

  //if( cudaEventQuery(event[??])==cudaSuccess )
  for(int i=0; i<GPN; ++i)
  {
    cudaSetDevice(i);
    cudaDeviceSynchronize();
  }
    //synchronize kernels that share a single matrix
#endif

  for(int i=0; i<mdGPU.streamCount; ++i)
  {
    cudaSetDevice(xG[i].idGPU);

    krn1<<<mdGPU.krn1B, mdGPU.krn1T, 0, mdGPU.stream[i]>>>(
                  v1G[ mdGPU.sToM[i] ].fst,
                  v2G[ mdGPU.sToM[i] ].fst,
                  v3G[ mdGPU.sToM[i] ].fst,
                  hG[ mdGPU.sToM[i] ].fst,
                  xPG[ mdGPU.sToM[i] ].fst,
                  v1PG[ mdGPU.sToM[i] ].fst,
                  v2PG[ mdGPU.sToM[i] ].fst,
                  v3PG[ mdGPU.sToM[i] ].fst,
                  xG[ mdGPU.sToM[i] ].n, xG[ mdGPU.sToM[i] ].m,
                  i, mdGPU.iOffset[i], mdGPU.jOffset[i]);
  }

  #if SDP==1
    //synchronize kernels that share a single matrix
  #endif

  for(int i=0; i<mdGPU.streamCount; ++i)
  {
    cudaSetDevice(xG[i].idGPU);
    krn2<<<mdGPU.krn2B, mdGPU.krn2T, 0, mdGPU.stream[i]>>>(
                  xG[ mdGPU.sToM[i] ].fst,
                  hG[ mdGPU.sToM[i] ].fst,
                  xPG[ mdGPU.sToM[i] ].fst,
                  v1PG[ mdGPU.sToM[i] ].fst,
                  v2PG[ mdGPU.sToM[i] ].fst,
                  v3PG[ mdGPU.sToM[i] ].fst,
                  cpG[ mdGPU.sToM[i] ].fst,
                  cnG[ mdGPU.sToM[i] ].fst,
                  xG[ mdGPU.sToM[i] ].n, xG[ mdGPU.sToM[i] ].m,
                  i, mdGPU.iOffset[i], mdGPU.jOffset[i]);
  }

  #if SDP==1
    //synchronize kernels that share a single matrix
  #endif

  for(int i=0; i<mdGPU.streamCount; ++i)
  {
    cudaSetDevice(xG[i].idGPU);
    krn3<<<mdGPU.krn3B, mdGPU.krn3T, 0, mdGPU.stream[i]>>>(
                  xG[ mdGPU.sToM[i] ].fst,
                  hG[ mdGPU.sToM[i] ].fst,
                  xPG[ mdGPU.sToM[i] ].fst,
                  v1PG[ mdGPU.sToM[i] ].fst,
                  v2PG[ mdGPU.sToM[i] ].fst,
                  v3PG[ mdGPU.sToM[i] ].fst,
                  cpG[ mdGPU.sToM[i] ].fst,
                  cnG[ mdGPU.sToM[i] ].fst,
                  xG[ mdGPU.sToM[i] ].n, xG[ mdGPU.sToM[i] ].m,
                  i, mdGPU.iOffset[i], mdGPU.jOffset[i]);
    cudaEventRecord(mdGPU.event[i], mdGPU.stream[i]);
  }

  #if SDP==1 //??????
    //synchronize kernels that share a single matrix
  #endif
  return 1;
}

//*** Update halo between left and right matrices within a single node *********
int UpdateGPUhor::update()
{
  cudaSetDevice(idGPUfrom);
#if HSPG==1
  if(LR) cpyKrnLR<<<mdGPU.nBV, mdGPU.tPB, 0, mdGPU.stream[idSfrom]>>>
           (fstTo, fstFrom, n, m);
  else   cpyKrnRL<<<mdGPU.nBV, mdGPU.tPB, 0, mdGPU.stream[idSfrom]>>>
           (fstTo, fstFrom, n, m);
  ++ind;
  return 0;
#else
  cudaSetDevice(idGPUto);
  if( cudaEventQuery(mdGPU.event[idSto])==cudaSuccess )
  {
    cudaSetDevice(idGPUfrom);
    cpyKrnLRRL<<<mdGPU.nBV, mdGPU.tPB, 0, mdGPU.stream[idSfrom]>>>
      (fstTo, fstFrom, n, m);
    ++ind;
  }
  return 0;
#endif
}

int UpdateGPUhor::query()
{
  int finished=0;
  cudaSetDevice(idGPUfrom);
  if( cudaStreamQuery(mdGPU.stream[idSfrom])==cudaSuccess )
  {
    std::cout << tmp << "zakonczylem kopiowac GPU LRRL\n";
    ++ind;
    finished=1;
  }
  return finished;
}

int UpdateGPUhor::done()
{
  return 1;
}

int UpdateGPUhor::run(const int ts __attribute__((unused)))
{
  return (this->*stage[ind])();
}

//*******************************************************************************
bool UpdateGPUver::queryRows()
{
  const int offY=(TB)? 1 : -1;
  const int iteration = -offY*GPN*SPG/TGP;
  const int end = (offY>0)? -idSfrom+GPN*SPG/TGP*(mdGPU.xS-1)+1 :
    idSfrom+GPN*SPG/TGP*(mdGPU.xS-1)+1;

  bool err=true;
  for(int I=idSfrom+iteration; -offY*I<end; I+=iteration)
  {
  #if HSPG==0
    cudaSetDevice(idGPUfrom);
  #endif
    err &= (cudaEventQuery( mdGPU.event[ I ] )==cudaSuccess);
  #if HSPG==0
    cudaSetDevice(idGPUto);
    err &= (cudaEventQuery( mdGPU.event[ I+1 ] )==cudaSuccess);
  #endif
  }
#if HSPG==0
  cudaSetDevice(idGPUto);
  err &= (cudaEventQuery( mdGPU.event[ idSfrom+1 ] )==cudaSuccess);
#endif
  return err;
}

int UpdateGPUver::update()
{
  cudaSetDevice(idGPUfrom);

#if HSPG==1
  if(TB)
  {
    if( queryRows() )
    {
    #if CPYG>0
      cudaMemcpyAsync(dstTo, srcFrom, sizeV,
        cudaMemcpyDeviceToDevice, mdGPU.stream[idSfrom]);
    #else
      cpyKrnTB<<<mdGPU.nBH, mdGPU.tPB, 0, mdGPU.stream[idSfrom]>>>
        (fstTo, fstFrom, n, m);
    #endif
      ++ind;
    }
  }
  else
  {
    if( queryRows() )
    {
    #if CPYG>0
      cudaMemcpyAsync(dstTo, srcFrom, sizeV,
        cudaMemcpyDeviceToDevice, mdGPU.stream[idSfrom]);
    #else
      cpyKrnBT<<<mdGPU.nBH, mdGPU.tPB, 0, mdGPU.stream[idSfrom]>>>
        (fstTo, fstFrom, n, m);
    #endif
      ++ind;
    }
  }
#else
#if CPYG>0
  if( queryRows() )
  {
    cudaMemcpyAsync(dstFrom, srcTo, sizeV,
      cudaMemcpyDeviceToDevice, mdGPU.stream[idSto]);
    cudaSetDevice(idSfrom);
    cudaMemcpyAsync(dstTo, srcFrom, sizeV,
      cudaMemcpyDeviceToDevice, mdGPU.stream[idSfrom]);
    ++ind;
  }
#else
  if( queryRows() )
  {
    cpyKrnTBBT<<<mdGPU.nBH, mdGPU.tPB, 0, mdGPU.stream[idSfrom]>>>
      (fstTo, fstFrom, n, m);
    ++ind;
  }
#endif
#endif
  return 0;
}

int UpdateGPUver::query()
{
  int finished=0;
  cudaSetDevice(idGPUfrom);
  if( cudaStreamQuery(mdGPU.stream[idSfrom])==cudaSuccess )
  {
    ++ind;
    finished=1;
  }
  return finished;
}

int UpdateGPUver::done()
{
  return 1;
}

int UpdateGPUver::run(const int ts __attribute__((unused)))
{
  return (this->*stage[ind])();
}
//******************************************************************************
#if NDS>1
int UpdateGPUhorMPI::updateBuf()
{
  cudaSetDevice(mG->idGPU);
  bool err=true;

  if(LR)
  {
    for(int i=mG->lSiM; i>mG->lSiM-mdGPU.yS; --i)
    {
      err &= (cudaEventQuery(mdGPU.event[i])==cudaSuccess);
    }

    if(err)
    {
      cpyKrnLRtoBuf<<<mdGPU.nBV, mdGPU.tPB, 0, mG->updateNode->srcBufLR.str>>>
          (mG->updateNode->srcBufLR.buf, mG->fst, mG->n, mdGPU.mT*mdGPU.xS);
    #if GDR==0
      cudaMemcpyAsync(mG->updateNode->srcBufLR.bufH, mG->updateNode->srcBufLR.buf,
                      mG->updateNode->sizeLRRL*sizeof(realX), cudaMemcpyDeviceToHost,
                      mG->updateNode->srcBufLR.str);
    #endif
      ++ind;
    }
  }
  else
  {
    for(int i=mG->fSiM; i<mG->fSiM+mdGPU.yS; ++i)
    {
      err &= (cudaEventQuery(mdGPU.event[i])==cudaSuccess);
    }

    if(err)
    {
      cpyKrnRLtoBuf<<<mdGPU.nBV, mdGPU.tPB, 0, mG->updateNode->srcBufRL.str>>>
          (mG->updateNode->srcBufRL.buf, mG->fst, mG->n, mdGPU.mT*mdGPU.xS);
    #if GDR==0
      cudaMemcpyAsync(mG->updateNode->srcBufRL.bufH, mG->updateNode->srcBufRL.buf,
                      mG->updateNode->sizeLRRL*sizeof(realX), cudaMemcpyDeviceToHost,
                      mG->updateNode->srcBufRL.str);
    #endif
      ++ind;
    }
  }
  return 0;
}

int UpdateGPUhorMPI::done()
{
  return 1;
}

int UpdateGPUhorMPI::run(const int ts __attribute__((unused)))
{
  return (this->*stage[ind])();
}

#if GDR==0
int UpdateGPUverMPI::updateBuf()
{
  cudaSetDevice(mG->idGPU);
  bool err=true;

  if(TB)
  {
    const int iteration = -GPN*SPG/TGP;
    const int end = -mG->lSiM+GPN*SPG/TGP*(mdGPU.xS-1)+1;

    for(int I=mG->lSiM+iteration; -I<end; I+=iteration)
    {
      err &= (cudaEventQuery( mdGPU.event[ I ] )==cudaSuccess);
    }
    err &= (cudaEventQuery( mdGPU.event[ mG->lSiM ] )==cudaSuccess);

    if(err)
    {
      cudaMemcpyAsync(mG->updateNode->srcBufTB.bufH, mG->srcTB,
        mG->updateNode->sizeTBBT*sizeof(realX), cudaMemcpyDeviceToHost,
        mG->updateNode->srcBufTB.str);
      ++ind;
    }
  }
  else
  {
    const int iteration = GPN*SPG/TGP;
    const int end = mG->fSiM+GPN*SPG/TGP*(mdGPU.xS-1)+1;

    for(int I=mG->fSiM+iteration; I<end; I+=iteration)
    {
      err &= (cudaEventQuery( mdGPU.event[ I ] )==cudaSuccess);
    }
    err &= (cudaEventQuery( mdGPU.event[ mG->fSiM ] )==cudaSuccess);

    if(err)
    {
      cudaMemcpyAsync(mG->updateNode->srcBufBT.bufH, mG->srcBT,
        mG->updateNode->sizeTBBT*sizeof(realX), cudaMemcpyDeviceToHost,
        mG->updateNode->srcBufBT.str);
      ++ind;
    }
  }
  return 0;
}
#endif //GDR==0

int UpdateGPUverMPI::done()
{
  return 1;
}

int UpdateGPUverMPI::run(const int ts __attribute__((unused)))
{
  return (this->*stage[ind])();
}
#endif //NDS>1
//******************************************************************************

void initKernelGPU(MatrixGPU<realX> *xG,
            MatrixGPU<realV1> *v1G,
            MatrixGPU<realV2> *v2G,
            MatrixGPU<realV3> *v3G,
            MatrixGPU<realH> *hG,
            MatrixGPU<realX> *xPG,
            MatrixGPU<realV1P> *v1PG,
            MatrixGPU<realV2P> *v2PG,
            MatrixGPU<realV3P> *v3PG,
            MatrixGPU<realCP> *cpG,
            MatrixGPU<realCN> *cnG,
            int procId,
            const int offYGPU, const int offXGPU)
{
  for(int i=0; i<mdGPU.streamCount; ++i)
  {
    const int offY=offYGPU-mdGPU.iOffset[i]+(i%(mdGPU.yTPG*mdGPU.yS))*mdGPU.nT;
    const int offX=offXGPU-mdGPU.jOffset[i]+(i/(mdGPU.yTPG*mdGPU.yS))*mdGPU.mT;
    cudaSetDevice(xG[i].idGPU);
    initKrn<<<mdGPU.krn0B, mdGPU.krn0T>>>(
                  xG[ mdGPU.sToM[i] ].fst,
                  v1G[ mdGPU.sToM[i] ].fst,
                  v2G[ mdGPU.sToM[i] ].fst,
                  v3G[ mdGPU.sToM[i] ].fst,
                  hG[ mdGPU.sToM[i] ].fst,
                  xPG[ mdGPU.sToM[i] ].fst,
                  xG[ mdGPU.sToM[i] ].n, xG[ mdGPU.sToM[i] ].m,
                  i, procId, mdGPU.iOffset[i], mdGPU.jOffset[i], offY, offX);
  }
}

realX valGPU(int i, int j, int k, int offY, int offX)
{
  return i-3+offY + (j-3+offX)*NDIM + (k-3)*NDIM*MDIM;
}

void saveResGPU(MatrixGPU<realX> *xG, const int nCPU,
  const int procId, const int ts, const double time, const double energy)
{
  for(int i=0; i<mdGPU.matrixCount; ++i)
  {
    const int metaDataEl = 12*sizeof(int)/sizeof(realX);
    int metaData[12];
    metaData[0] = mdGPU.n;
    metaData[1] = mGPU<realX>();
    metaData[2] = mdGPU.l;
    metaData[3] = xG[i].n;
    metaData[4] = xG[i].m;
    metaData[5] = mdGPU.mT*mdGPU.xS;
  #if GCP<100
    metaData[6] = mdCPU.nT*mdCPU.yS;
  #else
    metaData[6] = 0;
  #endif
    metaData[7] = nCPU;
    
    memcpy(&metaData[8], &time, 8);
    memcpy(&metaData[10], &energy, 8);
/*
    std::cout << "mdGPU.n="<<mdGPU.n << " mGPU<realX>()="<<mGPU<realX>() << " mdGPU.l="<<mdGPU.l << " xG[i].n="<<xG[i].n << " xG[i].m="<<xG[i].m << " mdGPU.mT*mdGPU.xS="<<mdGPU.mT*mdGPU.xS <<'\n';
        */
    realX *tmp;// = new realX[ xG[i].n*mlGPU<realX>() + metaDataEl ];
    cudaHostAlloc(&tmp, (xG[i].n*mlGPU<realX>() + metaDataEl)*sizeof(realX), cudaHostAllocPortable);

    memcpy(tmp, metaData, 12*sizeof(int));

    cudaSetDevice(xG[i].idGPU);
    cudaDeviceSynchronize();
    cudaMemcpy(tmp+metaDataEl, xG[i].fst-3, xG[i].n*mlGPU<realX>()*sizeof(realX), cudaMemcpyDeviceToHost);

    std::string filename=toStr("./sim/")+TSK+toStr("-GPU-")+toStr(ts)+"-"+toStr(procId)+"-"+toStr(i)+".txt";
    std::ofstream fout;
    fout.open(filename.c_str(), std::ofstream::binary);
    fout.write((char*)tmp, 12*sizeof(int)+xG[i].n*mlGPU<realX>()*sizeof(realX));
    fout.close();
/*
    std::cout << filename << ' ' << metaData[0] << 'x' << metaData[1] << 'x' << metaData[2] << " -> " << metaData[3] << 'x' << metaData[4] << 'x' << metaData[5] << '\n';
    for(int K=0; K<metaData[5]; ++K)
    {
      for(int I=0; I<metaData[3]; ++I)
      {
        for(int J=0; J<metaData[4]; ++J)
        {
          const int srcInd = metaDataEl + I*metaData[1]*metaData[2] + J+3 + K*metaData[1];
          std::cout << tmp[srcInd] << ' ';
        }
        std::cout << '\n';
      }
      std::cout << '\n';
    }
*/
    cudaFreeHost(tmp);//delete [] tmp;
  }
}
#endif //GCP>0
