#include "memCPU.h"

#if GCP<100
int getCPUThreadId(const int i, int &iOffset, int &jOffset,
                   const int nT, const int mT, int &yS, int &xS)
{
#if GTC>=TTH
  const int yTPG=THS/GTC;
  const int xTPG=1;
#else
  const int yTPG=THS/TTH;
  const int xTPG=TTH/GTC;
#endif
  yS=THS/TTH/yTPG;
  xS=TTH/xTPG;
  const int I=i%yS;
  const int J=(i/(THS/TTH))%xS;

  iOffset = I*nT;
  jOffset = J*mT;

  return i/(THS/TTH)/xS*yTPG+(i%(THS/TTH))/yS;
}

MetaDataCPU::MetaDataCPU()
{
  yN=NDS/TDS; //x - node
  xN=TDS;     //y - node

  const int agmElF = divT(AGMC, sizeof(float));
  const int agmElD = divT(AGMC, sizeof(double));

//x, y - topology
#if GTC>=TTH
  yTPG=THS/GTC;
  xTPG=1;
#else
  yTPG=THS/TTH;
  xTPG=TTH/GTC;
#endif

#if GCP>0
  const int remnantN = divT(NDIM, yN)-mdGPU.yTPG*mdGPU.nT*mdGPU.yS;
  const int remnantM = mdGPU.xTPG*mdGPU.mT*mdGPU.xS;//divT(MDIM, xN);
#else
  const int remnantN = divT(NDIM, yN);
  const int remnantM = divT(MDIM, xN);
#endif

  const int maxY = std::max(std::max(std::max(CPUAY, CPUBY), CPUCY), CPUDY);
  nT=rndT(divT(divT(remnantN, THS/TTH), 1), maxY)*THS/TTH/yTPG;
  n=6+nT;
    cpu0B.y=divT(nT, CPUAY);
  nT=rndT(divT(divT(remnantN, THS/TTH), 1), maxY);
  const int maxX = std::max(std::max(std::max(CPUAX, CPUBX), CPUCX), CPUDX);
  mT=rndT(divT(divT(remnantM, TTH), 1), maxX)*TTH/xTPG;
  mF=rndT(mT+6, agmElF);
  mD=rndT(mT+6, agmElD);
  mT=rndT(divT(divT(remnantM, TTH), 1), maxX);

  l=LDIM+6;

  mFl=mF*l;
  mDl=mD*l;

  sizeF = (n*mFl+agmElF)*sizeof(float);
  sizeD = (n*mDl+agmElD)*sizeof(double);

  matrixCount=THS/GTC;

  tToM.resize(THS);
  iOffset.resize(THS);
  jOffset.resize(THS);
  for(int i=0; i<THS; ++i)
  {
  #if GTC>1
    tToM[i]=getCPUThreadId(i, iOffset[i], jOffset[i], nT, mT, yS, xS);
  #else
    tToM[i]=i;
    iOffset[i]=0;
    jOffset[i]=0;
    yS=1;
    xS=1;
  #endif
  }

  nBV.x=divT(3, CPUDX);
  nBV.y=rndT(divT(divT(remnantN, THS/TTH), 1), maxY)*THS/TTH/yTPG;
  nBH.x=rndT(divT(divT(remnantM+6, TTH), 1), maxX)*TTH/xTPG;
  nBH.y=divT(3, CPUDY);

  cpu0B.x=divT(mT, CPUAX);
  cpu1B.x=divT(mT, CPUBX);
  cpu2B.x=divT(mT, CPUCX);
  cpu3B.x=divT(mT, CPUDX);

  cpu0B.y=divT(nT, CPUAY);
  cpu1B.y=divT(nT, CPUBY);
  cpu2B.y=divT(nT, CPUCY);
  cpu3B.y=divT(nT, CPUDY);
}

MetaDataCPU::~MetaDataCPU()
{
}

MetaDataCPU mdCPU;

#if NDS>1
void UpdateNodeCPU::create(int sLRRL, int sTBBT,
                           bool l, bool r, bool t, bool b)
{
  sizeLRRL=sLRRL;
  sizeTBBT=sTBBT;

  if(r)
  {
//    srcBufLR.create(sizeLRRL*sizeof(realX));
//    dstBufLR.create(sizeLRRL*sizeof(realX));
  }
  if(l)
  {
//    srcBufRL.create(sizeLRRL*sizeof(realX));
//    dstBufRL.create(sizeLRRL*sizeof(realX));
  }
  if(b)
  {
    srcBufTB.create(sizeTBBT*sizeof(realX));
    dstBufTB.create(sizeTBBT*sizeof(realX));
  }
  if(t)
  {
    srcBufBT.create(sizeTBBT*sizeof(realX));
    dstBufBT.create(sizeTBBT*sizeof(realX));
  }
}
#endif

#endif //GCP<100
