#include "memGPU.h"

int NDS_V=NDS;

int divT(const int l, const int r)
{
  return (l+r-1)/r;
}

int rndT(const int l, const int r)
{
  return divT(l, r)*r;
}

#if GCP>0
int getGPUStreamId(const int i, int &iOffset, int &jOffset,
                   const int nT, const int mT, int &yS, int &xS)
{
  const int yTPG=GPN*SPG/TGP;
  const int xTPG=TGP;

  int I=i%yTPG;
  int J=i/yTPG;
  const int y=divT(yTPG, GPN);
  const int x=xTPG/( GPN/( yTPG/y ) );

  iOffset = (I%y) * nT;
  jOffset = (J%x) * mT;

  I/=y;
  J/=x;

  yS=y;
  xS=x;

  return I+J*yTPG/y;
}

MetaDataGPU::MetaDataGPU()
{
  yN=NDS/TDS; //x - node
  xN=TDS;     //y - node

  const int agmElF = divT(AGMG, sizeof(float));
  const int agmElD = divT(AGMG, sizeof(double));

//x, y - topology
#if SDP==1
  #if SPG>=TGP
    yTPG=GPN;
    xTPG=1;
  #else
    yTPG=GPN*SPG/TGP;
    xTPG=TGP/SPG;
  #endif
#else
  yTPG=GPN*SPG/TGP;
  xTPG=TGP;
#endif

  const int remnantN = divT(NDIM, yN);
  const int remnantM = divT(MDIM, xN);

  const int maxY = std::max(std::max(std::max(KRNAY, KRNBY), KRNCY), KRNDY);
  nT=rndT(divT(divT(divT(remnantN*GCP, 100), GPN*SPG/TGP), 1), maxY)*
          GPN*SPG/TGP/yTPG;
  n=6+nT;
  nT=rndT(divT(divT(divT(remnantN*GCP, 100), GPN*SPG/TGP), 1), maxY);

  const int maxX = std::max(std::max(std::max(KRNAX, KRNBX), KRNCX), KRNDX);
  mT=rndT(divT(divT(remnantM, TGP), 1), maxX)*TGP/xTPG;
  mF=rndT(mT+6, agmElF);
  mD=rndT(mT+6, agmElD);
  mT=rndT(divT(divT(remnantM, TGP), 1), maxX);

  l=LDIM+6;

  mFl=mF*l;
  mDl=mD*l;

  sizeF = (n*mFl+agmElF)*sizeof(float);
  sizeD = (n*mDl+agmElD)*sizeof(double);

#if SDP==1
  matrixCount=GPN;
#else
  matrixCount=GPN*SPG;
#endif
  streamCount=GPN*SPG;

  stream = new cudaStream_t[streamCount];
  event = new cudaEvent_t[streamCount];
  sToM.resize(streamCount);
  iOffset.resize(streamCount);
  jOffset.resize(streamCount);
  for(int i=0; i<streamCount; ++i)
  {
    const int id = getGPUStreamId(i, iOffset[i], jOffset[i], nT, mT, yS, xS);
    cudaSetDevice(id);
    cudaStreamCreate(&stream[i]);
    cudaEventCreate(&event[i]);

  #if SDP==1
    sToM[i]=id;
  #else
    sToM[i]=i;
    iOffset[i]=0;
    jOffset[i]=0;
    yS=1;
    xS=1;
  #endif
  }

  tPB.x=KRNDX;
  tPB.y=KRNDY;

  nBV.x=divT(3, tPB.x);
  nBV.y=rndT(divT(divT(divT(remnantN*GCP, 100), GPN*SPG/TGP), 1), maxY)*
        GPN*SPG/TGP/yTPG;
  nBH.x=rndT(divT(divT(remnantM+6, TGP), 1), maxX)*TGP/xTPG;
  nBH.y=divT(3, tPB.y);

  krn0T.x=KRNAX;
  krn1T.x=KRNBX;
  krn2T.x=KRNCX;
  krn3T.x=KRNDX;

  krn0T.y=KRNAY;
  krn1T.y=KRNBY;
  krn2T.y=KRNCY;
  krn3T.y=KRNDY;

  krn0B.x=divT(mT, KRNAX);
  krn1B.x=divT(mT, KRNBX);
  krn2B.x=divT(mT, KRNCX);
  krn3B.x=divT(mT, KRNDX);

  krn0B.y=divT(nT, KRNAY);
  krn1B.y=divT(nT, KRNBY);
  krn2B.y=divT(nT, KRNCY);
  krn3B.y=divT(nT, KRNDY);
}

MetaDataGPU::~MetaDataGPU()
{
  for(int i=0; i<streamCount; ++i)
  {
    const int id = getGPUStreamId(i, iOffset[i], jOffset[i], nT, mT, yS, xS);
    cudaSetDevice(id);
    cudaStreamDestroy(stream[i]);
    cudaEventDestroy(event[i]);
  }
  delete [] stream;
}

MetaDataGPU mdGPU;

#if NDS>1
void UpdateNodeGPU::create(int id, int sLRRL, int sTBBT,
                           bool l, bool r, bool t, bool b, int procId)
{
  idGPU=id;
  sizeLRRL=sLRRL;
  sizeTBBT=sTBBT;
  cudaSetDevice(idGPU);

  if(r)
  {
    srcBufLR.create(sizeLRRL*sizeof(realX));
    dstBufLR.create(sizeLRRL*sizeof(realX));
  }
  if(l)
  {
    srcBufRL.create(sizeLRRL*sizeof(realX));
    dstBufRL.create(sizeLRRL*sizeof(realX));
  }
  if(b)
  {
    srcBufTB.create(sizeTBBT*sizeof(realX));
    dstBufTB.create(sizeTBBT*sizeof(realX));
  }
  if(t)
  {
    srcBufBT.create(sizeTBBT*sizeof(realX));
    dstBufBT.create(sizeTBBT*sizeof(realX));
  }
}
#endif

#endif //GCP>0
