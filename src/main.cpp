#include "task.h"

#if NDS>1
int main(int argc, char **argv)
#else
int main()
#endif
{
#if NDS>1
    int provided;
    //MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    //  MPI_Init(&argc, &argv);
#endif
    run();
#if NDS>1
    MPI_Finalize();
#endif
    return 0;
}
