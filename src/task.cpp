#include "task.h"
#include "dmr.h"

class RunTimeStep;
void compute(int t);

MatrixCPU<realX> xCPU[1]; // = new MatrixCPU<realX>[mdCPU.matrixCount];
MatrixCPU<realV1> v1CPU[1]; // = new MatrixCPU<realV1>[mdCPU.matrixCount];
MatrixCPU<realV2> v2CPU[1]; // = new MatrixCPU<realV2>[mdCPU.matrixCount];
MatrixCPU<realV3> v3CPU[1]; // = new MatrixCPU<realV3>[mdCPU.matrixCount];
MatrixCPU<realH> hCPU[1]; // = new MatrixCPU<realH>[mdCPU.matrixCount];
MatrixCPU<realX> xPCPU[1]; // = new MatrixCPU<realX>[mdCPU.matrixCount];
MatrixCPU<realV1P> v1PCPU[1]; // = new MatrixCPU<realV1P>[mdCPU.matrixCount];
MatrixCPU<realV2P> v2PCPU[1]; // = new MatrixCPU<realV2P>[mdCPU.matrixCount];
MatrixCPU<realV3P> v3PCPU[1]; // = new MatrixCPU<realV3P>[mdCPU.matrixCount];
MatrixCPU<realCP> cpCPU[1]; // = new MatrixCPU<realCP>[mdCPU.matrixCount];
MatrixCPU<realCN> cnCPU[1]; // = new MatrixCPU<realCN>[mdCPU.matrixCount];

#if GCP>0

class TimeGPU {
private:
    cudaEvent_t ts;
    cudaEvent_t te;
    float t;

public:

    TimeGPU()
    : t(0.0f) {
        cudaSetDevice(0);
        cudaEventCreate(&ts);
        cudaEventCreate(&te);
    }

    void start() {
        cudaSetDevice(0);
        cudaEventRecord(ts, 0);
    }

    float read() {
        cudaSetDevice(0);
        cudaEventRecord(te, 0);
        cudaEventSynchronize(te);
        cudaEventElapsedTime(&t, ts, te);
        t *= 0.001;
        return t;
    }

    const float& time() const {
        return t;
    }

    float& time() {
        return t;
    }

    ~TimeGPU() {
        cudaSetDevice(0);
        cudaEventDestroy(ts);
        cudaEventDestroy(te);
    }
};

inline std::ostream& operator<<(std::ostream& os, const TimeGPU& t) {
    return os << t.time();
}
#endif

void displayInfo(std::string& display) {
    display = "HybridMPDATA application\n";
    display += "------------------------------------------------------------------\n";
    display += "Task parameters:\n";
    display += "  Mode:       " + toStr(TSK) + '\n';
    display += "  Size:       " + toStr(NDIM) + " x "
            + toStr(MDIM) + " x "
            + toStr(LDIM) + '\n';
    display += "  Time steps: " + toStr(TS) + '\n';
    display += "  Collect results after every " + toStr(TSRES) + " time step(s)\n";
#if OUTPUT==1
    display += "  Print output to file: yes\n";
#else
    display += "  Print output to file: no\n";
#endif
    display += "Precision: ";
#if PRC==0
    display += "mixed\n";
    display += toStr("  X/XP precision: ")+((X == 1) ? "float\n" : "double\n");
    display += toStr("  V1 precision:   ")+((V1 == 1) ? "float\n" : "double\n");
    display += toStr("  V2 precision:   ")+((V2 == 1) ? "float\n" : "double\n");
    display += toStr("  V3 precision:   ")+((V3 == 1) ? "float\n" : "double\n");
#if H>0
    display += toStr("  H precision:    ")+((H == 1) ? "float\n" : "double\n");
#endif
    display += toStr("  V1P precision:  ")+((V1P == 1) ? "float\n" : "double\n");
    display += toStr("  V2P precision:  ")+((V2P == 1) ? "float\n" : "double\n");
    display += toStr("  V3P precision:  ")+((V3P == 1) ? "float\n" : "double\n");
    display += toStr("  CP precision:   ")+((CP == 1) ? "float\n" : "double\n");
    display += toStr("  CN precision:   ")+((CN == 1) ? "float\n" : "double\n");
#elif PRC==1
    display += "float\n";
#else
    display += "double\n";
#endif
    display += "Domain configuration:\n";
    display += "  GPU configuration:\n";
    display += "    GPU part:        " + toStr(GCP) + " [%]\n";
    display += "    GPUs per node:   " + toStr(GPN) + '\n';
    display += "    Streams per GPU: " + toStr(SPG) + '\n';
    display += "    Topology:        " + toStr(GPN * SPG / TGP) + "x" + toStr(TGP) + '\n';
#if SDP>0
    display += "    Policy:          shared memory across streams\n";
#else
    display += "    Policy:          distributed memory across streams\n";
#endif
    display += "    Data alignment:  " + toStr(AGMG) + " B\n";
#if CPYG>0
    display += "    Memory copying model: use cudaMemcpy for top/bottom halos\n";
#else
    display += "    Memory copying model: use kernels for top/bottom halos\n";
#endif
#if HSPG>0
    display += "    Strategy of halo copying: 4 separate kernels for l/r/t/b halos\n";
#else
    display += "    Strategy of halo copying: common kernel for l/r and t/b halos\n";
#endif
#if GDR>0
    display += "    Intra GPU communication: use GPU direct\n";
#else
    display += "    Intra GPU communication: do not use GPU direct\n";
#endif
    display += "  CPU configuration:\n";
    display += "    CPU part:                 " + toStr(100 - GCP) + " [%]\n";
    display += "    Number of threads:        " + toStr(THS) + '\n';
    display += "    Threads per shared group: " + toStr(GTC) + '\n';
    display += "    Topology:                 " + toStr(THS / TTH) + "x" + toStr(TTH) + '\n';
#if CPYC>0
    display += "    Memory copying model:     use memcpy for top/bottom halos\n";
#else
    display += "    Memory copying model:     use kernels for top/bottom halos\n";
#endif
    display += "    Data alignment:           " + toStr(AGMC) + " B\n";
#if HSPC>0
    display += "    Strategy of halo copying: 4 separate kernels for l/r/t/b halos\n";
#else
    display += "    Strategy of halo copying: common kernel for l/r and t/b halos\n";
#endif
#if GCU==0
    display += "  Memory GPU-CPU copying model: use kernels for top/bottom halos\n";
#else
    display += "  Memory GPU-CPU copying model: use cudaMemcpy for top/bottom halos\n";
#endif
    display += "  Nodes configuration:\n";
    display += "    Number of nodes: " + toStr(NDS) + '\n';
    display += "    Topology:        " + toStr(NDS / TDS) + "x" + toStr(TDS) + '\n';
#if B_V>0
    display += "    Buffering data in vertical direction: yes\n";
#else
    display += "    Buffering data in vertical direction: no\n";
#endif
#if B_H>0
    display += "    Buffering data in horizontal direction: yes\n";
#else
    display += "    Buffering data in horizontal direction: no\n";
#endif
    display += "------------------------------------------------------------------\n";
}

void printToFile(const std::string& display, const int procId = -1) {
    if (procId != -1) {
        time_t t = time(0);
        const struct tm *now = localtime(&t);
        const std::string date = "./res/" + toStr(now->tm_year + 1900) + '-'
                + toStr(now->tm_mon + 1) + '-'
                + toStr(now->tm_mday) + '_'
                + toStr(now->tm_hour) + '-'
                + toStr(now->tm_min) + '-'
                + toStr(now->tm_sec) + '_'
                + toStr(procId) + ".txt";

        std::ofstream fout;
        fout.open(date.c_str());
        fout << display;
        fout.close();
    }
}

#if GCP>0

int getIdGPU(const int i)//matrix per GPU
{
    int I = i % mdGPU.yTPG;
    int J = i / mdGPU.yTPG;

    const int y = divT(mdGPU.yTPG, GPN);
    const int x = mdGPU.xTPG / (GPN / (mdGPU.yTPG / y));

    I /= y;
    J /= x;

    return I + J * mdGPU.yTPG / y;
}
#endif //GCP>0

#if(GCP>0 && GCP<100) // start GPUCPU update
volatile int sigFromTHS = -1;
volatile int sigFrom[THS]; //wystarczajacy rozmiar to mdCPU.xTPG
bool anyBuff = true;

void initGPUCPU_(MatrixGPU<realX> *xG,
        MatrixCPU<realX> *xC,
        UpdateGPUCPU *bufGPU,
        UpdateGPUCPU *bufCPU,
        const bool GPUaboveCPU) {
    if (mdGPU.mT == mdCPU.mT &&
            mdGPU.mF == mdCPU.mF &&
            mdGPU.mD == mdCPU.mD &&
            mdGPU.xS == mdCPU.xS &&
            mdGPU.xTPG == mdCPU.xTPG &&
            GCU == 1 &&
            AGMG == AGMC) {
        anyBuff = false;
    }

    for (int i = 0; i < mdCPU.xTPG; ++i) {
        sigFrom[i] = -1;
    }
    int j = 0;
    for (int i = 0; i < mdGPU.matrixCount; ++i) {
        if (j < mdGPU.xTPG) {
            bufGPU[j].bodyRow = 0;
            bufGPU[j].fullRow = 0;
            if ((GPUaboveCPU && i % mdGPU.yTPG == mdGPU.yTPG - 1) ||
                    (!GPUaboveCPU && i % mdGPU.yTPG == 0)) {
                bufGPU[j].offset = 0;
                bufGPU[j].bodyRow = xG[i].m;
                bufGPU[j].fullRow = xG[i].m + 6;
                bufGPU[j].matrix = i;
                bufGPU[j].tB.x = KRNDX;
                bufGPU[j].tB.y = KRNDY;
                bufGPU[j].idGPU = xG[i].idGPU;

                if (i / mdGPU.yTPG == 0 || i / mdGPU.yTPG == mdGPU.xTPG - 1) {
                    if (i / mdGPU.yTPG == 0) {
                        bufGPU[j].bodyRow += 3;
                        bufGPU[j].offset = -3;
                    }
                    if (i / mdGPU.yTPG == mdGPU.xTPG - 1) {
                        bufGPU[j].bodyRow += 3;
                    }
                }
                bufGPU[j].nB.x = divT(bufGPU[j].bodyRow, bufGPU[j].tB.x);
                bufGPU[j].nB.y = divT(3, bufGPU[j].tB.y);
                bufGPU[j].nB2.x = divT(bufGPU[j].fullRow, bufGPU[j].tB.x);
                bufGPU[j].nB2.y = divT(3, bufGPU[j].tB.y);
            }
            if (bufGPU[j].bodyRow > 0) {
                ++j;
            }
        }
    }
    int sum = 0;
    for (int i = 0; i < mdGPU.xTPG; ++i) {
        bufGPU[i].previous = sum;
        sum += bufGPU[i].bodyRow;
    }

    j = 0;
    for (int i = 0; i < mdCPU.matrixCount; ++i) {
        if (j < mdCPU.xTPG) {
            bufCPU[j].bodyRow = 0;
            bufCPU[j].fullRow = 0;

            if ((GPUaboveCPU && i % mdCPU.yTPG == 0) ||
                    (!GPUaboveCPU && i % mdCPU.yTPG == mdCPU.yTPG - 1)) {
                bufCPU[j].offset = 0;
                bufCPU[j].bodyRow = xC[i].m;
                bufCPU[j].fullRow = xC[i].m + 6;
                bufCPU[j].matrix = i;
                bufCPU[j].tB.x = CPUDX;
                bufCPU[j].tB.y = CPUDY;

                if (i / mdCPU.yTPG == 0 || i / mdCPU.yTPG == mdCPU.xTPG - 1) {
                    if (i / mdCPU.yTPG == 0) {
                        bufCPU[j].bodyRow += 3;
                        bufCPU[j].offset = -3;
                    }
                    if (i / mdCPU.yTPG == mdCPU.xTPG - 1) {
                        bufCPU[j].bodyRow += 3;
                    }
                }
                bufCPU[j].nB.x = divT(bufCPU[j].bodyRow, bufCPU[j].tB.x);
                bufCPU[j].nB.y = divT(3, bufCPU[j].tB.y);
            }
            if (bufCPU[j].bodyRow > 0) {
                ++j;
            }
        }
    }
    sum = 0;
    for (int i = 0; i < mdCPU.xTPG; ++i) {
        bufCPU[i].previous = sum;
        sum += bufCPU[i].bodyRow;
    }
}

realX getEl(const int i, const int j, const int k,
        UpdateGPUCPU *bufGPU) {
    int x = j;
    int ind = 0;
    for (int I = 0; I < mdGPU.xTPG; ++I) {
        if (x >= bufGPU[I].bodyRow) {
            x -= bufGPU[I].bodyRow;
            ind = I + 1;
        } else {
            break;
        }
    }

    if (ind >= mdGPU.xTPG) {
        return bufGPU[0].CPUtoCPU[0];
    } else {
#if GCU==0
        return bufGPU[ind].CPUtoCPU
                [i * bufGPU[ind].bodyRow * (LDIM + 6) + x + k * bufGPU[ind].bodyRow];
#else
        const int size = mGPU<realX>();
        int flag = 0;
        if (ind > 0) flag = 1;
        return bufGPU[ind].CPUtoCPU
                [i * size * (LDIM + 6) + x + 3 * flag + k * size];
#endif
    }
}

void putEl(const int i, const int j, const int k,
        UpdateGPUCPU *bufGPU, const realX el) {
    int x = j;
    int flag = 0;
    int flag2 = 1;
    int ind = 0;
    for (int I = 0; I < mdGPU.xTPG; ++I) {
        if (x >= bufGPU[I].bodyRow) {
            x -= bufGPU[I].bodyRow;
            ind = I + 1;
            flag = 1;
        } else {
            break;
        }
    }

    if (ind == mdGPU.xTPG - 1) flag2 = 0;

    if (ind >= mdGPU.xTPG) return;
    else {
#if GCU==0
        bufGPU[ind].CPUtoGPU
                [i * bufGPU[ind].fullRow * (LDIM + 6) + x + 3 * flag + k * bufGPU[ind].fullRow] = el;
        if (flag == 1 && x < 3) {
            int offset = (ind - 1 > 0) ? 3 : 0;
            bufGPU[ind - 1].CPUtoGPU
                    [i * bufGPU[ind - 1].fullRow * (LDIM + 6) + x + bufGPU[ind - 1].bodyRow + offset + k * bufGPU[ind - 1].fullRow] = el;
        }
        if (flag2 == 1 && x >= bufGPU[ind].bodyRow - 3) {
            bufGPU[ind + 1].CPUtoGPU
                    [i * bufGPU[ind + 1].fullRow * (LDIM + 6) + x - bufGPU[ind].bodyRow + 3 + k * bufGPU[ind + 1].fullRow] = el;
        }
#else
        const int size = mGPU<realX>();
        bufGPU[ind].CPUtoGPU
                [i * size * (LDIM + 6) + x + 3 * flag + k * size] = el;
        if (flag == 1 && x < 3) {
            int offset = (ind - 1 > 0) ? 3 : 0;
            bufGPU[ind - 1].CPUtoGPU
                    [i * size * (LDIM + 6) + x + bufGPU[ind - 1].bodyRow + offset + k * size] = el;
        }
        if (flag2 == 1 && x >= bufGPU[ind].bodyRow - 3) {
            bufGPU[ind + 1].CPUtoGPU
                    [i * size * (LDIM + 6) + x - bufGPU[ind].bodyRow + 3 + k * size] = el;
        }
#endif
    }
}

class UpdateGPUCPUmain : public RunBase {
private:
    UpdateGPUCPU *bufGPU; //element
    UpdateGPUCPU *bufCPU; //element
    MatrixGPU<realX> *mG; //full matrix
    MatrixCPU<realX> *mC; //full matrix
    const bool GPUaboveCPU;
    int ind;

    int updateMatGtoBufC(const int ts);
    int updateBufCtoMatG(const int ts);
    int query(const int ts);
    int updateGPUCPU(const int ts);
    int queryGPUCPU(const int ts);
    int done(const int ts);

    int (UpdateGPUCPUmain::*stage[4])(const int ts);

    bool queryRows(const int id);
    bool queryRows(const int idTfrom, const int ts);


    int debug_procid;

public:

    UpdateGPUCPUmain(UpdateGPUCPU *bufGPU, UpdateGPUCPU *bufCPU,
            MatrixGPU<realX> *mG, MatrixCPU<realX> *mC,
            const bool GPUaboveCPU)
    : bufGPU(bufGPU), bufCPU(bufCPU), mG(mG), mC(mC), GPUaboveCPU(GPUaboveCPU),
    ind(0) {
        if (anyBuff) {
            stage[0] = &UpdateGPUCPUmain::updateMatGtoBufC;
            stage[1] = &UpdateGPUCPUmain::updateBufCtoMatG;
            stage[2] = &UpdateGPUCPUmain::query;
            stage[3] = &UpdateGPUCPUmain::done;
        } else//chyba tu error
        {
            stage[0] = &UpdateGPUCPUmain::updateGPUCPU;
            stage[1] = &UpdateGPUCPUmain::queryGPUCPU;
            stage[2] = &UpdateGPUCPUmain::done;
            MPI_Comm_rank(MPI_COMM_WORLD, &debug_procid);
        }
    }

    int run(const int ts);

    void reset() {
        ind = 0;
    }
};

bool UpdateGPUCPUmain::queryRows(const int idSfrom) {
    const int offY = (GPUaboveCPU) ? 1 : -1;
    const int iteration = -offY * GPN * SPG / TGP;
    const int end = (offY > 0) ? -idSfrom + GPN * SPG / TGP * (mdGPU.xS - 1) + 1 :
            idSfrom + GPN * SPG / TGP * (mdGPU.xS - 1) + 1;

    bool err = true;
    for (int I = idSfrom + iteration; -offY * I < end; I += iteration) {
        err &= (cudaEventQuery(mdGPU.event[ I ]) == cudaSuccess);
    }
    return err;
}

bool UpdateGPUCPUmain::queryRows(const int idTfrom, const int ts) {
    if (!GPUaboveCPU) {
        const int iteration = -THS / TTH;
        const int end = -idTfrom + THS / TTH * (mdCPU.xS - 1) + 1;

        bool err = true;
        for (int I = idTfrom + iteration; -I < end; I += iteration) {
            if (-(I + iteration) >= end) {
                if (!GPUaboveCPU) {
                    err &= threadQuery(I, 3, ts, 1);
                } else err &= threadQuery(I, 3, ts);
            } else {
                err &= threadQuery(I, 1, ts);
            }
        }
        err &= threadQuery(idTfrom, 3, ts);
        if (-idTfrom - iteration >= end) {
            err &= threadQuery(idTfrom, 3, ts, 1);
        }
        return err;
    } else {
        const int iteration = THS / TTH;
        const int end = idTfrom + THS / TTH * (mdCPU.xS - 1) + 1;

        bool err = true;
        for (int I = idTfrom + iteration; I < end; I += iteration) {
            if (I + iteration >= end) {
                err &= threadQuery(I, 3, ts);
            } else err &= threadQuery(I, 1, ts);
        }
        err &= threadQuery(idTfrom, 3, ts, 1);
        if (idTfrom + iteration >= end) {
            err &= threadQuery(idTfrom, 3, ts);
        }
        return err;
    }
}

int UpdateGPUCPUmain::updateMatGtoBufC(const int ts __attribute__ ((unused))) {
    const int idSfrom = (GPUaboveCPU) ? mG[bufGPU->matrix].lSiM : mG[bufGPU->matrix].fSiM;

    if (queryRows(idSfrom)) {
        cpyGPUtoBuf_(bufGPU, mG, GPUaboveCPU);
        ++ind;
    }
    return 0;
}

int UpdateGPUCPUmain::updateBufCtoMatG(const int ts) {
    bool err = false;
    const int stopCriterium = ts * 100 + 1;

    for (int i = 0; i < mdCPU.xTPG; ++i) {
        if (sigFrom[i] < stopCriterium) {
            err = true;
        }
    }

    if (cudaEventQuery(mG[bufGPU->matrix].updateGPUCPUevent) == cudaSuccess) sigFromTHS = ts * 100 + 1;

    if (!err) {
        cpyBufToGPU_(bufGPU, mG, GPUaboveCPU);
        ++ind;
    }
    return 0;
}

int UpdateGPUCPUmain::query(const int ts) {
    int finished = 0;

    if (cudaEventQuery(mG[bufGPU->matrix].updateGPUCPUevent) == cudaSuccess) {
        sigFromTHS = ts * 100 + 1;
        if (cudaStreamQuery(mG[bufGPU->matrix].updateGPUCPU) == cudaSuccess) {
            ++ind;
            finished = 1;
        }
    }
    return finished;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int UpdateGPUCPUmain::updateGPUCPU(const int ts) {
    const int idSfrom = (GPUaboveCPU) ? mG[bufGPU->matrix].lSiM : mG[bufGPU->matrix].fSiM;
    const int idTfrom = (!GPUaboveCPU) ? mC[bufCPU->matrix].lTiM : mC[bufCPU->matrix].fTiM;

    //usleep(500000);
    if (queryRows(idSfrom) && queryRows(idTfrom, ts))//tu jest błąd!!!!!
    {
        std::cout << debug_procid << "zaczynam kooiowac halo miedzy CPU i GPU\n";
        cpyGPUCPU_(bufGPU, bufCPU, mG, mC, GPUaboveCPU);
        ++ind;
    }
    return 0;
}

int UpdateGPUCPUmain::queryGPUCPU(const int ts) {
    int finished = 0;

    if (GPUaboveCPU) {
        //tu chyba tylko lSiM lub fSiM - bo to swiadczy o zapisie do CPU
        if (cudaStreamQuery(mG[bufGPU->matrix].updateGPUCPU) == cudaSuccess
                && cudaStreamQuery(mdGPU.stream[mG[bufGPU->matrix].lSiM]) == cudaSuccess
                ) {
            sigFromTHS = ts * 100 + 1;
            ++ind;
            finished = 1;
        }
    } else {
        if (cudaStreamQuery(mG[bufGPU->matrix].updateGPUCPU) == cudaSuccess
                && cudaStreamQuery(mdGPU.stream[mG[bufGPU->matrix].fSiM]) == cudaSuccess
                ) {
            sigFromTHS = ts * 100 + 1;
            ++ind;
            finished = 1;
        }
    }
    return finished;
}

int UpdateGPUCPUmain::done(const int ts __attribute__ ((unused))) {
    return 1;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int UpdateGPUCPUmain::run(const int ts) {
    return (this->*stage[ind])(ts);
}

//--

class UpdateGPUCPU_ : public RunBase {
private:
    UpdateGPUCPU *bufGPU; //full matrix
    UpdateGPUCPU *bufCPU; //element
    MatrixCPU<realX> *xC; //full matrix
    const int idTfrom;
    const bool GPUaboveCPU;
    int ind;

    int updateMatCtoBufC(const int ts);
    int updateBufCtoMatC(const int ts);
    int queryGPUCPU(const int ts);
    int done(const int ts);

    int (UpdateGPUCPU_::*stage[3])(const int ts);

    bool queryRows(const int ts);

public:

    UpdateGPUCPU_(UpdateGPUCPU *bufGPU, UpdateGPUCPU *bufCPU, MatrixCPU<realX> *xC,
            const int idTfrom, const bool GPUaboveCPU)
    : bufGPU(bufGPU), bufCPU(bufCPU), xC(xC), idTfrom(idTfrom), GPUaboveCPU(GPUaboveCPU), ind(0) {
        if (anyBuff) {
            stage[0] = &UpdateGPUCPU_::updateMatCtoBufC;
            stage[1] = &UpdateGPUCPU_::updateBufCtoMatC;
            stage[2] = &UpdateGPUCPU_::done;
        } else//chyba tu error
        {
            stage[0] = &UpdateGPUCPU_::queryGPUCPU;
            stage[1] = &UpdateGPUCPU_::done;
        }
    }

    int run(const int ts);

    void reset() {
        ind = 0;
    }
};

bool UpdateGPUCPU_::queryRows(const int ts) {
    if (!GPUaboveCPU) {
        const int iteration = -THS / TTH;
        const int end = -idTfrom + THS / TTH * (mdCPU.xS - 1) + 1;

        bool err = true;
        for (int I = idTfrom + iteration; -I < end; I += iteration) {
            if (-(I + iteration) >= end) {
                err &= threadQuery(I, 3, ts, 1);
            } else err &= threadQuery(I, 1, ts);
        }
        err &= threadQuery(idTfrom, 3, ts);
        if (-idTfrom - iteration >= end) {
            err &= threadQuery(idTfrom, 3, ts, 1);
        }
        return err;
    } else {
        const int iteration = THS / TTH;
        const int end = idTfrom + THS / TTH * (mdCPU.xS - 1) + 1;

        bool err = true;
        for (int I = idTfrom + iteration; I < end; I += iteration) {
            if (I + iteration >= end) {
                err &= threadQuery(I, 3, ts);
            } else err &= threadQuery(I, 1, ts);
        }
        err &= threadQuery(idTfrom, 3, ts, 1);
        if (idTfrom + iteration >= end) {
            err &= threadQuery(idTfrom, 3, ts);
        }
        return err;
    }
}

int UpdateGPUCPU_::updateMatCtoBufC(const int ts) {
    if (queryRows(ts)) {
        const int m = bufCPU->bodyRow;
        const int blx = bufCPU->nB.x;
        const int bly = bufCPU->nB.y;
        const int offset = bufCPU->offset;
        const int previousSteps = bufCPU->previous;
        const int thx = CPUDX;
        const int thy = CPUDY;
        const int xOff = mCPU<realX>();
        const int xOffL = xOff * (LDIM + 6);

        for (int k = -3; k < LDIM + 3; ++k) {
            for (int idI = 0; idI < bly; ++idI) {
                for (int idJ = 0; idJ < blx; ++idJ) {
                    for (int idi = 0; idi < thy; ++idi) {
                        for (int idj = 0; idj < thx; ++idj) {
                            const int i = idI * thy + idi;
                            const int j = idJ * thx + idj;

                            if (i < 3 && j < m) {
                                if (GPUaboveCPU) {
                                    putEl(i, j + previousSteps, k + 3, bufGPU,
                                            xC[bufCPU->matrix].fst[i * xOffL + j + offset + k * xOff]);
                                } else {
                                    putEl(i, j + previousSteps, k + 3, bufGPU,
                                            xC[bufCPU->matrix].fst[(i + xC[bufCPU->matrix].n - 3) * xOffL + j + offset + k * xOff]);
                                }
                            }
                        }
                    }
                }
            }
        }

        const int I = idTfrom / mdCPU.yTPG / mdCPU.xS;
        sigFrom[I] = ts * 100 + 1;
        ++ind;
    }
    return 0;
}

int UpdateGPUCPU_::updateBufCtoMatC(const int ts) {
    const int stopCriterium = ts * 100 + 1;

    if (sigFromTHS >= stopCriterium) {
        const int m = xC[bufCPU->matrix].m + 6;
        const int blx = divT(m, CPUDX);
        const int bly = bufCPU->nB.y;
        const int offset = -3;
        const int previousSteps = (bufCPU->previous > 0) ? bufCPU->previous - 3 : bufCPU->previous;
        const int thx = CPUDX;
        const int thy = CPUDY;
        const int xOff = mCPU<realX>();
        const int xOffL = xOff * (LDIM + 6);

        for (int k = -3; k < LDIM + 3; ++k) {
            for (int idI = 0; idI < bly; ++idI) {
                for (int idJ = 0; idJ < blx; ++idJ) {
                    for (int idi = 0; idi < thy; ++idi) {
                        for (int idj = 0; idj < thx; ++idj) {
                            const int i = idI * thy + idi;
                            const int j = idJ * thx + idj;

                            if (i < 3 && j < m) {
                                if (GPUaboveCPU) {
                                    xC[bufCPU->matrix].fst[(i - 3) * xOffL + j + offset + k * xOff] =
                                            getEl(i, j + previousSteps, k + 3, bufGPU);
                                } else {
                                    xC[bufCPU->matrix].fst[(i + xC[bufCPU->matrix].n) * xOffL + j + offset + k * xOff] =
                                            getEl(i, j + previousSteps, k + 3, bufGPU);
                                }
                            }
                        }
                    }
                }
            }
        }
        const int I = idTfrom / mdCPU.yTPG / mdCPU.xS;
        sigFrom[I] = ts * 100 + 2;
        ++ind;
    }
    return 0;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int UpdateGPUCPU_::queryGPUCPU(const int ts) {
    int finished = 0;
    const int stopCriterium = ts * 100 + 1;
    if (sigFromTHS >= stopCriterium) {
        const int I = idTfrom / mdCPU.yTPG / mdCPU.xS;
        sigFrom[I] = ts * 100 + 2;
        finished = 1;
        ++ind;
    }
    return finished;
}

int UpdateGPUCPU_::done(const int ts __attribute__ ((unused))) {
    return 1;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int UpdateGPUCPU_::run(const int ts) {
    return (this->*stage[ind])(ts);
}

//----query

class QueryGPUCPU_ : public RunBase {
private:
    const int idTfrom;
    int ind;

    int queryGPUCPU(const int ts);
    int done(const int ts);

    int (QueryGPUCPU_::*stage[2])(const int ts);

public:

    QueryGPUCPU_(const int idTfrom)
    : idTfrom(idTfrom), ind(0) {
        stage[0] = &QueryGPUCPU_::queryGPUCPU;
        stage[1] = &QueryGPUCPU_::done;
    }

    int run(const int ts);

    void reset() {
        ind = 0;
    }
};

int QueryGPUCPU_::queryGPUCPU(const int ts) {
    int finished = 0;
    const int stopCriterium = ts * 100 + 2;
    if (sigFrom[idTfrom / mdCPU.yTPG / mdCPU.xS] >= stopCriterium) {
        finished = 1;
        ++ind;
    }
    return finished;
}

int QueryGPUCPU_::done(const int ts __attribute__ ((unused))) {
    return 1;
}

int QueryGPUCPU_::run(const int ts) {
    return (this->*stage[ind])(ts);
}
//---------

#endif //finish GPUCPU update

class RunTimeStep {
private:
    RunBase *exec;
    RunBase **hor;
    RunBase **ver;

    int sizeH;
    int sizeV;

    int procId_deb;
    int tId;



public:

    MatrixCPU<realX> *xC_;
    MatrixCPU<realV1> *v1C_;
    MatrixCPU<realV2> *v2C_;
    MatrixCPU<realV3> *v3C_;
    MatrixCPU<realH> *hC_;
    MatrixCPU<realX> *xPC_;
    MatrixCPU<realV1P> *v1PC_;
    MatrixCPU<realV2P> *v2PC_;
    MatrixCPU<realV3P> *v3PC_;
    MatrixCPU<realCP> *cpC_;
    MatrixCPU<realCN> *cnC_;

    void init(const int threadId, const int procId
#if(GCP>0 && GCP<100)
            , const int GPUaboveCPU
#endif
#if GCP>0
            , MatrixGPU<realX> *xG,
            MatrixGPU<realV1> *v1G,
            MatrixGPU<realV2> *v2G,
            MatrixGPU<realV3> *v3G,
            MatrixGPU<realH> *hG,
            MatrixGPU<realX> *xPG,
            MatrixGPU<realV1P> *v1PG,
            MatrixGPU<realV2P> *v2PG,
            MatrixGPU<realV3P> *v3PG,
            MatrixGPU<realCP> *cpG,
            MatrixGPU<realCN> *cnG
#endif
#if GCP<100
            , MatrixCPU<realX> *xC,
            MatrixCPU<realV1> *v1C,
            MatrixCPU<realV2> *v2C,
            MatrixCPU<realV3> *v3C,
            MatrixCPU<realH> *hC,
            MatrixCPU<realX> *xPC,
            MatrixCPU<realV1P> *v1PC,
            MatrixCPU<realV2P> *v2PC,
            MatrixCPU<realV3P> *v3PC,
            MatrixCPU<realCP> *cpC,
            MatrixCPU<realCN> *cnC
#endif
#if(GCP>0 && GCP<100)
            , UpdateGPUCPU *bufGPU,
            UpdateGPUCPU *bufCPU
#endif
            ) {

        xC_ = xC;
        v1C_ = v1C;
        v2C_ = v2C;
        v3C_ = v3C;
        hC_ = hC;
        xPC_ = xPC;
        v1PC_ = v1PC;
        v2PC_ = v2PC;
        v3PC_ = v3PC;
        cpC_ = cpC;
        cnC_ = cnC;

        procId_deb = procId;
        tId = threadId;
        //printf("[%d]-> %s(%s,%d) || procId:%d, tId:%d\n", getpid(), __func__, __FILE__, __LINE__, procId_deb, tId);
#if GCP<100
        int runFlag = 1;
        bool firstColumn = false;
#endif
        for (int pre = 0; pre < 2; ++pre) {
            int indH = 0;
            int indV = 0;
            if (threadId < THS) {
                //printf("------->[%d]-> %s(%s,%d) || procId:%d, tId:%d\n", getpid(), __func__, __FILE__, __LINE__, procId_deb, tId);
#if(GCP<100 && (NDS>1 || GCP>0))
                MatrixCPU<realX> *mC = xPC;
#endif

#if NDS>1

#if GCP<100
                //left to right
                /*      if (procId < NDS_V - NDS_V / TDS)
                          for (int i = 0; i < mdCPU.yTPG; ++i) {
                              int send = mdCPU.yTPG * (mdCPU.xTPG - 1) + i;
                              if (pre == 1) hor[indH] = new UpdateCPUhorMPImain(mC[send], true, procId, i + GPN * SPG / TGP);
                              ++indH;
                          }*/

                //right to left
                /*      if (procId >= NDS_V / TDS)
                          for (int i = 0; i < mdCPU.yTPG; ++i) {
                              if (pre == 1) hor[indH] = new UpdateCPUhorMPImain(mC[i], false, procId, i + GPN * SPG / TGP);
                              ++indH;
                          }*/

                //top to bottom
                if (procId % (NDS_V / TDS) < NDS_V / TDS - 1) {

                    for (int i = 0; i < mdCPU.xTPG; ++i) {
                        int send = mdCPU.yTPG * i + mdCPU.yTPG - 1;
                        if (pre == 1) ver[indV] = new UpdateCPUverMPImain(mC[send],
                                true, procId, mC[send].lTiM, i + TGP, i);
                        ++indV;
                    }

                }

                //bottom to top
                if (procId % (NDS_V / TDS) > 0) {
#if GCP>0
                    if (!GPUaboveCPU) {
#endif
                        for (int i = 0; i < mdCPU.xTPG; ++i) {
                            int send = mdCPU.yTPG*i;
                            if (pre == 1) ver[indV] = new UpdateCPUverMPImain(mC[send],
                                    false, procId, mC[send].fTiM, i + TGP, i);
                            ++indV;
                        }
#if GCP>0
                    }
#endif
                }
#endif //GCP<100
#endif //NDS>1
#if(GCP>0 && GCP<100)
                for (int i = 0; i < mdGPU.xTPG; ++i) {
                    //           if(GPUaboveCPU)
                    //           {
                    if (pre == 1) ver[indV] = new UpdateGPUCPUmain(bufGPU + i, bufCPU + i,
                            mG, mC, GPUaboveCPU);
                    ++indV;
                    /*            }
                                else
                                {
                                  if(pre==1) ver[indV] = new UpdateGPUCPUmain(bufGPU+i, bufCPU+i,
                                    mG, mC, GPUaboveCPU);
                                  ++indV;
                                }*/
                }
#endif
            }

#if GCP<100
            if (threadId < THS) {
                if (pre == 1) {
                    //printf("-new------>[%d]-> %s(%s,%d) || procId:%d, tId:%d\n", getpid(), __func__, __FILE__, __LINE__, procId_deb, tId);
                    exec = new RunCPU(xC, v1C, v2C, v3C, hC,
                            xPC, v1PC, v2PC, v3PC,
                            cpC, cnC, procId, threadId,
                            runFlag, firstColumn);
                }

                MatrixCPU<realX> *mC = xPC;
                for (int i = 0; i < mdCPU.matrixCount; ++i) {
                    if (mdCPU.tToM[threadId] == i) {
                        //left to right
                        if (i < mC[i].indexR) {
                            if (threadId == mC[i].lTiM) {
                                if (pre == 1) hor[indH] = new UpdateCPUhor(procId, threadId + THS / TTH, threadId,
                                        mC[mC[i].indexR].fst, mC[i].fst, mC[i].n, mdCPU.mT * mdCPU.xS
#if HSPC==1
                                        , true
#endif
                                        );
                                ++indH;
                            }
                        }
                        if (pre == 0) {
                            if (threadId >= THS - THS / TTH)
                                //if(threadId/(THS/TTH)==TTH-1)
                            {
#if NDS>1
                                if (procId >= NDS_V - NDS_V / TDS) {
#endif
                                    runFlag = 3;
#if NDS>1
                                }
#endif
                            }
                        }
                        if (pre == 0) {
                            if (threadId < THS / TTH)
                                //if(threadId/(THS/TTH)==0)
                            {
#if NDS>1
                                if (procId < NDS_V / TDS) {
#endif
                                    firstColumn = true;
#if NDS>1
                                }
#endif
                            }
                        }
                        //top to bottom
                        if (i < mC[i].indexB) {
                            if (threadId == mC[i].lTiM) {
                                if (pre == 1) {
                                    ver[indV] = new UpdateCPUver(mC[mC[i].indexB].fTiM, threadId,
#if CPYC>0
                                            mC[mC[i].indexB].dstTB, mC[i].srcTB, mC[i].sizeV,
#if HSPC==0
                                            mC[i].dstBT, mC[mC[i].indexB].srcBT,
#endif
#else
                                            mC[mC[i].indexB].fst, mC[i].fst, mdCPU.nT * mdCPU.yS, mC[i].m,
#endif
                                            true);
                                }
                                ++indV;
                            } else {
#if HSPC==0
                                if (pre == 1) ver[indV] = new QueryCPUver(mC[i].lTiM);
#else
                                if (pre == 1) ver[indV] = new QueryCPUver(mC[mC[i].indexB].fTiM, 1);
#endif
                                ++indV;
                            }
                        }
                    }

                    const int ir = mdCPU.matrixCount - 1 - i;

                    if (mdCPU.tToM[threadId] == ir) {
                        //right to left
                        if (ir > mC[ir].indexL) {
                            if (threadId == mC[ir].fTiM) {
#if HSPC==1
                                if (pre == 1) hor[indH] = new UpdateCPUhor(procId, threadId - THS / TTH, threadId,
                                        mC[mC[ir].indexL].fst, mC[ir].fst, mC[ir].n, mdCPU.mT * mdCPU.xS, false);
                                ++indH;
#else
                                if (pre == 1) hor[indH] = new QueryCPUhor(threadId, threadId - THS / TTH);
                                ++indH;
#endif
                            }
                        }
                        //bottom to top
                        if (ir > mC[ir].indexT) {
                            if (threadId == mC[ir].fTiM) {
#if HSPC==1
                                if (pre == 1) ver[indV] = new UpdateCPUver(mC[mC[ir].indexT].lTiM, threadId,
#if CPYC>0
                                        mC[mC[ir].indexT].dstBT, mC[ir].srcBT, mC[ir].sizeV,
#else
                                        mC[mC[ir].indexT].fst, mC[ir].fst,
                                        mdCPU.nT * mdCPU.yS, mC[ir].m,
#endif
                                        false);
                                ++indV;
#endif
                            }
#if HSPC==0
                            if (pre == 1) ver[indV] = new QueryCPUver(mC[mC[ir].indexT].lTiM);
                            ++indV;
#else
else {
                                if (pre == 1) ver[indV] = new QueryCPUver(mC[mC[ir].indexT].lTiM);
                                ++indV;
                            }
#endif
                        }
                    }
                }

#if NDS>1
                for (int i = 0; i < mdCPU.yTPG; ++i) {
                    //left to right
                    int send = mdCPU.yTPG * (mdCPU.xTPG - 1) + i;
                    if (mdCPU.tToM[threadId] == send) {
                        if (procId < NDS_V - NDS_V / TDS) {
                            if (threadId == mC[send].lTiM) {
                                if (pre == 1) hor[indH] = new UpdateCPUhorMPI(threadId, mC[send], true, procId);
                                ++indH;
                            } else {
                                if (pre == 1) hor[indH] = new QueryCPUhorMPI(mC[send].lTiM, threadId, true);
                                ++indH;
                            }
                        }
                    }

                    const int ir = mdCPU.yTPG - 1 - i;

                    //right to left
                    if (mdCPU.tToM[threadId] == ir) {
                        if (procId >= NDS_V / TDS) {
                            if (threadId == mC[ir].fTiM) {
                                if (pre == 1) hor[indH] = new UpdateCPUhorMPI(threadId, mC[ir], false, procId);
                                ++indH;
                            } else {
                                if (pre == 1) hor[indH] = new QueryCPUhorMPI(mC[ir].fTiM, threadId, false);
                                ++indH;
                            }
                        }
                    }
                }

                //top to bottom
                if (procId % (NDS_V / TDS) < NDS_V / TDS - 1) {
#if GCP>0
                    if (GPUaboveCPU) {
#endif
                        for (int i = 0; i < mdCPU.xTPG; ++i) {
                            int send = mdCPU.yTPG * i + mdCPU.yTPG - 1;
                            if (mdCPU.tToM[threadId] == send) {
                                if (pre == 1) ver[indV] = new QueryCPUverMPI(i);
                                ++indV;
                            }
                        }
#if GCP>0
                    }
#endif
                }

                //bottom to top
                if (procId % (NDS_V / TDS) > 0) {
#if GCP>0
                    if (!GPUaboveCPU) {
#endif
                        for (int i = 0; i < mdCPU.xTPG; ++i) {
                            int send = mdCPU.yTPG*i;
                            if (mdCPU.tToM[threadId] == send) {
                                if (pre == 1) ver[indV] = new QueryCPUverMPI(i);
                                ++indV;
                            }
                        }
#if GCP>0
                    }
#endif
                }
#endif //NDS>1
#if(GCP>0)&&(GCP<100)
                if (!GPUaboveCPU) {
                    for (int i = 0; i < mdCPU.xTPG; ++i) {
                        int send = mdCPU.yTPG * i + mdCPU.yTPG - 1;
                        if (mdCPU.tToM[threadId] == send) {
                            if (threadId == mC[send].lTiM) {
                                if (pre == 1) ver[indV] = new UpdateGPUCPU_(bufGPU, bufCPU + i, mC, threadId, GPUaboveCPU);
                                ++indV;
                            } else {
                                if (pre == 1) ver[indV] = new QueryGPUCPU_(mC[send].lTiM);
                                ++indV;
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < mdCPU.xTPG; ++i) {
                        int send = mdCPU.yTPG*i;
                        if (mdCPU.tToM[threadId] == send) {
                            if (threadId == mC[send].fTiM) {
                                if (pre == 1) ver[indV] = new UpdateGPUCPU_(bufGPU, bufCPU + i, mC, threadId, GPUaboveCPU);
                                ++indV;
                            } else {
                                if (pre == 1) ver[indV] = new QueryGPUCPU_(mC[send].fTiM);
                                ++indV;
                            }
                        }
                    }
                }
#endif //(GCP>0)&&(GCP<100)
            }
#endif //GCP<100

            if (pre == 0) {
                //printf("[%d]-> %s(%s,%d) || \n", getpid(), __func__, __FILE__, __LINE__);
                sizeH = indH;
                sizeV = indV;
                if (sizeH > 0) hor = new RunBase*[sizeH];
                //if (sizeV > 0) ver = new RunBase*[sizeV];
                if (sizeV > 0) ver = new RunBase*[sizeV]();
            }
        }
    }

    void run(const int ts) {
#if GCP==0
        if (tId < THS)
#endif
            exec->run(ts); //runGPU / runCPU
        if (ts < TS - 1) {
            int sum = 0;
            //printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
            while (sum < sizeH) {
                sum = 0;
                for (int i = 0; i < sizeH; ++i) {
                    sum += hor[i]->run(ts);
                }
            }
            sum = 0;
            //printf("[%d]-> %s(%s,%d) || TS:%d, ts:%d, sum:%d, sizeH:%d, sizeV:%d\n", getpid(), __func__, __FILE__, __LINE__, TS, ts, sum, sizeH, sizeV);
            while (sum < sizeV) {

                sum = 0;
                for (int i = 0; i < sizeV; ++i) {
                    //printf("[%d]-> %s(%s,%d) || TS:%d, ts:%d, sum:%d, sizeH:%d, sizeV:%d, ver[%d]:%d\n", getpid(), __func__, __FILE__, __LINE__, TS, ts, sum, sizeH, sizeV, i, ver[i]->run(ts));
                    //it always return 0 ???
                    sum += ver[i]->run(ts);
                    //printf("[%d]-> %s(%s,%d) || TS:%d, ts:%d, sum:%d, sizeH:%d, sizeV:%d, ver[%d]:%d\n", getpid(), __func__, __FILE__, __LINE__, TS, ts, sum, sizeH, sizeV, i, ver[i]->run(ts));
                }
            }
            for (int i = 0; i < sizeH; ++i) hor[i]->reset();
            for (int i = 0; i < sizeV; ++i) ver[i]->reset();
        }
    }

    RunTimeStep()
    : exec(0), hor(0), ver(0), sizeH(0), sizeV(0) {
    }

    ~RunTimeStep() {
        if (exec) {
            //printf("-delete------>[%d]-> %s(%s,%d) || procId:%d, tId:%d\n", getpid(), __func__, __FILE__, __LINE__, procId_deb, tId);
            delete exec;
        }
        for (int i = 0; i < sizeH; ++i) delete hor[i];
        if (hor) delete [] hor;
        for (int i = 0; i < sizeV; ++i) delete ver[i];
        if (ver) delete [] ver;
    }
};

RunTimeStep rEven, rOdd;

void saveResults(const int procId, const int threadId,
#if GCP>0
        MatrixGPU<realX> *xPGPU, MatrixGPU<realX> *xGPU,
#endif
#if GCP<100
        MatrixCPU<realX> *xPCPU, MatrixCPU<realX> *xCPU,
#endif
        const int ts, const double time, const double energy) {
    if (threadId == THS && procId == 0 && ts == TS - 1) {
        int metaData[14];
        metaData[0] = TS;
        metaData[1] = TSRES;
        metaData[2] = GCP;
        metaData[3] = NDS_V;
        metaData[4] = NDS_V / TDS;
#if GCP>0
        metaData[5] = mdGPU.matrixCount;
        metaData[6] = mdGPU.yTPG;
#else
        metaData[5] = 0;
        metaData[6] = 0;
#endif
#if GCP<100
        metaData[7] = mdCPU.matrixCount;
        metaData[8] = mdCPU.yTPG;
#else
        metaData[7] = 0;
        metaData[8] = 0;
#endif
        metaData[9] = NDIM;
        metaData[10] = MDIM;
        metaData[11] = LDIM;
        metaData[12] = sizeof (realX);
        metaData[13] = 0;

        std::string filename = toStr("./sim/") + TSK + toStr("-metaData.txt");
        std::ofstream fout;
        fout.open(filename.c_str(), std::ofstream::binary);
        fout.write((char*) metaData, 14 * sizeof (int));
        fout.close();
    }

#if GCP>0
    if (threadId == THS) {
#if GCP<100
        const int CPUnPSize = xPCPU[mdCPU.yTPG - 1].n;
        const int CPUnSize = xCPU[mdCPU.yTPG - 1].n;
#else
        const int CPUnPSize = 0;
        const int CPUnSize = 0;
#endif
        if (ts % 2 == 0) saveResGPU(xPGPU, CPUnPSize, procId, ts, time, energy);
        else saveResGPU(xGPU, CPUnSize, procId, ts, time, energy);
    }
#endif

#if GCP<100
#if GTC>1
#pragma omp barrier
#endif
    if (threadId < THS && (threadId / mdCPU.yTPG) % mdCPU.xS == 0) {
#if GCP>0
        const int GPUnPSize = xPGPU[mdGPU.yTPG - 1].n;
        const int GPUnSize = xGPU[mdGPU.yTPG - 1].n;
#else
        const int GPUnPSize = 0;
        const int GPUnSize = 0;
#endif
        if (ts % 2 == 0) saveResCPU(xPCPU, GPUnPSize, procId, threadId, ts, time, energy);
        else saveResCPU(xCPU, GPUnSize, procId, threadId, ts, time, energy);
    }
#endif
}

void sendEx(int step) {
    int my_rank, comm_size, intercomm_size, localSize, factor, dst, res;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);

    factor = intercomm_size / comm_size;
    //NDS_V = intercomm_size;
    //mdCPU.yN = NDS_V;

    localSize = rEven.xC_->n / factor * mdCPU.mDl;
    for (int i = 0; i < factor; i++) {
        dst = my_rank * factor + i; //(3*mlCPU<T>()+3
        int iniPart = i * rEven.xC_->n / factor * mdCPU.mDl;

        /*  std::string tmp="rEven.xC_->n / factor="+toStr(rEven.xC_->n / factor)+"\nlocalSize="+toStr(localSize)+"\niniPart="+
                  toStr(iniPart)+"\nmdCPU.sizeD: N="+toStr(mdCPU.sizeD/sizeof(double)/mdCPU.mDl)+"\nfactor="+toStr(factor)+"\n";
          std::cout << tmp;*/

        if (step % 2 == 0) {
            MPI_Send(rEven.xPC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 0, DMR_INTERCOMM);
            MPI_Send(rEven.cnC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 1, DMR_INTERCOMM);
            MPI_Send(rEven.cpC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 2, DMR_INTERCOMM);
            MPI_Send(rEven.hC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 3, DMR_INTERCOMM);
            MPI_Send(rEven.v1C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 4, DMR_INTERCOMM);
            MPI_Send(rEven.v1PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 5, DMR_INTERCOMM);
            MPI_Send(rEven.v2C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 6, DMR_INTERCOMM);
            MPI_Send(rEven.v2PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 7, DMR_INTERCOMM);
            MPI_Send(rEven.v3C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 8, DMR_INTERCOMM);
            MPI_Send(rEven.v3PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 9, DMR_INTERCOMM);
            MPI_Send(rEven.xC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 10, DMR_INTERCOMM);
        } else {
            MPI_Send(rOdd.xPC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 0, DMR_INTERCOMM);
            MPI_Send(rOdd.cnC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 1, DMR_INTERCOMM);
            MPI_Send(rOdd.cpC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 2, DMR_INTERCOMM);
            MPI_Send(rOdd.hC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 3, DMR_INTERCOMM);
            MPI_Send(rOdd.v1C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 4, DMR_INTERCOMM);
            MPI_Send(rOdd.v1PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 5, DMR_INTERCOMM);
            MPI_Send(rOdd.v2C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 6, DMR_INTERCOMM);
            MPI_Send(rOdd.v2PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 7, DMR_INTERCOMM);
            MPI_Send(rOdd.v3C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 8, DMR_INTERCOMM);
            MPI_Send(rOdd.v3PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 9, DMR_INTERCOMM);
            MPI_Send(rOdd.xC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 10, DMR_INTERCOMM);
        }
        printf("(sergio[%d/%d]): %s(%s,%d): sent %d doubles to = %d, tag = 10\n", my_rank, comm_size, __FILE__, __func__, __LINE__, localSize, dst);
    }
}

void recvEx(int step) {
    int my_rank, comm_size, parent_size, src, factor, iniPart, i, parent_data_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);
    MPI_Status status;

    NDS_V = comm_size;
    mdCPU.yN = NDS_V;

    const int remnantN = divT(NDIM, mdCPU.yN);
    const int maxY = std::max(std::max(std::max(CPUAY, CPUBY), CPUCY), CPUDY);
    mdCPU.nT = rndT(divT(divT(remnantN, THS / TTH), 1), maxY) * THS / TTH / mdCPU.yTPG;
    mdCPU.n = 6 + mdCPU.nT;
    mdCPU.cpu0B.y = divT(mdCPU.nT, CPUAY);
    mdCPU.cpu1B.y = divT(mdCPU.nT, CPUBY);
    mdCPU.cpu2B.y = divT(mdCPU.nT, CPUCY);
    mdCPU.cpu3B.y = divT(mdCPU.nT, CPUDY);
    const int agmElD = divT(AGMC, sizeof (double));
    mdCPU.sizeD = (mdCPU.n * mdCPU.mDl + agmElD) * sizeof (double);
    mdCPU.nBV.y = rndT(divT(divT(remnantN, THS / TTH), 1), maxY) * THS / TTH / mdCPU.yTPG;

    bool GPUaboveCPU = true;
    // std::cout << "NDS_V=" << NDS_V << "\n";

    for (int i = 0; i < mdCPU.matrixCount; ++i) {
        xCPU[i].init(i, GPUaboveCPU, 1, my_rank);
        v1CPU[i].init(i, GPUaboveCPU, 0, my_rank);
        v2CPU[i].init(i, GPUaboveCPU, 0, my_rank);
        v3CPU[i].init(i, GPUaboveCPU, 0, my_rank);
        hCPU[i].init(i, GPUaboveCPU, 0, my_rank);
        xPCPU[i].init(i, GPUaboveCPU, 1, my_rank);
        v1PCPU[i].init(i, GPUaboveCPU, 0, my_rank);
        v2PCPU[i].init(i, GPUaboveCPU, 0, my_rank);
        v3PCPU[i].init(i, GPUaboveCPU, 0, my_rank);
        cpCPU[i].init(i, GPUaboveCPU, 0, my_rank);
        cnCPU[i].init(i, GPUaboveCPU, 0, my_rank);
    }
    rEven.init(0, my_rank, xCPU, v1CPU, v2CPU, v3CPU, hCPU, xPCPU, v1PCPU, v2PCPU, v3PCPU, cpCPU, cnCPU);
    if (TS > 1)
        rOdd.init(0, my_rank, xPCPU, v1CPU, v2CPU, v3CPU, hCPU, xCPU, v1PCPU, v2PCPU, v3PCPU, cpCPU, cnCPU);


    factor = comm_size / parent_size;
    int size = rEven.xC_->n * mdCPU.mDl;
    //  std::cout << "rEven.xC_->n=" << rEven.xC_->n << " mdCPU.mDl=" << mdCPU.mDl << " size=" << size << "\n";
    src = my_rank / factor;
    if (step % 2 == 0) {
        MPI_Recv(rEven.xPC_->fst - 3, size, MPI_DOUBLE, src, 0, DMR_INTERCOMM, &status);
        MPI_Recv(rEven.cnC_->fst - 3, size, MPI_DOUBLE, src, 1, DMR_INTERCOMM, &status);
        MPI_Recv(rEven.cpC_->fst - 3, size, MPI_DOUBLE, src, 2, DMR_INTERCOMM, &status);
        MPI_Recv(rEven.hC_->fst - 3, size, MPI_DOUBLE, src, 3, DMR_INTERCOMM, &status);
        MPI_Recv(rEven.v1C_->fst - 3, size, MPI_DOUBLE, src, 4, DMR_INTERCOMM, &status);
        MPI_Recv(rEven.v1PC_->fst - 3, size, MPI_DOUBLE, src, 5, DMR_INTERCOMM, &status);
        MPI_Recv(rEven.v2C_->fst - 3, size, MPI_DOUBLE, src, 6, DMR_INTERCOMM, &status);
        MPI_Recv(rEven.v2PC_->fst - 3, size, MPI_DOUBLE, src, 7, DMR_INTERCOMM, &status);
        MPI_Recv(rEven.v3C_->fst - 3, size, MPI_DOUBLE, src, 8, DMR_INTERCOMM, &status);
        MPI_Recv(rEven.v3PC_->fst - 3, size, MPI_DOUBLE, src, 9, DMR_INTERCOMM, &status);
        MPI_Recv(rEven.xC_->fst - 3, size, MPI_DOUBLE, src, 10, DMR_INTERCOMM, &status);

    } else {
        MPI_Recv(rOdd.xPC_->fst - 3, size, MPI_DOUBLE, src, 0, DMR_INTERCOMM, &status);
        MPI_Recv(rOdd.cnC_->fst - 3, size, MPI_DOUBLE, src, 1, DMR_INTERCOMM, &status);
        MPI_Recv(rOdd.cpC_->fst - 3, size, MPI_DOUBLE, src, 2, DMR_INTERCOMM, &status);
        MPI_Recv(rOdd.hC_->fst - 3, size, MPI_DOUBLE, src, 3, DMR_INTERCOMM, &status);
        MPI_Recv(rOdd.v1C_->fst - 3, size, MPI_DOUBLE, src, 4, DMR_INTERCOMM, &status);
        MPI_Recv(rOdd.v1PC_->fst - 3, size, MPI_DOUBLE, src, 5, DMR_INTERCOMM, &status);
        MPI_Recv(rOdd.v2C_->fst - 3, size, MPI_DOUBLE, src, 6, DMR_INTERCOMM, &status);
        MPI_Recv(rOdd.v2PC_->fst - 3, size, MPI_DOUBLE, src, 7, DMR_INTERCOMM, &status);
        MPI_Recv(rOdd.v3C_->fst - 3, size, MPI_DOUBLE, src, 8, DMR_INTERCOMM, &status);
        MPI_Recv(rOdd.v3PC_->fst - 3, size, MPI_DOUBLE, src, 9, DMR_INTERCOMM, &status);
        MPI_Recv(rOdd.xC_->fst - 3, size, MPI_DOUBLE, src, 10, DMR_INTERCOMM, &status);
    }
    int number_amount;
    MPI_Get_count(&status, MPI_DOUBLE, &number_amount);
    printf("(sergio[%d/%d]): %s(%s,%d): received %d doubles (%d expected) from = %d (%d expected), tag = %d\n",
            my_rank, comm_size, __FILE__, __func__, __LINE__, number_amount, size, status.MPI_SOURCE, src, status.MPI_TAG);

}

void sendSh(int step) {
    int my_rank, comm_size, intercomm_size, factor, dst;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);

    int localSize = rEven.xC_->n * mdCPU.mDl;

    factor = comm_size / intercomm_size;
    dst = my_rank / factor;

    int iniPart = 0; //We are lazy an we dont want to delete the variable from the MPI_Send instrutions
    if (step % 2 == 0) {
        MPI_Send(rEven.xPC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 0, DMR_INTERCOMM);
        MPI_Send(rEven.cnC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 1, DMR_INTERCOMM);
        MPI_Send(rEven.cpC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 2, DMR_INTERCOMM);
        MPI_Send(rEven.hC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 3, DMR_INTERCOMM);
        MPI_Send(rEven.v1C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 4, DMR_INTERCOMM);
        MPI_Send(rEven.v1PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 5, DMR_INTERCOMM);
        MPI_Send(rEven.v2C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 6, DMR_INTERCOMM);
        MPI_Send(rEven.v2PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 7, DMR_INTERCOMM);
        MPI_Send(rEven.v3C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 8, DMR_INTERCOMM);
        MPI_Send(rEven.v3PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 9, DMR_INTERCOMM);
        MPI_Send(rEven.xC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 10, DMR_INTERCOMM);
    } else {
        MPI_Send(rOdd.xPC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 0, DMR_INTERCOMM);
        MPI_Send(rOdd.cnC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 1, DMR_INTERCOMM);
        MPI_Send(rOdd.cpC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 2, DMR_INTERCOMM);
        MPI_Send(rOdd.hC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 3, DMR_INTERCOMM);
        MPI_Send(rOdd.v1C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 4, DMR_INTERCOMM);
        MPI_Send(rOdd.v1PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 5, DMR_INTERCOMM);
        MPI_Send(rOdd.v2C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 6, DMR_INTERCOMM);
        MPI_Send(rOdd.v2PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 7, DMR_INTERCOMM);
        MPI_Send(rOdd.v3C_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 8, DMR_INTERCOMM);
        MPI_Send(rOdd.v3PC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 9, DMR_INTERCOMM);
        MPI_Send(rOdd.xC_->fst - 3 + iniPart, localSize, MPI_DOUBLE, dst, 10, DMR_INTERCOMM);
    }
    printf("(sergio[%d/%d]): %s(%s,%d): sent %d doubles to = %d, tag = 10\n", my_rank, comm_size, __FILE__, __func__, __LINE__, localSize, dst);

}

void recvSh(int step) {
    int my_rank, comm_size, parent_size, src, factor, iniPart, i;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);
    MPI_Status status;

    NDS_V = comm_size;
    mdCPU.yN = NDS_V;

    const int remnantN = divT(NDIM, mdCPU.yN);
    const int maxY = std::max(std::max(std::max(CPUAY, CPUBY), CPUCY), CPUDY);
    mdCPU.nT = rndT(divT(divT(remnantN, THS / TTH), 1), maxY) * THS / TTH / mdCPU.yTPG;
    mdCPU.n = 6 + mdCPU.nT;
    mdCPU.cpu0B.y = divT(mdCPU.nT, CPUAY);
    mdCPU.cpu1B.y = divT(mdCPU.nT, CPUBY);
    mdCPU.cpu2B.y = divT(mdCPU.nT, CPUCY);
    mdCPU.cpu3B.y = divT(mdCPU.nT, CPUDY);
    const int agmElD = divT(AGMC, sizeof (double));
    mdCPU.sizeD = (mdCPU.n * mdCPU.mDl + agmElD) * sizeof (double);
    mdCPU.nBV.y = rndT(divT(divT(remnantN, THS / TTH), 1), maxY) * THS / TTH / mdCPU.yTPG;

    bool GPUaboveCPU = true;
    // std::cout << "NDS_V=" << NDS_V << "\n";

    for (int i = 0; i < mdCPU.matrixCount; ++i) {
        xCPU[i].init(i, GPUaboveCPU, 1, my_rank);
        v1CPU[i].init(i, GPUaboveCPU, 0, my_rank);
        v2CPU[i].init(i, GPUaboveCPU, 0, my_rank);
        v3CPU[i].init(i, GPUaboveCPU, 0, my_rank);
        hCPU[i].init(i, GPUaboveCPU, 0, my_rank);
        xPCPU[i].init(i, GPUaboveCPU, 1, my_rank);
        v1PCPU[i].init(i, GPUaboveCPU, 0, my_rank);
        v2PCPU[i].init(i, GPUaboveCPU, 0, my_rank);
        v3PCPU[i].init(i, GPUaboveCPU, 0, my_rank);
        cpCPU[i].init(i, GPUaboveCPU, 0, my_rank);
        cnCPU[i].init(i, GPUaboveCPU, 0, my_rank);
    }
    rEven.init(0, my_rank, xCPU, v1CPU, v2CPU, v3CPU, hCPU, xPCPU, v1PCPU, v2PCPU, v3PCPU, cpCPU, cnCPU);
    if (TS > 1)
        rOdd.init(0, my_rank, xPCPU, v1CPU, v2CPU, v3CPU, hCPU, xCPU, v1PCPU, v2PCPU, v3PCPU, cpCPU, cnCPU);

    factor = parent_size / comm_size;


    int size = rEven.xC_->n / factor * mdCPU.mDl;

    for (i = 0; i < factor; i++) {
        src = my_rank * factor + i;

        iniPart = size * i;

        if (step % 2 == 0) {
            MPI_Recv(rEven.xPC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 0, DMR_INTERCOMM, &status);
            MPI_Recv(rEven.cnC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 1, DMR_INTERCOMM, &status);
            MPI_Recv(rEven.cpC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 2, DMR_INTERCOMM, &status);
            MPI_Recv(rEven.hC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 3, DMR_INTERCOMM, &status);
            MPI_Recv(rEven.v1C_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 4, DMR_INTERCOMM, &status);
            MPI_Recv(rEven.v1PC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 5, DMR_INTERCOMM, &status);
            MPI_Recv(rEven.v2C_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 6, DMR_INTERCOMM, &status);
            MPI_Recv(rEven.v2PC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 7, DMR_INTERCOMM, &status);
            MPI_Recv(rEven.v3C_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 8, DMR_INTERCOMM, &status);
            MPI_Recv(rEven.v3PC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 9, DMR_INTERCOMM, &status);
            MPI_Recv(rEven.xC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 10, DMR_INTERCOMM, &status);

        } else {
            MPI_Recv(rOdd.xPC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 0, DMR_INTERCOMM, &status);
            MPI_Recv(rOdd.cnC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 1, DMR_INTERCOMM, &status);
            MPI_Recv(rOdd.cpC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 2, DMR_INTERCOMM, &status);
            MPI_Recv(rOdd.hC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 3, DMR_INTERCOMM, &status);
            MPI_Recv(rOdd.v1C_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 4, DMR_INTERCOMM, &status);
            MPI_Recv(rOdd.v1PC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 5, DMR_INTERCOMM, &status);
            MPI_Recv(rOdd.v2C_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 6, DMR_INTERCOMM, &status);
            MPI_Recv(rOdd.v2PC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 7, DMR_INTERCOMM, &status);
            MPI_Recv(rOdd.v3C_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 8, DMR_INTERCOMM, &status);
            MPI_Recv(rOdd.v3PC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 9, DMR_INTERCOMM, &status);
            MPI_Recv(rOdd.xC_->fst - 3 + iniPart, size, MPI_DOUBLE, src, 10, DMR_INTERCOMM, &status);
        }
        int number_amount;
        MPI_Get_count(&status, MPI_DOUBLE, &number_amount);
        printf("(sergio[%d/%d]): %s(%s,%d): received %d doubles (%d expected) from = %d (%d expected), tag = %d\n",
                my_rank, comm_size, __FILE__, __func__, __LINE__, number_amount, size, status.MPI_SOURCE, src, status.MPI_TAG);
    }
}

void compute(int t) {
    int procId;
    MPI_Comm_rank(MPI_COMM_WORLD, &procId);
    int min = 2, max = 8, pref = 0;
    DMR_Set_parameters(min, max, pref);
    for (int step = t; step < TS; step++) {
        if (procId == 0)
            printf("(sergio): %s(%s,%d) - TIMESTEP %d OF %d\n", __FILE__, __func__, __LINE__, step, TS);
        //sleep(1);
        DMR_RECONFIG(
                compute(step),
                sendEx(step),
                recvEx(step),
                sendSh(step),
                recvSh(step));

        //printf("(sergio): %s(%s,%d) - step %d, TS %d, t %d ((%d))\n", __FILE__, __func__, __LINE__, step, TS, t, getpid());
        rEven.run(step);
        ++step;
        if (step < TS) {
            if (procId == 0)
                printf("(sergio): %s(%s,%d) - TIMESTEP %d OF %d\n", __FILE__, __func__, __LINE__, step, TS);
            rOdd.run(step);
        }
    }
    //printf("Finished (sergio): %s(%s,%d) - step %d, TS %d, t %d ((%d))\n", __FILE__, __func__, __LINE__, step, TS, t, getpid());
    //std::cout << "Finished\n";
}

void run() {
    std::string display;

    int comm_size;
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);

    NDS_V = comm_size;
    mdCPU.yN = NDS_V;

    const int remnantN = divT(NDIM, mdCPU.yN);
    const int maxY = std::max(std::max(std::max(CPUAY, CPUBY), CPUCY), CPUDY);
    mdCPU.nT = rndT(divT(divT(remnantN, THS / TTH), 1), maxY) * THS / TTH / mdCPU.yTPG;
    mdCPU.n = 6 + mdCPU.nT;
    mdCPU.cpu0B.y = divT(mdCPU.nT, CPUAY);
    mdCPU.cpu1B.y = divT(mdCPU.nT, CPUBY);
    mdCPU.cpu2B.y = divT(mdCPU.nT, CPUCY);
    mdCPU.cpu3B.y = divT(mdCPU.nT, CPUDY);
    const int agmElD = divT(AGMC, sizeof (double));
    mdCPU.sizeD = (mdCPU.n * mdCPU.mDl + agmElD) * sizeof (double);
    mdCPU.nBV.y = rndT(divT(divT(remnantN, THS / TTH), 1), maxY) * THS / TTH / mdCPU.yTPG;

    int procId = 0;
    bool GPUaboveCPU = true;
#if NDS>1
    MPI_Comm_rank(MPI_COMM_WORLD, &procId);
    GPUaboveCPU = (procId % (NDS_V / TDS)) % 2 == 0;
#endif

    for (int i = 0; i < mdCPU.matrixCount; ++i) {
        xCPU[i].init(i, GPUaboveCPU, 1, procId);
        v1CPU[i].init(i, GPUaboveCPU, 0, procId);
        v2CPU[i].init(i, GPUaboveCPU, 0, procId);
        v3CPU[i].init(i, GPUaboveCPU, 0, procId);
        hCPU[i].init(i, GPUaboveCPU, 0, procId);
        xPCPU[i].init(i, GPUaboveCPU, 1, procId);
        v1PCPU[i].init(i, GPUaboveCPU, 0, procId);
        v2PCPU[i].init(i, GPUaboveCPU, 0, procId);
        v3PCPU[i].init(i, GPUaboveCPU, 0, procId);
        cpCPU[i].init(i, GPUaboveCPU, 0, procId);
        cnCPU[i].init(i, GPUaboveCPU, 0, procId);
    }



    rEven.init(0, procId, xCPU, v1CPU, v2CPU, v3CPU, hCPU, xPCPU, v1PCPU, v2PCPU, v3PCPU, cpCPU, cnCPU);
    if (TS > 1)
        rOdd.init(0, procId, xPCPU, v1CPU, v2CPU, v3CPU, hCPU, xCPU, v1PCPU, v2PCPU, v3PCPU, cpCPU, cnCPU);


    //    if (procId == 0) displayInfo(display);

    int ySizeNode, xSizeNode;

#if GCP<100
    //initCPU();

    static bool flag = true;

    if (flag) {
        ySizeNode = mdCPU.yTPG * mdCPU.nT * mdCPU.yS;
        xSizeNode = mdCPU.xTPG * mdCPU.mT * mdCPU.xS;
        int ySizeGPU = 0;

        //offYCPU-mdCPU.iOffset[i]+(i%(mdCPU.yTPG*mdCPU.yS))*mdCPU.nT
        const int offYCPU = ySizeNode * (procId % (NDS_V / TDS)) + ySizeGPU;
        const int offXCPU = xSizeNode * (procId / (NDS_V / TDS));
        // TODO call it only once
        initCpuKrnCall(xCPU, v1CPU, v2CPU, v3CPU, hCPU, xPCPU,
                /*v1PCPU, v2PCPU, v3PCPU, cpCPU, cnCPU, procId,*/ offYCPU, offXCPU); //moze warto to wywolac w watkach - first touch
        flag = false;
    }
#endif


#if GCP==0
    omp_set_dynamic(0);
    omp_set_num_threads(THS); //bo jak jest update node - to robi sie w id==THS
#endif

    std::string results = "Results from node: " + toStr(procId) + "\n";

#if NDS>1
    MPI_Barrier(MPI_COMM_WORLD);
#endif
    TimeCPU clusterT;
    TimeCPU nodeT;
    if (procId == 0) clusterT.start();
    nodeT.start();
#if GCP<100
#pragma omp parallel
    {
        //Thread affinity
        cpu_set_t set;
        CPU_ZERO(&set);
        int id = omp_get_thread_num();
        //printf("[%d]-> %s(%s,%d) || tId: %d\n", getpid(), __func__, __FILE__, __LINE__, id);
        CPU_SET(id, &set);
        pid_t tid = (pid_t) syscall(SYS_gettid);
        sched_setaffinity(tid, sizeof (set), &set);
#else //GCP<100
    const int id = THS;
#endif //GCP<100

        // TODO do this in every receive DMR

#if NDS>1
        if (id == THS) MPI_Barrier(MPI_COMM_WORLD);
#endif
#if GCP<100
#pragma omp barrier
#endif

        TimeCPU threadT;
        threadT.start();

        //--==##==--
        int step = 0;
        compute(step);
        //printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
        //exit(0);
        //--==##==--

        MPI_Comm_rank(MPI_COMM_WORLD, &procId);
        printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
        double threadTime = threadT.read();
#if GCP<100
#pragma omp critical
        {
#endif
#if GCP<100
            if (id < THS) {
                results += "  Exec. time (CPU thread: " + toStr(id) + "): " +
                        toStr(threadTime) + " [s]\n";
            }
#endif
#if GCP>0 && GCP<100
            else
#endif
#if GCP>0
                results += "  Exec. time (GPU thread): " + toStr(threadTime) + " [s]\n";
#endif
#if GCP<100
        }
#endif
#if GCP<100
    }//end omp parallel
#endif //GCP<100
    //pamietac by dobrze ustawic cudaSetDevice - bo w update node nie ma tego - a strumienie naleza do gpu
    double nodeTime = nodeT.read();

#if NDS>1
    MPI_Barrier(MPI_COMM_WORLD);
#endif
    double clusterTime;
    if (procId == 0) {
        clusterTime = clusterT.read();
    }
    results += "  Exec. time (node: " + toStr(procId) + "): " +
            toStr(nodeTime) + " [s]\n";
    if (procId == 0) {
        display += "Exec. time (cluster): " +
                toStr(clusterTime) + " [s]\n";
    }
    display += results;

#if NDS>1
    for (int i = 0; i < NDS_V; ++i) {
        if (procId == i) {
#endif
#if OUTPUT==1
            printToFile(display, procId);
#endif
            std::cout << display;
#if NDS>1
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif
}
