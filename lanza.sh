#!/bin/bash

#salloc -N2 ./lanza.sh

export PATH=/home/siserte/ompss-new/bin/:$PATH
export LD_LIBRARY_PATH=/state/partition1/soft/intel/compilers_and_libraries_2017/linux/lib/intel64/:$LD_LIBRARY_PATH
export PATH=/state/partition1/soft/intel/compilers_and_libraries_2017/linux/bin/intel64/:$PATH 

export LD_LIBRARY_PATH=/home/siserte/slurm/lib:/state/partition1/soft/gnu/mpich-3.2/lib:$LD_LIBRARY_PATH
#export NX_ARGS="--enable-block --force-tie-master --smp-workers=12"
#export NX_ARGS="--smp-workers=12"
#export NX_ARGS="--enable-block --force-tie-master"
#export NX_ARGS="--enable-block"

NODELIST="$(scontrol show hostname $SLURM_JOB_NODELIST | paste -d, -s)"
mpiexec -iface eth0 -n $SLURM_JOB_NUM_NODES -hosts $NODELIST ./bin/mpdata.INTEL64
