#include "etmLib.h"

/*
 * pkg - the entire processor
 * pp0 - cores
 * pp1 - uncore resources (integrated GPU)
 * dram - ram memory
 * server - motherboard with its components
 */

void show(TimeCPU &t, ETM_Energy& e)
{
  std::cout << "------======###### Start statistics ######======------\n";
  std::cout << "---===## Energy ##===---\n";
  if(e.ifPKG()) std::cout << "PKG E [J]      = " << e.pkgE() << '\n';
  if(e.ifPP0()) std::cout << "PP0 E [J]      = " << e.pp0E() << '\n';
  if(e.ifPP1()) std::cout << "PP1 E [J]      = " << e.pp1E() << '\n';
  if(e.ifDRAM()) std::cout << "DRAM E [J]     = " << e.dramE() << '\n';
  double cpuE_DRAM = e.pkgE() + e.dramE();
  std::cout << "The whole CPU and DRAM E [J] = " << cpuE_DRAM << '\n';
  if(e.ifServer()) std::cout << "Server E [J]   = " << e.serverE() << '\n';
  std::cout << "---===## Power ##===---\n";
  if(e.ifPKG()) std::cout << "PKG P [W]      = " << e.pkgE()/t << '\n';
  if(e.ifPP0()) std::cout << "PP0 P [W]      = " << e.pp0E()/t << '\n';
  if(e.ifPP1()) std::cout << "PP1 P [W]      = " << e.pp1E()/t << '\n';
  if(e.ifDRAM()) std::cout << "DRAM P [W]     = " << e.dramE()/t << '\n';
  std::cout << "All the CPUs and DRAMs P [W] = " << cpuE_DRAM/t << '\n';
  if(e.ifServer()) std::cout << "Server P [W]   = " << e.serverE()/t << '\n';
  std::cout << "---===## Time ##===---\n";
  std::cout << "Exec. time [s] = " << t << '\n';
  std::cout << "---===## You have " << e.socketCount()
            << " socket(s) in your system ##===---\n";

  for(unsigned int i=0; i<e.socketCount(); ++i)
  {
    std::cout << "---===## Energy for socket " << i << " ##===---\n";
    if(e.ifPKG()) std::cout << "PKG E [J]      = " << e.pkgE(i) << '\n';
    if(e.ifPP0()) std::cout << "PP0 E [J]      = " << e.pp0E(i) << '\n';
    if(e.ifPP1()) std::cout << "PP1 E [J]      = " << e.pp1E(i) << '\n';
    if(e.ifDRAM()) std::cout << "DRAM E [J]     = " << e.dramE(i) << '\n';
    double cpuE_DRAM = e.pkgE(i) + e.dramE(i);
    std::cout << "The whole CPU and DRAM E [J] = " << cpuE_DRAM << '\n';
  }
  std::cout << "------======###### Finish statistics ######======------\n\n";
}

int main()
{
  std::cout << "Data type conversion:\n";
  int a=1;
  float b=2.0;
  double c=3.0;
  char piC[14]="3.14159265359";
  std::string piS="3.14159265359";

  std::cout << "Int to string:    " << toStr(a) << '\n';
  std::cout << "Float to string:  " << toStr(b) << '\n';
  std::cout << "Double to string: " << toStr(c) << '\n';
  std::cout << "Char* to double:  " << strTo<double>(piC) << '\n';
  std::cout << "String to double: " << strTo<double>(piS) << "\n\n";

  std::cout << "Energy and Time Measurement:\n";
  ETM_Energy e;
  TimeCPU t;

  //reset counters                           read0  read1
  if(e.ifPKG()) e.startPKG();                //|      |
  if(e.ifPP0()) e.startPP0();                //|      |
  if(e.ifPP1()) e.startPP1();                //|      |
  if(e.ifDRAM()) e.startDRAM();              //|      |
  if(e.ifServer()) e.startServer();          //|      |
  t.start();                                 //|      |
                                             //|      |
  //do some job and collect new statictics   //|      |
  sleep(3);                                  //|      |
                                             //|      |
  //read0 counters status                    //|      |
  t.read();                                  //|      |
  if(e.ifPKG()) e.readPKG();                 //|      |
  if(e.ifPP0()) e.readPP0();                 //|      |
  if(e.ifPP1()) e.readPP1();                 //|      |
  if(e.ifDRAM()) e.readDRAM();               //|      |
  if(e.ifServer()) e.readServer();           //|      |
                                           //end0     |
  //print counters status                    //       |
  show(t, e);                                //       |
                                             //       |
  //do more job and continue collecting the statistics|
  sleep(3);                                  //       |
                                             //       |
  //read1 counters status                    //       |
  t.read();                                  //       |
  if(e.ifPKG()) e.readPKG();                 //       |
  if(e.ifPP0()) e.readPP0();                 //       |
  if(e.ifPP1()) e.readPP1();                 //       |
  if(e.ifDRAM()) e.readDRAM();               //       |
  if(e.ifServer()) e.readServer();           //       |
                                             //     end1
  //print counters status
  show(t, e);

	return 0;
}
