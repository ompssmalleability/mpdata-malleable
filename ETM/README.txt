Data type conversion:
Int to string:    1
Float to string:  2
Double to string: 3
Char* to double:  3.14159
String to double: 3.14159

Time and energy measurement:
------======###### Start statistics ######======------
---===## Energy ##===---
PKG E [J]      = 1.57755
PP0 E [J]      = 0.186951
DRAM E [J]     = 3.6423
The whole CPU and DRAM E [J] = 5.21985
Server E [J]   = 421.522
---===## Power ##===---
PKG P [W]      = 0.525831
PP0 P [W]      = 0.0623148
DRAM P [W]     = 1.21406
All the CPUs and DRAMs P [W] = 1.73989
Server P [W]   = 140.502
---===## Time ##===---
Exec. time [s] = 3.0001
---===## You have 1 socket(s) in your system##===---
---===## Energy for socket 0 ##===---
PKG E [J]      = 1.57755
PP0 E [J]      = 0.186951
DRAM E [J]     = 3.6423
The whole CPU and DRAM E [J] = 5.21985
------======###### Finish statistics ######======------

------======###### Start statistics ######======------
---===## Energy ##===---
PKG E [J]      = 3.24329
PP0 E [J]      = 0.370865
DRAM E [J]     = 7.4097
The whole CPU and DRAM E [J] = 10.653
Server E [J]   = 841.55
---===## Power ##===---
PKG P [W]      = 0.540378
PP0 P [W]      = 0.0617914
DRAM P [W]     = 1.23456
All the CPUs and DRAMs P [W] = 1.77494
Server P [W]   = 140.214
---===## Time ##===---
Exec. time [s] = 6.00188
---===## You have 1 socket(s) in your system ##===---
---===## Energy for socket 0 ##===---
PKG E [J]      = 3.24329
PP0 E [J]      = 0.370865
DRAM E [J]     = 7.4097
The whole CPU and DRAM E [J] = 10.653
------======###### Finish statistics ######======------

