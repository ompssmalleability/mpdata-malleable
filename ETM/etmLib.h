#ifndef ETMLIB_H
#define ETMLIB_H

#include "etmConfig.h"

#include <cmath>
#include <cstdlib>
#include <fcntl.h>
#include <fstream>
#include <inttypes.h>
#include <iostream>
#include <linux/perf_event.h>
#include <sstream>
#include <sys/syscall.h>
#include <sys/time.h>
#include <unistd.h>
#include <vector>

#ifdef GET_SERVER
#include "pmlib.h"
#endif

#ifdef GET_CPU

#define MSR_RAPL_POWER_UNIT 0x606

//RAPL package - the entire processor
#define MSR_PKG_ENERGY_STATUS		0x611

//RAPL PP0 - all the cores
#define MSR_PP0_ENERGY_STATUS		0x639

//RAPL PP1 - uncore devices (integrated GPU)
#define MSR_PP1_ENERGY_STATUS		0x641

//RAPL DRAM - the dram memory
#define MSR_DRAM_ENERGY_STATUS		0x619

#define CPU_SANDYBRIDGE 42
#define CPU_SANDYBRIDGE_EP 45
#define CPU_IVYBRIDGE 58
#define CPU_IVYBRIDGE_EP 62
#define CPU_HASWELL 60
#define CPU_HASWELL_EP 63
#define CPU_BROADWELL 61

#endif

template<typename T>
std::string toStr(const T& nr)
{
  std::ostringstream os;
  os << nr;
  return os.str();
}

template<typename T>
T strTo(const std::string &s)
{
  T num;
  std::istringstream is(s);
  is >> num;
  return num;
}

template<typename T>
T strTo(const char* s)
{
  T num;
  std::istringstream is(s);
  is >> num;
  return num;
}

class TimeCPU
{
  private:
    struct timeval ts;
    double t;

  public:
    TimeCPU()
    : t(0.0)
    {}

    void start()
    {
      gettimeofday(&ts, NULL);
    }

    double read()
    {
      struct timeval te;
      gettimeofday(&te, NULL);
      t = (double)(te.tv_sec-ts.tv_sec)+0.000001*(double)(te.tv_usec-ts.tv_usec);
      return t;
    }

    const double& time() const { return t; }
    double time() { return t; }
};

inline std::ostream& operator<<(std::ostream& os, const TimeCPU& t)
{
  return os << t.time();
}

inline double operator/(const double& l, const TimeCPU& r)
{
  return l/r.time();
}

inline double operator/(const TimeCPU& l, const double& r)
{
  return l.time()/r;
}

inline double operator*(const double& l, const TimeCPU& r)
{
  return l*r.time();
}

inline double operator*(const TimeCPU& l, const double& r)
{
  return l.time()*r;
}

inline double operator+(const double& l, const TimeCPU& r)
{
  return l+r.time();
}

inline double operator+(const TimeCPU& l, const double& r)
{
  return l.time()+r;
}

inline double operator-(const double& l, const TimeCPU& r)
{
  return l-r.time();
}

inline double operator-(const TimeCPU& l, const double& r)
{
  return l.time()-r;
}

class ETM_Energy
{
  private:
#ifdef GET_CPU
    std::vector<int> fd; //file descriptor
    std::vector<double> cpuEnergyUnits;
    std::vector<double> dramEnergyUnits;

    //all the sockets
    double pkgBefore;
    double pp0Before;
    double pp1Before;
    double dramBefore;

    double pkg_;
    double pp0_;
    double pp1_;
    double dram_;

    //single socket
    std::vector<double> pkgSBefore;
    std::vector<double> pp0SBefore;
    std::vector<double> pp1SBefore;
    std::vector<double> dramSBefore;

    std::vector<double> pkgS;
    std::vector<double> pp0S;
    std::vector<double> pp1S;
    std::vector<double> dramS;

    bool pkgpp0Flag;
    bool pp1Flag;
    bool dramFlag;
#endif

#ifdef GET_SERVER
    server_t server;
    counter_t counter;
    line_t lines;
    double serverEnergy;
    bool counterStarted;
#endif

#ifdef GET_CPU
    int type;

    std::vector<int> pkgFD;
    std::vector<int> pp0FD;
    std::vector<int> pp1FD;
    std::vector<int> dramFD;

    uint64_t readMSR(int fd, int which); //model-specific register
    uint64_t readPERF(int fd);
#endif

  public:
    ETM_Energy();

    void startPKG();
    void startPP0();
    void startPP1();
    void startDRAM();
    void startServer();

    double readPKG();
    double readPP0();
    double readPP1();    
    double readDRAM();
    double readServer();

    bool ifPKG()
    {
#ifdef GET_CPU
      return pkgpp0Flag;
#else
      return false;
#endif
    }
    bool ifPP0()
    {
#ifdef GET_CPU
      return pkgpp0Flag;
#else
      return false;
#endif
    }
    bool ifPP1()
    {
#ifdef GET_CPU
      return pp1Flag;
#else
      return false;
#endif
    }
    bool ifDRAM()
    {
#ifdef GET_CPU
      return dramFlag;
#else
      return false;
#endif
    }
    bool ifServer()
    {
#ifdef GET_SERVER
      return true;
#else
      return false;
#endif
    }

    double pkgE(int s=-1)
    {
#ifdef GET_CPU
      return (s==-1)? pkg_ : pkgS[s];
#else
      return (s==-1)? 0.0 : 0.0;
#endif
    }
    double pp0E(int s=-1)
    {
#ifdef GET_CPU
      return (s==-1)? pp0_ : pp0S[s];
#else
      return (s==-1)? 0.0 : 0.0;
#endif
    }
    double pp1E(int s=-1)
    {
#ifdef GET_CPU
      return (s==-1)? pp1_ : pp1S[s];
#else
      return (s==-1)? 0.0 : 0.0;
#endif
    }
    double dramE(int s=-1)
    {
#ifdef GET_CPU
      return (s==-1)? dram_ : dramS[s];
#else
      return (s==-1)? 0.0 : 0.0;
#endif
    }
    double serverE()
    {
#ifdef GET_SERVER
      return serverEnergy;
#else
      return 0.0;
#endif
    }

    unsigned int socketCount()
    {
#ifdef GET_CPU
      return fd.size();
#else
      return 0.0;
#endif
    }
    
    ~ETM_Energy();
};

#endif
