#include "etmLib.h"

#ifdef GET_SERVER
char pmLibIP[] = PMLIBIP;
char pmLibLine[] = PMLIBLINE;
char pmLibCounter[] = PMLIBCOUNTER;
int pmLibPort = PMLIBPORT;
#endif

#ifdef GET_CPU
int perf_event_open(struct perf_event_attr *hw_event_uptr,
    pid_t pid, int cpu, int group_fd, unsigned long flags)
{
  return syscall(__NR_perf_event_open, hw_event_uptr, pid, cpu,
         group_fd, flags);
}
#endif

#ifdef GET_CPU
uint64_t ETM_Energy::readMSR(int fd, int which)
{
  uint64_t data;
  if( pread(fd, &data, sizeof(data), which) != sizeof(data) )
  {
    std::cerr << "Error while reading MSR!\n";
  }
  return data;
}

uint64_t ETM_Energy::readPERF(int fd)
{
  uint64_t data;
  if( read(fd, &data, sizeof(data)) != sizeof(data) )
  {
    std::cerr << "Error while reading PERF!\n";
  }
  return data;
}
#endif

ETM_Energy::ETM_Energy()
#ifdef GET_CPU
: pkgBefore(0.0), pp0Before(0.0), pp1Before(0.0), dramBefore(0.0),
  pkg_(0.0), pp0_(0.0), pp1_(0.0), dram_(0.0)
#ifdef GET_SERVER
,
#endif
#else
#ifdef GET_SERVER
:
#endif
#endif
#ifdef GET_SERVER
serverEnergy(0.0), counterStarted(false)
#endif
{
#ifdef GET_CPU
  std::string tmp;
  std::ifstream fin;

  type=-1;
  fin.open("/sys/bus/event_source/devices/power/type");
	if(fin)
  {
		fin >> type;
	}
	fin.close();

  int result = std::system("lscpu > /tmp/ETM_Tmp.txt");
  fin.open("/tmp/ETM_Tmp.txt");
  do{
    fin >> tmp;
  }while(tmp!="Model:" && fin);
  fin >> tmp;

  int cpuModel = strTo<int>(tmp);

  do{
    fin >> tmp;
    if(tmp=="NUMA")
    {
      fin >> tmp;
      fin >> tmp;
      if(tmp=="CPU(s):")
      {
        fin >> tmp;
        int core = strTo<int>(tmp);
        if(type==-1)
        {
          std::string fileName = "/dev/cpu/"+toStr(core)+"/msr";
          fd.push_back( open(fileName.c_str(), O_RDONLY) );
        }
        else
        {
          fd.push_back(core);
        }
      }
    }
  }while(fin);

  fin.close();
  result |= std::system("rm /tmp/ETM_Tmp.txt");
  if(result!=0) std::cerr << "Error while reading CPU!\n";

  cpuEnergyUnits.resize(fd.size());
  dramEnergyUnits.resize(fd.size());
  pkgSBefore.resize(fd.size());
  pp0SBefore.resize(fd.size());
  pp1SBefore.resize(fd.size());
  dramSBefore.resize(fd.size());
  pkgS.resize(fd.size());
  pp0S.resize(fd.size());
  pp1S.resize(fd.size());
  dramS.resize(fd.size());
  pkgFD.resize(fd.size());
  pp0FD.resize(fd.size());
  pp1FD.resize(fd.size());
  dramFD.resize(fd.size());
  
  for(unsigned int i=0; i<fd.size(); ++i)
  {
    pkgFD[i] = -1;
    pp0FD[i] = -1;
    pp1FD[i] = -1;
    dramFD[i] = -1;
  }

  if(type==-1)
  {
    for(unsigned int i=0; i<fd.size(); ++i)
    {
      uint64_t result = readMSR(fd[i], MSR_RAPL_POWER_UNIT);
      cpuEnergyUnits[i] = pow(0.5, (double)((result>>8)&0x1f));
      
      if(cpuModel==CPU_HASWELL_EP)
      {
        dramEnergyUnits[i] = pow(0.5, 16.0);
      }
      else
      {
        dramEnergyUnits[i] = cpuEnergyUnits[i];
      }
    }
  }
  else
  {
    unsigned int config=0;
    double cpuEU;
    double dramEU;

    fin.open("/sys/bus/event_source/devices/power/events/energy-pkg");
    if(fin)
    {
      fin >> tmp;
      tmp.erase(0, 6);
      sscanf(tmp.c_str(), "%x", &config);
      fin.close();
    }
    fin.open("/sys/bus/event_source/devices/power/events/energy-pkg.scale");
    if(fin)
    {
      fin >> cpuEU;
      fin.close();
      for(unsigned int i=0; i<fd.size(); ++i) cpuEnergyUnits[i] = cpuEU;
      static struct perf_event_attr attr;
      attr.type=type;
      attr.config=config;
      for(unsigned int i=0; i<fd.size(); ++i)
        pkgFD[i]=perf_event_open(&attr, -1, fd[i], -1, 0);
    }

    fin.open("/sys/bus/event_source/devices/power/events/energy-ram");
    if(fin)
    {
      fin >> tmp;
      tmp.erase(0, 6);
      sscanf(tmp.c_str(), "%x", &config);
      fin.close();
    }
    fin.open("/sys/bus/event_source/devices/power/events/energy-ram.scale");
    if(fin)
    {
      fin >> dramEU;
      fin.close();
      for(unsigned int i=0; i<fd.size(); ++i) dramEnergyUnits[i] = dramEU;
      static struct perf_event_attr attr;
      attr.type=type;
      attr.config=config;
      for(unsigned int i=0; i<fd.size(); ++i)
        dramFD[i]=perf_event_open(&attr, -1, fd[i], -1, 0);
    }

    fin.open("/sys/bus/event_source/devices/power/events/energy-cores");
    if(fin)
    {
      fin >> tmp;
      tmp.erase(0, 6);
      sscanf(tmp.c_str(), "%x", &config);
      fin.close();
      static struct perf_event_attr attr;
      attr.type=type;
      attr.config=config;
      for(unsigned int i=0; i<fd.size(); ++i)
        pp0FD[i]=perf_event_open(&attr, -1, fd[i], -1, 0);
    }

    fin.open("/sys/bus/event_source/devices/power/events/energy-gpu");
    if(fin)
    {
      fin >> tmp;
      tmp.erase(0, 6);
      sscanf(tmp.c_str(), "%x", &config);
      fin.close();
      static struct perf_event_attr attr;
      attr.type=type;
      attr.config=config;
      for(unsigned int i=0; i<fd.size(); ++i)
        pp1FD[i]=perf_event_open(&attr, -1, fd[i], -1, 0);
    }
  }

  if((cpuModel==CPU_SANDYBRIDGE) || (cpuModel==CPU_IVYBRIDGE) ||
     (cpuModel==CPU_HASWELL))
  pp1Flag = true;
  else pp1Flag = false;

  if((cpuModel==CPU_SANDYBRIDGE_EP) || (cpuModel==CPU_IVYBRIDGE_EP) ||
     (cpuModel==CPU_HASWELL_EP) || (cpuModel==CPU_HASWELL) ||
     (cpuModel==CPU_BROADWELL))
  dramFlag = true;
  else dramFlag = false;
  
  if((cpuModel==CPU_SANDYBRIDGE) || (cpuModel==CPU_SANDYBRIDGE_EP) ||
     (cpuModel==CPU_IVYBRIDGE) || (CPU_IVYBRIDGE_EP) ||
     (cpuModel==CPU_HASWELL) || (CPU_HASWELL_EP) ||
     (cpuModel==CPU_BROADWELL))
  pkgpp0Flag = true;
  else pkgpp0Flag = false;
#endif
#ifdef GET_SERVER
  pm_set_server(pmLibIP, pmLibPort, &server);
  pm_set_lines(pmLibLine, &lines);
  pm_create_counter(pmLibCounter, lines, 1, 0, server, &counter);
#endif
}

void ETM_Energy::startPKG()
{
#ifdef GET_CPU
  pkgBefore=0.0;
  for(unsigned int i=0; i<fd.size(); ++i)
  {
    uint64_t result;
    if(type==-1) result = readMSR(fd[i], MSR_PKG_ENERGY_STATUS);
    else result = readPERF(pkgFD[i]);
    pkgSBefore[i] = result*cpuEnergyUnits[i];
    pkgBefore += pkgSBefore[i];
  }
#endif
}

void ETM_Energy::startPP0()
{
#ifdef GET_CPU
  pp0Before=0.0;
  for(unsigned int i=0; i<fd.size(); ++i)
  {
    uint64_t result;
    if(type==-1) result = readMSR(fd[i], MSR_PP0_ENERGY_STATUS);
    else result = readPERF(pp0FD[i]);
    pp0SBefore[i] = result*cpuEnergyUnits[i];
    pp0Before += pp0SBefore[i];
  }
#endif
}

void ETM_Energy::startPP1()
{
#ifdef GET_CPU
  pp1Before=0.0;
  for(unsigned int i=0; i<fd.size(); ++i)
  {
    uint64_t result;
    if(type==-1) result = readMSR(fd[i], MSR_PP1_ENERGY_STATUS);
    else result = readPERF(pp1FD[i]);
    pp1SBefore[i] = result*cpuEnergyUnits[i];
    pp1Before += pp1SBefore[i];
  }
#endif
}

void ETM_Energy::startDRAM()
{
#ifdef GET_CPU
  dramBefore=0.0;
  for(unsigned int i=0; i<fd.size(); ++i)
  {
    uint64_t result;
    if(type==-1) result = readMSR(fd[i], MSR_DRAM_ENERGY_STATUS);
    else result = readPERF(dramFD[i]);
    dramSBefore[i] = result*dramEnergyUnits[i];
    dramBefore += dramSBefore[i];
  }
#endif
}

void ETM_Energy::startServer()
{
#ifdef GET_SERVER
  if(counterStarted) pm_stop_counter(&counter);

  serverEnergy=0.0;
  pm_start_counter(&counter);
  counterStarted=true;
#endif
}

double ETM_Energy::readPKG()
{
#ifdef GET_CPU
  double pkgAfter=0.0;
  for(unsigned int i=0; i<fd.size(); ++i)
  {
    uint64_t result;
    if(type==-1) result = readMSR(fd[i], MSR_PKG_ENERGY_STATUS);
    else result = readPERF(pkgFD[i]);
    pkgS[i] = result*cpuEnergyUnits[i] - pkgSBefore[i];
    pkgAfter += result*cpuEnergyUnits[i];
  }
  pkg_ = pkgAfter - pkgBefore;
  return pkg_;
#else
  return 0.0;
#endif
}

double ETM_Energy::readPP0()
{
#ifdef GET_CPU
  double pp0After=0.0;
  for(unsigned int i=0; i<fd.size(); ++i)
  {
    uint64_t result;
    if(type==-1) result = readMSR(fd[i], MSR_PP0_ENERGY_STATUS);
    else result = readPERF(pp0FD[i]);
    pp0S[i] = result*cpuEnergyUnits[i] - pp0SBefore[i];
    pp0After += result*cpuEnergyUnits[i];
  }
  pp0_ = pp0After - pp0Before;
  return pp0_;
#else
  return 0.0;
#endif
}

double ETM_Energy::readPP1()
{
#ifdef GET_CPU
  double pp1After=0.0;
  for(unsigned int i=0; i<fd.size(); ++i)
  {
    uint64_t result;
    if(type==-1) result = readMSR(fd[i], MSR_PP1_ENERGY_STATUS);
    else result = readPERF(pp1FD[i]);
    pp1S[i] = result*cpuEnergyUnits[i] - pp1SBefore[i];
    pp1After += result*cpuEnergyUnits[i];
  }
  pp1_ = pp1After - pp1Before;
  return pp1_;
#else
  return 0.0;
#endif
}

double ETM_Energy::readDRAM()
{
#ifdef GET_CPU
  double dramAfter=0.0;
  for(unsigned int i=0; i<fd.size(); ++i)
  {
    uint64_t result;
    if(type==-1) result = readMSR(fd[i], MSR_DRAM_ENERGY_STATUS);
    else result = readPERF(dramFD[i]);
    dramS[i] = result*cpuEnergyUnits[i] - dramSBefore[i];
    dramAfter += result*dramEnergyUnits[i];
  }
  dram_ = dramAfter - dramBefore;
  return dram_;
#else
  return 0.0;
#endif
}

double ETM_Energy::readServer()
{
#ifdef GET_SERVER
  pm_stop_counter(&counter);
  pm_get_counter_data(&counter);
  int last=counter.measures->energy.watts_sets_size-1;

  for(int s=0; s<last; ++s)
  {
    int ini=counter.measures->energy.watts_sets[s];
    int fin=counter.measures->energy.watts_sets[s+1];
    float time=counter.measures->timing[(s*2)+1]-
               counter.measures->timing[s*2];
    float inc_time=time/(fin-ini-1);

    if(ini==0) ini=1;
    for(int i=ini; i<fin; ++i)
    {
      serverEnergy+=inc_time*counter.measures->energy.watts[i];
    }
  }
  pm_start_counter(&counter);
  counterStarted=true;
  return serverEnergy;
#else
  return 0.0;
#endif
}

ETM_Energy::~ETM_Energy()
{
#ifdef GET_CPU
  if(type==-1)
  {
    for(unsigned int i=0; i<fd.size(); ++i)
    {
      close(fd[i]);
    }
  }
  else
  {
    for(unsigned int i=0; i<fd.size(); ++i)
    {
      if(pkgFD[i]!=-1) close(pkgFD[i]);
      if(pp0FD[i]!=-1) close(pp0FD[i]);
      if(pp1FD[i]!=-1) close(pp1FD[i]);
      if(dramFD[i]!=-1) close(dramFD[i]);
    }
  }
#endif
#ifdef GET_SERVER
  if(counterStarted) pm_stop_counter(&counter);
  pm_finalize_counter(&counter);
#endif
}
