MPIFLAGS    = -I/state/partition1/soft/gnu/mpich-3.2/include -L/state/partition1/soft/gnu/mpich-3.2/lib -lmpi
SLURMFLAGS  = -I/home/siserte/slurm/include -L/home/siserte/slurm/lib -lslurm
DMRFLAGS    = -I/home/siserte/dmr -L/home/siserte/dmr -ldmr++
OMPSSFLAGS  = --ompss
FLAGS  = -O3 -k -std=gnu99 #-Wall

PROJECT = mpdata.INTEL64
BIN	= ./bin
INC	= ./inc
OBJ	= ./obj
SRC	= ./src

OGPU	= $(OBJ)/memGPU.o $(OBJ)/mpdataGPU.o
OFILE	= $(OBJ)/etmLib.o $(OGPU) $(OBJ)/memCPU.o $(OBJ)/mpdataCPU.o $(OBJ)/task.o $(OBJ)/main.o

include ./config.ini

ifeq ($(NDS),1 )
ifeq ($(GCP),0 )
CMPCPU	= mcxx --ompss
CMPGPU	= g++
else
CMPCPU	= nvcc
CMPGPU	= nvcc
endif
else
ifeq ($(GCP),0 )
CMPCPU	= mpimcxx --ompss $(MPIFLAGS) $(SLURMFLAGS) $(DMRFLAGS)
CMPGPU	= mpic++
else
CMPCPU	= nvcc --compiler-bindir mpic++
CMPGPU	= nvcc --compiler-bindir mpic++
endif
endif

#ICPU	= -I$(INC) -I/home/krojek/pmlib/include
#LCPU	= /home/krojek/pmlib/lib/pmlib.a
ICPU	= -I$(INC) -I/state/partition1/soft/gnu/mpich-3.2/include
LCPU	=
ifeq ($(NDS),1 )
IGPU	= -I$(INC)
else
IGPU	= -I$(INC) -I/state/partition1/soft/gnu/mpich-3.2/include
endif
LGPU	=

CONFIG = -DTSK=\"$(TSK)\" -DNDIM=$(N) -DMDIM=$(M) -DLDIM=$(L) -DTS=$(TS) -DTSRES=$(TSRES) -DOUTPUT=$(OUTPUT) -DPRC=$(PRC) -DX=$(X) -DV1=$(V1) -DV2=$(V2) -DV3=$(V3) -DH=$(H) -DV1P=$(V1P) -DV2P=$(V2P) -DV3P=$(V3P) -DCP=$(CP) -DCN=$(CN) -DGCP=$(GCP) -DTHS=$(THS) -DGTC=$(GTC) -DTTH=$(TTH) -DGPN=$(GPN) -DSPG=$(SPG) -DTGP=$(TGP) -DSDP=$(SDP) -DNDS=$(NDS) -DTDS=$(TDS) -DAGMC=$(AGMC) -DAGMG=$(AGMG) -DGDR=$(GDR) -DETS=$(ETS) -DECPU=$(ECPU) -DEGPU=$(EGPU) -DCPUAX=$(CPUAX) -DCPUAY=$(CPUAY) -DCPUBX=$(CPUBX) -DCPUBY=$(CPUBY) -DCPUCX=$(CPUCX) -DCPUCY=$(CPUCY) -DCPUDX=$(CPUDX) -DCPUDY=$(CPUDY) -DKRNAX=$(KRNAX) -DKRNAY=$(KRNAY) -DKRNBX=$(KRNBX) -DKRNBY=$(KRNBY) -DKRNCX=$(KRNCX) -DKRNCY=$(KRNCY) -DKRNDX=$(KRNDX) -DKRNDY=$(KRNDY) -DCPYC=$(CPYC) -DCPYG=$(CPYG) -DHSPC=$(HSPC) -DHSPG=$(HSPG) -DGCU=$(GCU)

#OPTCPU	= -pg
ifeq ($(GCP),0 )
OPTCPU	= $(OFLG) $(FMTH) $(CONFIG) -Wall -W
else
OPTCPU	= $(OFLG) $(FMTH) $(CONFIG) -Xcompiler -Xcompiler -Wall -Xcompiler -W
endif

#-Xptxas -dlcm=ca - caching load
#-Xptxas -dlcm=cg - non caching load
#-Xptxas -v       - show memory utilization
#OPTGPU	= -pg -G --maxrregcount 256
ifeq ($(GCP),0 )
OPTGPU	= $(OPTCPU)
else
OPTGPU	= $(OFLG) -Xptxas -v,-dlcm=$(DLCM),--opt-level=$(OPTL) $(FMTH) -arch $(ARCH) -lineinfo $(CONFIG) -Xcompiler -Xcompiler -Wall
endif

FCPU	= $(ICPU) $(LCPU) $(OPTCPU)
FGPU	= $(IGPU) $(LGPU) $(OPTGPU)

$(PROJECT):	$(OFILE)
	$(CMPCPU) $(FCPU) -o $(BIN)/$@ $(OFILE)
	
ifeq ($(GCP),0 )
$(OBJ)/%.o:	$(SRC)/%.cpp
	$(CMPGPU) $(FGPU) -c -o $@ $<
else
$(OBJ)/%.o:	$(SRC)/%.cu
	$(CMPGPU) $(FGPU) -c -o $@ $<
endif
$(OBJ)/%.o:	./ETM/%.cpp
	$(CMPCPU) $(FCPU) -c -o $@ $<
$(OBJ)/%.o:	$(SRC)/%.cpp
	$(CMPCPU) $(FCPU) -c -o $@ $<

-include $(subst .o,.d, $(OFILE))

clean:
	@rm -fv $(OBJ)/*
	@rm core.*

realclean:
	@rm -fv $(OBJ)/*
	@rm -fv $(BIN)/*
	@rm core.*

run:
ifeq ($(NDS),1 )
	$(BIN)/$(PROJECT) > ./out.txt
else
	mpiexec -n $(NDS) $(BIN)/$(PROJECT) > ./out.txt
endif
